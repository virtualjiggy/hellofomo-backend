<?php

return [
    'default_limit' => 20,

    'default_max_image_width'   => 1000,
    'default_max_image_height'   => 1000,
    'default_max_image_size' => 3072,
    'image_extension_logo'   => 'png,jpg,jpeg,svg',


    'list_default_status'    => [
        'pending' => 'pending',
        'publish' => 'publish',
        'draft' => 'draft',
    ],

    'validation'    => [
        'per_keyword_length' => 50,
    ],

    'general'    => [
        'token' => [
            'expired_token_day'    => 1, // days
        ],
    ],

    'role' => [
        'allow' => [
            'Administrator',
            'Subscriber'
        ],
    ],

    'default_role' => 'New Publisher',

    'settings'  => [
        'mail_driver' => 'mail_driver',
        'mail_host' => 'mail_host',
        'mail_port' => 'mail_port',
        'mail_username' => 'mail_username',
        'mail_password' => 'mail_password',
        'mail_encryption' => 'mail_encryption',
        'mail_from_name' => 'mail_from_name',
        'mail_from_address' => 'mail_from_address',
        'mail_main_admin_address' => 'mail_main_admin_address',
        'mailgun_domain' => 'mailgun_domain',
        'mailgun_secret' => 'mailgun_secret',


        'general_image_extension'    => 'general_image_extension',
        'general_image_size' => 'general_image_size',

    ],
    'session'   => [
        'primary_company'   => 'primary_company'
    ],

    'image_collections' => [
        'user'  => [
            'avatar'    => 'avatar'
        ],
    ],

    'default_record_per_page' => 10,

    'pattern_href_link_marked_text' => 'news_internal_link?id=',
];
