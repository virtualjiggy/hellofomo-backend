<?php

return [
    'enable' => env('API_LOG_ENABLE', true),
    'ignore_params' => [
        'auth_token',
        'token',
        'api_token',
        'password',
        'file',
        'client_id',
        'client_secret',
    ],
    'driver' => env('API_LOG_DRIVER', 'file'), // file or mysql,
    'ignore_log_success_response' => [
        // Ex: /api/v1/login
    ],
    'cache' => [
        'enable' => env('API_LOG_CACHE_ENABLE', false), // true or false
        'cache_time' => env('API_LOG_CACHE_TIME', 10),
        'stored_keys' => storage_path('api_log_cache_keys.json'), // Cache config
    ],
    'middleware' => ['web'],
    'per_page' => [
        5 => 5,
        10 => 10,
        20 => 20,
        40 => 40,
        0 => 'All',
    ],
    'default_per_page' => 10,
    'api' => [
        /*'/api/v1/social-login' => [
            'name' => 'Social Login',
            'attributes' => [
                'social_id' => 'Social ID',
            ],
        ],*/
    ],
];