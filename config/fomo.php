<?php

return [

    'category' => [
        'types' => [
            'news'       => 'news',
            'salutation' => 'salutation',
            'title'      => 'title',
        ],
        'limit' => 1000,
    ],

    'image' => [
        'default' => 'admin-assets/global/img/placeholder/default.png',
        'avatar_default' => 'admin-assets/global/img/placeholder/avatar.png',
    ],

    'support_languages' => [
        'English' => 'en',
        'German' => 'de',
    ],

    // @ticket #13182, #13238
    'support_fonts' => [
        'Arial'             => 'Arial',
        'Arial Hebrew'      => 'Arial Hebrew',
        'ArialHebrew-Bold'  => 'ArialHebrew-Bold',
        'ArialHebrew-Light' => 'ArialHebrew-Light',
        'Courier'           => 'Courier',
        'Courier-Bold'      => 'Courier-Bold',
        'Helvetica'         => 'Helvetica',
    ],

    'settings'  => [
        'date_time_en'     => 'date_time_en',
        'date_time_de'     => 'date_time_de',
        'language'         => 'language',
        'news_files_types' => 'news_files_types',
    ],

    // @ticket #13238
    'app_settings' => [
        'font'                         => 'app_font',
        'app_layout_dashboard'         => 'app_layout_dashboard',
        'app_layout_news'              => 'app_layout_news',
        'app_layout_contact'           => 'app_layout_contact',
        'app_layout_privacy'           => 'app_layout_privacy',
        'app_layout_impressum'         => 'app_layout_impressum',
        'app_layout_agb'               => 'app_layout_agb',
        'fcm_sender_id'                => 'fcm_sender_id',
        'fcm_server_key'               => 'fcm_server_key',
        'push_notification_max_length' => 'push_notification_max_length',
    ],

    // @ticket #13295
    'push_notification' => [
        'default_topic' => 'fomo_news'
    ]
];
