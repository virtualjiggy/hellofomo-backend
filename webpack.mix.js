const { mix } = require('laravel-mix');

/**
 * Fomo system
 */
mix.copy('resources/assets/admin/dev/js', 'public/admin-assets/internal/js');

mix.js([
    'resources/assets/admin/dev/js/table.js',
    'resources/assets/admin/dev/js/form-validation.js',
    'resources/assets/admin/dev/js/fomo-form.js',
    'resources/assets/admin/dev/js/news/fomo-news-form.js',
    'resources/assets/admin/dev/js/fomo-news-services.js',
    'resources/assets/admin/dev/js/page-bar.js',
    'resources/assets/admin/dev/js/user/form.js',
    'resources/assets/admin/dev/js/toastr.js',
    'resources/assets/admin/dev/js/user/update-profile.js',
    'resources/assets/admin/dev/js/fomo-link-marked-text.js',
    'resources/assets/admin/dev/js/app/fomo-app-dashboard-layout.js',
    'resources/assets/admin/dev/js/app/push-notification.js',
], 'public/admin-assets/internal/js/app.js')


mix.sass('resources/assets/admin/dev/scss/app.scss', 'public/admin-assets/internal/css');
mix.copy('resources/assets/js/libs/jquery/jquery.more.js', 'public/admin-assets/global/scripts/jquery.more.js');
/**
 * Files management
 * */
mix.sass('vendor/botble/media/resources/assets/sass/media.scss', 'public/vendor/media/css');
mix.js('vendor/botble/media/resources/assets/js/integrate.js', 'public/vendor/media/js');
mix.js('vendor/botble/media/resources/assets/js/media.js', 'public/vendor/media/js');
mix.js('vendor/botble/media/resources/assets/js/focus.js', 'public/vendor/media/js');

/**
 * ACL
 * */
mix.js('resources/assets/admin/vendor/acl/js/acl.js', 'public/vendor/acl/js/acl.js');


/**
 * Build for media package
 * */
mix
    .copyDirectory('public/vendor/media/css', './vendor/botble/media/public/assets/css')
    .copyDirectory('public/vendor/media/js', './vendor/botble/media/public/assets/js');

if (mix.config.inProduction) {
    mix.version();
}
