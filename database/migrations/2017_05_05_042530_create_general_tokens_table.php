<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneralTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('general_tokens', function(Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->string('email', 100);
            $table->string('token', 100);
            $table->string('type', 20);
            $table->primary(['email', 'token', 'type']);
            $table->timestamp('expired_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('general_tokens');
    }
}
