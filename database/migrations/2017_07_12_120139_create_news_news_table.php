<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsNewsTable extends Migration
{

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_news', function(Blueprint $table) {
			$table->bigInteger('news_id')->unsigned();
			$table->bigInteger('related_news_id')->unsigned();

			$table->primary(['news_id', 'related_news_id']);
			$table->index('related_news_id','related_news_id_idx');

			$table->foreign('news_id')->references('id')->on('news');
			$table->foreign('related_news_id')->references('id')->on('news');

			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news_news');
	}

}
