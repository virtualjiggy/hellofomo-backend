<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsAuthorsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_authors', function(Blueprint $table) {
			$table->bigInteger('news_id')->unsigned();
			$table->bigInteger('author_id')->unsigned();

			$table->primary(['news_id', 'author_id']);
			$table->index('author_id','author_id_idx');

			$table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
			$table->foreign('author_id')->references('id')->on('authors')->onDelete('cascade');

            $table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news_authors');
	}
}
