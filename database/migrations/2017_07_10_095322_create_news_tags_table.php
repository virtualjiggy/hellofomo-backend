<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTagsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_tags', function(Blueprint $table) {
			$table->bigInteger('news_id')->unsigned();
			$table->integer('tag_id')->unsigned();

			$table->primary(['news_id', 'tag_id']);
			$table->index('tag_id','tag_id_idx');

			$table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
			$table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news_tags');
	}
}
