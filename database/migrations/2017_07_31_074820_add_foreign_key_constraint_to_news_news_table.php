<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyConstraintToNewsNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('news_news', function (Blueprint $table) {
            $table->dropForeign('news_news_news_id_foreign');
            $table->foreign('news_id')
                ->references('id')->on('news')
                ->onDelete('cascade')
                ->change();

            $table->dropForeign('news_news_related_news_id_foreign');
            $table->foreign('related_news_id')
                ->references('id')->on('news')
                ->onDelete('cascade')
                ->change();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('news_news', function (Blueprint $table) {
            //
        });
    }
}
