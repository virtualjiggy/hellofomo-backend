<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsCategoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('news_categories', function(Blueprint $table) {
			$table->bigInteger('news_id')->unsigned();
			$table->bigInteger('category_id')->unsigned();

			$table->primary(['news_id', 'category_id']);
			$table->index('category_id','category_id_idx');

			$table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
			$table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('news_categories');
	}
}
