<?php

use Illuminate\Database\Seeder;
use App\Repositories\Contracts\UserRepositoryInterface;

class UsersTableSeeder extends Seeder
{
    /**
     * @var $userRepo
     */
    protected $userRepo;

    /**
     * @param UserRepositoryInterface $userRepo
     */
    public function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if ( !$this->userRepo->findWhere(['email' => 'admin@elidev.info'])->first() ) {
            $user = $this->userRepo->create([
                'email'    => 'admin@elidev.info',
                'name'     => 'admin',
                'status'   => config('elidev.list_default_status.publish'),
                'password' => bcrypt('!@#Whatever'),
            ]);
            $user->assignRole('Super Administrator');
        }
    }
}
