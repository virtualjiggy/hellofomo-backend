<?php

use Illuminate\Database\Seeder;


class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            // seed date time & language
            config('fomo.settings.date_time_en') => 'Y/m/d H:i',
            config('fomo.settings.date_time_de') => 'd.m.Y H:i',
            config('fomo.settings.language') => 'en',
        );

        foreach($data as $key => $val) {
            \Efriandika\LaravelSettings\Facades\Settings::set($key, $val);
        }

    }
}
