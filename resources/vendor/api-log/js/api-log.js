(function ($) {
    'use strict';
    $(document).ready(function () {
        $('.api-timepicker-none-input').timepicker({
            showInputs: false
        });

        $('.api-datepicker').datepicker({
            autoclose: true
        });

        $('#api_name').on('change', function () {
            console.log('aaa');
            if ($(this).val() !== null && $(this).val() !== '') {
                console.log('bbb');
                $.ajax({
                    url: '/api-logs/get-api-attributes',
                    data: {
                        'url': $('#api_name').val()
                    },
                    type: 'GET',
                    dataType: 'json',
                    success: function (response) {
                        var options = '<option value="">Select a field in Request\'s Body</option>';
                        $.each(response.data, function (value, text) {
                            options += '<option value="' + value + '">' + text + '</option>';
                        });
                        $('#search_key').html(options).prop('disabled', false);
                        $('#search_value').prop('disabled', true);
                    }
                });
            } else {
                $('#search_key').prop('disabled', true);
                $('#search_value').prop('disabled', true);
            }
        });

        $('#from_date').on('change', function () {
            if ($(this).val() !== null && $(this).val() !== '') {
                $('#from_time').prop('disabled', false);
            } else {
                $('#from_time').prop('disabled', true);
            }
        });

        $('#to_date').on('change', function () {
            if ($(this).val() !== null && $(this).val() !== '') {
                $('#to_time').prop('disabled', false);
            } else {
                $('#to_time').prop('disabled', true);
            }
        });

        $('#search_key').on('change', function () {
            if ($(this).val() !== null && $(this).val() !== '') {
                $('#search_value').prop('disabled', false);
            } else {
                $('#search_value').prop('disabled', true);
            }
        });

        $('.select-user').multiselect({
            enableFiltering: true,
            nonSelectedText: 'Modified by user'
        });

        $('#per_page').on('change', function () {
           $(this).closest('form').submit();
        });
    });
})(jQuery);