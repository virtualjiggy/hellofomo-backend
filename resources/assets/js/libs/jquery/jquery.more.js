
/* ========================================================================
 * PYTHWEB: MORE.js v1.1.0
 * Requires jQuery v1.7 or later
 * Requires jQuery blockUI plugin or Later
 *
 * Examples at: http://pythweb.com
 * Copyright (c) 2007-2013 M. Alsup
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 * ======================================================================== */

+function ($) {
    'use strict';

    // More CLASS DEFINITION
    // =========================

    var More = function (element, options) {
        this.$element    = $(element).on('click', $.proxy(this.more, this));
        this.options     = options;
    };

    More.VERSION  = '1.1.0';

    More.DEFAULTS = {
        "sourcetype": "url",
        "holder": "#moreContainer",
        "methodinsert": "after"
    };

    More.prototype.more = function() {
        var _this = this,
            options = this.options;
        var notInIds = _this.$element.attr('data-not-in-ids'),
            totalDisplayedComments = _this.$element.attr('data-total-display-comments');

        // these variable only have in "user/deeds" page
        var deedIdsInPage1 = _this.$element.attr('data-deed-ids-page-1'),
            deedOffset = _this.$element.attr('data-deed-offset');

        this.$holder     = $(this.options.holder);

        var message = this.options.blockuihtml;
        var _blockUI = {
            message: message ? message : null,
            css: this.options.blockuicss
        };

        var _container = window;
        if(this.options.container !== undefined) {
            _container = this.options.container;
        }
        $(_container).block(_blockUI);
        _this.$element.addClass('wrapper-spinner');

        $(document).ajaxStop(function() {
            $(_container).unblock(_blockUI);
            _this.$element.removeClass('wrapper-spinner');
        });
        switch(this.options.sourcetype) {
            case 'url':
                this.options.sourcedata = _this.$element.data('sourcedata');

                // append not in ids
                if (notInIds) {
                    this.options.sourcedata += '&not_in_ids=' + notInIds;
                }

                // append total deleted comments number
                if ( totalDisplayedComments && totalDisplayedComments >= 0 ) {
                    this.options.sourcedata += '&total_display_comments=' + totalDisplayedComments;
                }

                if ( deedIdsInPage1 != '' ) {
                    this.options.sourcedata += '&deed_ids_page_1=' + deedIdsInPage1;
                }
                if ( deedOffset != '' ) {
                    this.options.sourcedata += '&deed_offset=' + deedOffset;
                }

                $.post(this.options.sourcedata, function( data ) {
                    if(data.url === undefined || data.url === "") {
                        console.log('To Using this plugin you must return next page url');
                        return;
                    }

                    // update total comments in current page
                    if ( _this.$element.attr('data-total-display-comments') != '' ) {
                        var totalComments = $('#list-comments .info-comment--content').length + data.total;
                        _this.$element.attr('data-total-display-comments', totalComments);
                    }

                    // in deed detail, comment section, if user deleted all comments in current page, so after call load more
                    // this plugin couldn't append data
                    if ( _this.$element.attr('data-page-source') == 'deed-comment' && _this.$holder.length == 0 ) {
                        var $holder = $(_this.$element.attr('data-comments-list-holder'));
                        $holder.append( data.html );
                    } else {
                        _this.$holder[_this.options.methodinsert]( data.html );
                    }

                    if( data.except_deeds_id != undefined || data.except_deeds_id != "" ) {
                        var currentIds = _this.$element.attr('data-deed-ids-page-1'),
                            newIds = currentIds + ',' + data.except_deeds_id;
                        _this.$element.attr('data-deed-ids-page-1', newIds);
                    }

                    _this.$element.data('sourcedata', data.url);

                    if(data.hasMore !== undefined && data.hasMore === false) {
                        _this.$element.hide();
                    }

                    if (_this.$element.data().parent != undefined && data.hasMore === false) {
                        _this.$element.parent(_this.$element.data().parent).hide();
                    }

                    $('body').trigger( 'viewmore', { 'hasMore' : data.hasMore, 'containerListItems' : _container, 'responseData' : data } );
                }).
                done(function() {

                }).
                fail(function() {

                }).
                always(function(){

                });

                this.$holder[this.options.methodinsert]()
                break;
            default:
                break;
        }
    }

    function Plugin(option) {
        return this.each(function () {
            var $this   = $(this)
            var data    = $this.data('bs.more')
            var options = $.extend({}, More.DEFAULTS, $this.data(), typeof option == 'object' && option)

            if (!data) $this.data('bs.more', (data = new More(this, options)))
        })
    }

    $.fn.more             = Plugin
    $.fn.more.Constructor = More

    $(window).on('load', function () {
        $('[data-ride="ap-more"]').each(function () {
            var $more = $(this)
            Plugin.call($more, $more.data())
        })
    })

}(jQuery);
