aclFn = function () {

    /**
     * Handle ACL permission by disabling some blocks or buttons
     * */
    function hideDisallowPermissions() {
        var permissions = window.permissions;
        var $elms = $('[data-acl-role]');

        if (! window.is_full_permission) {
            $elms.each(function(i, v) {
                var data = $(v).data();
                if (permissions.indexOf(data.aclRole) == -1 || permissions.indexOf(data.aclRole) == undefined) {
                    $(v).addClass('acl-not-allow')
                }
            });
        }

    }

    /**
     * Handle display alert message if some fields in the form not have permission
     * */
    function displayWarning() {
        if ($('form').find('[data-acl-role]').hasClass('acl-not-allow')) {
            try {
                toastr.warning(acl_msgs.warning_form_msg);
            }
            catch(e) {}
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            hideDisallowPermissions();
            displayWarning();
        }
    };

}();

jQuery(document).ready(function() {
    aclFn.init();
});
