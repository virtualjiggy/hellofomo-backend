FomoPageBar = function () {
    var originForm;

    /**
     * Handle click on the Save icon on the page bar
     * @author vulh
     * */
    function handleSaveFormBtn() {
        $('.page-bar').on('click', FOMO.saveBtnForm, function(e) {
            if ($(this).hasClass(FOMO.overrideSaveFrmClass)) {
                return false;
            }
            e.preventDefault();

            var pageContent = $(this).closest('.page-content');
            var form = FOMO.listeningFrm;
            var formIsValid = true;
            try {
                formIsValid = $(form).valid()
            }
            catch (e) {
                formIsValid = true;
            }

            if (formIsValid) {
                pageContent.find(form).submit();
            }
            else {
                toastr.error($(this).data().errorMsg);
            }
        });

    }

    /**
     * Handle click on the Delete forever icon on the page bar
     * @author vulh
     * */
    function handleDeleteForeverFormBtn() {
        $('.page-bar').on('click', '#fomo-single-delete-btn', function(e) {
            e.preventDefault();
            $('#fomo-single-confirm-delete-modal').modal('show');
        });
    }

    /**
     * Handle click on the View icon on the page bar
     * */
    function handleViewFormBtn() {
        $('.page-bar').on('click', '#fomo-view-btn', function(e) {
            askBeforeLeave(e);
        });
    }

    /**
     * Handle click on the Create icon on the page bar
     * */
    function handleCreateFormBtn() {
        $('.page-bar').on('click', '#fomo-create-btn', function(e) {
            askBeforeLeave(e);
        });
    }

    /**
     * Ask to confirm user before leave to new page (handle for View & Create buttons in the breadcrumb page bar)
     */
    function askBeforeLeave(e) {
        // if the form has changed
        if ( typeof originForm != 'undefined' && originForm && $('form.form-validation:not(.rv-form)').serialize() != originForm ) {
            e.preventDefault();

            var $anchor = $(e.currentTarget);
            swal(
                {
                    type: 'info',
                    title: 'Confirm',
                    text: 'Do you want to leave?',
                    allowOutsideClick: true,
                    showCancelButton: true,
                    confirmButtonClass: 'green',
                },
                function( isConfirmOK ) {
                    if ( isConfirmOK ) {
                        window.location.href = $anchor.attr('href');
                    }
                }
            );
        }
    }

    return {
        //main function to initiate the module
        init: function () {

            setTimeout(function() {
                if ( $('form.form-validation:not(.rv-form)') ) {
                    originForm = $('form.form-validation:not(.rv-form)').serialize();
                }
            }, 1000);

            handleSaveFormBtn();
            handleDeleteForeverFormBtn();

            // ask user before leave
            handleViewFormBtn();
            handleCreateFormBtn();
        }
    };

}();

jQuery(document).ready(function() {
    FomoPageBar.init();
});
