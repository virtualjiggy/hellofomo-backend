FomoForm = function () {

    /**
     * Auto focus on the fist child
     * */
    function autoFocusOnTheFistElm() {
        $('form').find('input:not(:hidden)').first().focus();
    }

    function enableDateTimePicker() {
        if ( $('.fomo-list-page-date-filter').size() > 0 ) {
            $('.fomo-list-page-date-filter').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                minView: 2
            });
        }
        if ( $('input.fomo-list-page-time-filter').size() > 0 ) {
            $('input.fomo-list-page-time-filter').inputmask('hh:mm', {
                placeholder: "00:00",
            });
        }
        if ( $('.fomo-list-page-datetime-filter').size() > 0 ) {
            $('.fomo-list-page-datetime-filter').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                minView: 2
            });
        }

        if ( $('.date-time-picker').size() > 0 ) {
            $('.date-time-picker').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                startDate: new Date(),
                pickTime: false,
                minView: 2
            });
        }

        if ( $('.btn-clear-date-filter').size() > 0 ) {
            $(document).on('click', '.btn-clear-date-filter', function() {
                $(this).parent().siblings('input').val('');
                $(this).parent().siblings('input').trigger('change');
            });
        }
    }

    function enableUploadFile() {
        var btnUploadImage = '.btn-upload-image',
            btnRemoveImage = '.btn-remove-image';

        if ( !$.fn.rvMedia ) {
            return;
        }

        // upload image
        $(btnUploadImage).rvMedia({
            multiple: false,
            onSelectFiles: function (files, $el) {
                var firstItem = _.first(files);
                if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && !firstItem.type.match('image') ) {

                } else if (typeof $el !== 'undefined') {
                    $el.closest('.image-box').find('.image-data').val(firstItem.id);
                    $el.closest('.image-box').find('.preview_image').attr('src', firstItem.thumb)
                        .data('rv-media', [{'selected_file_id': firstItem.id, 'file_type': 'image'}]).show();
                    $el.closest('.image-box').find(btnRemoveImage).show();
                    $el.closest('.image-box').find(btnUploadImage).hide();
                }
            }
        });

        // remove image
        $('.image-box').on('click', btnRemoveImage, function (event) {
            event.preventDefault();
            $(this).closest('.image-box').find('input').val('');

            var $previewImg = $(this).closest('.image-box').find('img.preview_image'),
                defaultSrc = $previewImg.attr('data-default-src');
            if ( typeof defaultSrc !== 'undefined' && defaultSrc !== '' ) {
                $previewImg.attr('src', defaultSrc).show();
            } else {
                $(this).closest('.image-box').find('img').hide();
            }

            $(this).closest('.image-box').find(btnUploadImage).show();
            $(this).closest('.image-box').find(btnRemoveImage).hide();
        });
        previewImageMedia();
    }

    //preview image
    function previewImageMedia(){
        $(".rvMedia_preview_file").each(function() {
            $(this).rvMedia({
                multiple: false,
                onSelectFiles: function (files, $el) {
                    var firstItem = _.first(files);
                    if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && firstItem.type.match('image')
                        && typeof $el !== 'undefined') {
                        $el.parent().find('.image-data').val(firstItem.id);
                        $el.attr('src', firstItem.thumb)
                            .data('rv-media', [{'selected_file_id': firstItem.id, 'file_type': 'image'}]).show();
                    }
                }
            });
        });
    };

    function enableUploadGalleryImages() {
        var btnUploadImage = '.btn-gallery-upload-image';
        if ( $(btnUploadImage).is(':visible') ) {
            // upload gallery images
            $(btnUploadImage).rvMedia({
                multiple: true,
                onSelectFiles: function (files, $el) {
                    var $gallery = $('.gallery-images'),
                        imagePlaceHolder = $gallery.find('li.image-placeholder');
                    if (typeof files !== 'undefined') {
                        for (var i = 0; i < files.length; i++) {
                            // only pics are processed
                            if (!files[i].type.match('image')) {
                                continue;
                            }

                            var $image = imagePlaceHolder.clone().removeClass('image-placeholder').removeClass('hidden').insertBefore(imagePlaceHolder);
                            if (typeof $image !== 'undefined' && $image) {
                                $image.attr('data-id', files[i].id);
                                $image.find('img').addClass('rvMedia_preview_file')
                                    .attr('src', files[i].url)
                                    .data('rv-media', [{'selected_file_id': files[i].id}]);
                                $image.find('a.btn-delete-gallery-img').attr('data-id', files[i].id);
                                $image.find('input[type="hidden"]').attr('value', files[i].id);
                                $image.find('input[type="hidden"]').attr('name', 'galleries[]');
                            }
                        }
                        previewImageMedia();
                    }
                }
            });

            // handle remove gallery images
            $(document).on('click', '.btn-delete-gallery-img', function(e) {
                var dataId = $(this).attr('data-id');
                if ( typeof dataId !== 'undefined' && dataId && $(this).parent().attr('data-id') === dataId ) {
                    $(this).parent().remove();
                }
            });
        }
    }

    function handleSelectFiles() {
        var btnTrigger = '.btn-gallery-select-file';
        if ( $(btnTrigger).is(':visible') ) {
            $(btnTrigger).rvMedia({
                multiple: true,
                onSelectFiles: function (files, $el) {
                    var $list = $('.list-files'), filePlaceHolder = $list.find('li.file-placeholder');
                    for(var i = 0; i < files.length; i++) {
                        var $file = filePlaceHolder.clone().removeClass('file-placeholder').removeClass('hidden').insertBefore(filePlaceHolder);
                        $file.attr('data-id', files[i].id);
                        $file.find('.file-icon').attr('class', files[i].icon);
                        $file.find('.file-item').attr('href', files[i].url).text(files[i].basename);
                        $file.find('a.btn-delete-file').attr('data-id', files[i].id);
                        $file.find('input[type="hidden"]').attr('value', files[i].id).attr('name', 'attachments[]');
                    }
                }
            });


            // handle remove gallery images
            $(document).on('click', '.btn-delete-file', function(e) {
                var dataId = $(this).attr('data-id');
                if ( typeof dataId !== 'undefined' && dataId && $(this).parent().attr('data-id') === dataId ) {
                    $(this).parent().remove();
                }
            });
        }
    }

    function initResources() {
        $('.select2, .select2-multiple').select2({
            placeholder: $(this).data('placeholder'),
            width: null
        });

        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker({
                autoclose: true,
                showSeconds: true,
                minuteStep: 1
            });

            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5,
                defaultTime: false
            });

            $('.timepicker-24').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            // handle input group button click
            $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function (e) {
                e.preventDefault();
                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            enableDateTimePicker();
            enableUploadFile();
            enableUploadGalleryImages();
            initResources();
            handleSelectFiles();
            autoFocusOnTheFistElm();
        }
    };

}();

jQuery(document).ready(function() {
    FomoForm.init();
});
