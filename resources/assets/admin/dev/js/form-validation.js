FormValidation = function () {

    var handleFormValidation = function() {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var forms = $('.form-validation');
        var error = $('.alert-danger', forms);
        var success = $('.alert-success', forms);

        forms.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",  // validate all fields including form hidden input

            invalidHandler: function (event, validator) { //display error alert on form submit
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function (error, element) { // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function (element) { // hightlight error inputs

                $(element)
                    .closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function (element) { // revert the change done by hightlight
                $(element)
                    .closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function (label) {
                label
                    .closest('.form-group').removeClass('has-error'); // set success class to the control group
            },

            //submitHandler: function (form) {
            //    success.show();
            //    error.hide();
            //}
        });

        // validate these controls when changing
        $('.select2, .multi-select').change(function () {
            forms.validate().element($(this));
        });

    }

    var handleWysihtml5 = function() {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"],
                "events": {
                    "blur": function() {
                        // hide error message if the editor is not empty
                        if ( this.textareaElement.value.length > 0 ) {
                            $(this.textareaElement).closest('.form-group').removeClass('has-error');
                            $(this.textareaElement).siblings().find('.help-block.help-block-error').hide();
                        }
                    }
                }
            });
        }
    }

    var handleSummernote = function () {
        if ($('.summernote').size() > 0) {
            $('.summernote').summernote({
                // customize summernote toolbar, @ticket #13187
                toolbar: [
                    ['style', ['style']],
                    ['font', ['bold', 'italic', 'underline', 'clear']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['insert', ['link']],
                    ['misc', ['fullscreen']],
                ],
                height: 300,
                callbacks: {
                    onBlur: function() {
                        if ( $(this).summernote('isEmpty') ) {
                            $(this).parent().parent('.form-group').addClass('has-error');
                            $(this).parent().find('.help-block.help-block-error').show();
                            $(this).summernote('code', '');
                        } else {
                            // hide error message if the editor is not empty
                            $(this).parent().parent('.form-group').removeClass('has-error');
                            $(this).parent().find('.help-block.help-block-error').hide();
                        }
                    }
                }
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {

            handleSummernote();
            handleWysihtml5();
            handleFormValidation();
        }

    };

}();

jQuery(document).ready(function() {
    FormValidation.init();
});
