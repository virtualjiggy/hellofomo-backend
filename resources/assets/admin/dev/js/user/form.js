FomoUserForm = function () {

    /**
     * Handle click on the Save icon on the page bar
     * @author vandd
     * */
    function handleSaveFormBtn() {
        $('.page-bar').on('click', FOMO.saveBtnForm, function(e) {
            if ( $(this).attr('data-ajax-url') != '' ) {
                $.ajax({
                    url: $(this).attr('data-ajax-url'),
                    type: 'POST',
                    data: {
                        'email': $('#email').val(),
                        'user_id': $(this).attr('user-id')
                    },
                    beforeSend: function () {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function( response ) {
                        $.unblockUI();
                        response = JSON.parse(response);
                        var emailElement = $('#email').closest('.form-group');
                        if ( response.data.email ) {
                            var errorText = emailElement.find('#email-error');
                            if ( !emailElement.hasClass('has-error') ) {
                                emailElement.addClass('has-error');
                            }

                            if(errorText.length > 0) {
                                $(errorText).text(response.data.message);
                            }
                        } else {
                            $(FOMO.saveBtnForm).closest('.page-content').find('form:not(.rv-form)').submit();
                        }
                    },
                    error: function(jqXHR, exception) {
                        $.unblockUI();
                    }
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSaveFormBtn();
        }
    };

}();

jQuery(document).ready(function() {
    FomoUserForm.init();
});
