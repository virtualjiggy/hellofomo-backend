@extends('layouts.app')

@section('title')
    {{ __('Home page') }}
@endsection

@section('body_class')
    page-home js-page-home transparent-header
@endsection

@section('background_video')
    <div class="homepage-hero-module">
        <div class="video-container">
            <div class="fillWidth"><video class="fillWidth hidden-xs hidden-sm hidden-md" controls="false" autoplay="" loop="" poster="/assets/img/hannumoilanen.jpg" preload=""><source src="/assets/img/Heaven-From-Top/MP4/sunlight-shining_CUT_v2_4.mp4" type="video/mp4"><source src="/assets/img/Heaven-From-Top/WEBM/Heaven-From-Top.webm" type="video/webm"></video>
                <div class="poster visible-xs visible-sm visible-md"><img class="lazy" data-src="/assets/img/hannumoilanen.jpg"></div>
            </div>
        </div>

        <div class="hero-views js-hero-view">
            <div class="container">
                <h1 class="hero-header">{{ isset($videoDesc) ? $videoDesc : '' }}</h1>
                <div class="interaction-bar">
                    @if (!$isLoggedIn)
                    <a class="hero-btn btn-green" href="#" type="button" data-toggle="modal" data-target="#join">{{ __("Join Now") }}</a>
                    @endif
                    <a class="hero-btn no--padding" href="{{ action('BlogController@about') }}#frequently-asked-questions"> {{ __("Frequently Asked Questions") }}</a></div>
            </div>
        </div>
    </div>
@endsection


@section('content')
    <div class="hero-container">
        <div class="deed-views">
            @if ( count($featuredDeeds) > 0 )
                <div class="container deed-container">
                    <div class="row hidden-xs hidden-sm">
                        <?php $cols = 0; $row = 0; ?>
                        @foreach($featuredDeeds as $key => $deedChunk)
                            <?php

                                $cols++;

                                if ($cols == 1) {
                                    $class  = ( $row >= 1 ) ? 'hidden-xs hidden-sm' : '';
                                    $colKey = 'offset-top-col-' . ($row + 1);
                                }
                            ?>

                            @if ($cols == 1)
                                <div class="col-lg-4 col-md-4 {{ $class }} {{ $colKey }}">
                            @endif

                            @include('deed.partial._inside_item', ['deed' => $deedChunk])

                            @if ($cols == 3)
                                </div>
                                <?php
                                    $cols = 0;
                                    $row++;
                                ?>
                            @endif
                        @endforeach
                    </div>

                    <div class="row visible-xs-block visible-sm mobile">
                        <div class="col-xs-12">
                            <div class="owl-carousel owl-theme">
                                @foreach($featuredDeeds as $key => $deedChunk)
                                    @include('deed.partial._inside_item', ['deed' => $deedChunk])
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <div class="more-deed-row"><a class="btn btn-white viewmore-title" href="{{ route('deeds.index') }}">{{ __('More Deeds') }}</a></div>
                </div>
            @endif
        </div>
    </div>

    @if ($homeAboutPage)
    <div class="home-about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="title hidden-xs">{{ $homeAboutPage->post_title }}</p>
                    <div class="block-content">
                        {!! $homeAboutPage->post_content !!}
                    </div>
                    <div class="block-action"><a href="{{ action('BlogController@about') }}">{{ __('Learn More') }}</a></div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection

@section('media_sharing')
    @include('partial.media_sharing')
@endsection
