<div class="form-group">
    <div class="input-group date">
        <input type="text" class="form-control api-datepicker input-sm" id="from_date" name="from_date" placeholder="{{ __('From date') }}" value="{{ request()->input('from_date') }}">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="bootstrap-timepicker">
        <div class="input-group">
            <input class="form-control api-timepicker-none-input input-sm" @if (!Request::has('from_date')) disabled="disabled" @endif id="from_time" name="from_time" value="{{ request()->has('from_time') ? \Carbon\Carbon::createFromFormat('H:i A', request()->input('from_time'))->format('h:i A') : '12:00 AM' }}" type="text">
            <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="input-group date">
        <input type="text" class="form-control api-datepicker input-sm" id="to_date" name="to_date" placeholder="{{ __('To date') }}" value="{{ request()->input('to_date') }}">
        <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
        </div>
    </div>
</div>

<div class="form-group">
    <div class="bootstrap-timepicker">
        <div class="input-group">
            <input class="form-control api-timepicker-none-input input-sm" @if (!Request::has('to_date')) disabled="disabled" @endif id="to_time" name="to_time" value="{{ request()->has('to_time') ? \Carbon\Carbon::createFromFormat('H:i A', request()->input('to_time'))->format('h:i A') : '12:00 AM' }}" type="text">
            <div class="input-group-addon">
                <i class="fa fa-clock-o"></i>
            </div>
        </div>
    </div>
</div>

<div class="form-group">
    <select name="api_name" id="api_name" class="form-control input-sm">
        <option value="">{{ __('Select API name') }}</option>
        @foreach(config('api-log.api', []) as $api_key => $api)
            <option value="{{ $api_key }}" @if (request()->input('api_name') == $api_key) selected @endif>{{ array_get($api, 'name') }}</option>
        @endforeach
    </select>
</div>

<div class="form-group">
    <select name="search_key" id="search_key" class="form-control input-sm" @if (!Request::has('api_name')) disabled="disabled" @endif>
        <option value="">{{ __('Select a field in request') }}</option>
        @if (request()->has('search_key'))
            @foreach(config('api-log.api.' . request()->input('api_name') . '.attributes', []) as $key => $attribute)
                <option value="{{ $key }}" @if (request()->input('search_key') == $key) selected @endif>{{ $attribute }}</option>
            @endforeach
        @endif
    </select>
</div>

<div class="form-group">
    <input type="text" name="search_value" class="form-control input-sm" value="{{ request()->input('search_value') }}" id="search_value" @if (!Request::has('api_name') || !Request::has('search_key')) disabled="disabled" @endif placeholder="{{ __('Value to search') }}">
</div>

<div class="form-group" style="max-width: none">
    <select name="user_id[]" id="user_id" class="form-control input-sm select-user" multiple>
        @foreach($users as $user)
            <option value="{{ $user->id }}" @if (in_array($user->id, request()->input('user_id', []))) selected @endif>{{ $user->name }}</option>
        @endforeach
    </select>
</div>
<div class="form-group select-method">
    <select name="method" id="method" class="form-control input-sm">
        <option value="">{{ __('Select method') }}</option>
        <option value="GET">GET</option>
        <option value="POST">POST</option>
        <option value="PUT">PUT</option>
        <option value="DELETE">DELETE</option>
    </select>
</div>

<div class="form-group form-group-actions">
    <input type="hidden" name="is_filtered" value="1">

    <button class="btn btn-primary btn-sm btn-flat" title="{{ __('Search') }}"><i class="fa fa-search"></i></button>

    <a class="btn btn-info btn-sm btn-flat" href="{{ url()->current() }}" title="{{ __('Clear Filter') }}"><i class="fa fa-refresh"></i></a>
</div>
