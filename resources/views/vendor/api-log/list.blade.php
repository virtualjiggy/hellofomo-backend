<div class="api-log-header">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{ asset('vendor/api-log/packages/bootstrap-multiselect/bootstrap-multiselect.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/api-log/packages/timepicker/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/api-log/packages/datepicker/datepicker3.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/api-log/css/api-log.css') }}">
</div>

<div class="api-log-wrapper">
    <form action="{{ url()->current() }}" method="get" class="filter_log_form">
    <div class="widget">
        <div class="widget-title">
            <div class="pull-left">
                <h4><i class="box_img_overdone"></i></h4>
            </div>
            <div class="pull-left">
                @include('api-log::partials.filter')
            </div>
        </div>
        <div class="clearfix"></div>
        <div class="widget-body">
            <div class="table-responsive">
                <table class="table table-hover table-striped table-bordered">
                    <thead>
                    <tr>
                        <th>{{ __('Date Time') }}</th>
                        <th>{{ __('User Agent') }}</th>
                        <th>{{ __('URL') }}</th>
                        <th>{{ __('Method') }}</th>
                        <th>{{ __('Request\'s Body') }}</th>
                        <th>{{ __('Response') }}</th>
                        <th>{{ __('User') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($logs as $log)
                        <tr>
                            <td>{{ $log->created_at }}</td>
                            <td><span title="{{ $log->user_agent }}">{{ api_format_string($log->user_agent, 30) }}</span></td>
                            <td>{{ $log->url }}</td>
                            <td>{{ $log->method }}</td>
                            <td>{!! api_format_data($log->request) !!}</td>
                            <td>{!! api_format_data($log->response) !!}</td>
                            <td>{{ $log->user_name }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="widget-title">
                <div class="row">
                    <div class="col-sm-6 pull-left text-left">
                        <div class="form-group" style="max-width: 120px">
                            <select name="per_page" id="per_page" class="form-control">
                                @foreach(config('api-log.per_page', []) as $key => $value)
                                    <option value="{{ $key }}" @if (request()->input('per_page', config('api-log.default_per_page')) == $key) selected @endif>{{ $value }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6 pull-right text-right">
                        {!! $logs->links() !!}
                    </div>
                </div>
            </div>
        </div>
        </div>
    </form>
</div>

<div class="api-log-footer">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="{{ asset('vendor/api-log/packages/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('vendor/api-log/packages/timepicker/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('vendor/api-log/packages/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendor/api-log/js/api-log.js') }}"></script>
</div>