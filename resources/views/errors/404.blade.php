@extends('layouts.app')

@section('title', __('Error 404 (page not found)'))

@section('body_class')
    page-404
@endsection

@section('content')
    <div class="container">
        <div class="wrapper-404">
            <h3 class="ttl-404"><span>{{ __('404') }}</span>{{ __('That\'s an error') }}</h3>
            <p>{{ __('The requested URL') }} {{ str_replace(url(''), '', url()->current()) }} {{ __('was not found on this server.') }}</p>
        </div>
    </div>
@endsection

