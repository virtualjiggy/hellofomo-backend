{{-- Only render when have delete action in table --}}
@if ( array_get($table, 'actions.delete') != null )
    <div class="modal fade" id="listing-confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <form method="POST" action="{{ route(array_get($table, 'actions.delete.route'), ['id' => -1]) }}">
                {{ csrf_field() }}
                {{ method_field('DELETE') }}

                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title">{{ __('Delete permanently') }}</h4>
                    </div>
                    <div class="modal-body">{{ __('Do you want to delete permanently?') }}</div>
                    <div class="modal-footer">
                        <button type="button" class="btn dark btn-outline btn-close" data-dismiss="modal">{{ __('Close') }}</button>
                        <button type="submit" class="btn red btn-action">{{ __('Delete permanently') }}</button>
                    </div>
                </div>
            </form>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endif
