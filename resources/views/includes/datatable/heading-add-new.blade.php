<div class="portlet-title">
    <div class="caption font-green">{{ __(array_get($table, 'title', 'Table Overview')) }}</div>
    <div class="actions">
        <button class="btn btn-icon-only btn-default fullscreen"
                data-original-title="Vollbild"
                title="{{ __('Full screen') }}">
        </button>
    </div>
</div>

