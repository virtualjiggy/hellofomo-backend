@php $hasCheckbox = array_get($table, 'checkbox_column', false); @endphp

<tr role="row" class="filter">
    @if ( $hasCheckbox )
        <th></th>
    @endif

    @foreach($table['columns'] as $key => $column)
        @php
            $type = array_get($column, 'filter.type', '');
            $dataColumnIndex = $hasCheckbox ? $loop->iteration : $loop->iteration - 1;
        @endphp

        @if ( $type === '' )
            <th></th>
        @elseif ( $type == 'input' )
            <th>
                <input type="text"
                       class="form-control form-filter input-sm"
                       name="{{ $key }}"
                       data-column-index="{{ $dataColumnIndex }}">
            </th>
        @elseif ( $type == 'select' )
            <th>
                <select class="filter-select form-control form-filter"
                        data-column-index="{{ $dataColumnIndex }}"
                        name="{{ $key }}">
                    <option value="">{{ __('Select') }}</option>
                    @foreach(array_get($column, 'filter.data') as $optionValue => $optionText)
                        <option value="{{ $optionValue }}">
                            {{ $optionText }}
                        </option>
                    @endforeach
                </select>
            </th>
        @elseif ( $type == 'date' )
            <th>
                <div class="input-group date">
                    @php $format = \App\Utils\Utils::dateTimeFormatBasedOnLanguage(); @endphp
                    <input type="text"
                           data-column-index="{{ $dataColumnIndex }}"
                           class="form-control form-filter input-sm fomo-datetime-filter fomo-list-page-date-filter"
                           data-date-format="{{ \App\Utils\Utils::convertToJSDateTimeFormat($format['date_format'], true) }}"
                           onkeydown="return false"
                           name="{{ $key }}">
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-clear-date-filter" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                    </span>
                </div>
            </th>
        @elseif ( $type == 'time' )
            <th>
                <div class="input-group">
                    <input type="text"
                           data-column-index="{{ $dataColumnIndex }}"
                           class="form-control form-filter input-sm fomo-list-page-time-filter"
                           name="{{ $key }}">
                </div>
            </th>
        @elseif ( $type == 'datetime' )
            <th>
                <div class="input-group date">
                    <input type="text"
                           data-column-index="{{ $dataColumnIndex }}"
                           class="form-control form-filter input-sm fomo-datetime-filter fomo-list-page-datetime-filter"
                           data-date-format="{{ \App\Utils\Utils::convertToJSDateTimeFormat($format['date_time_format'], true) }}"
                           onkeydown="return false"
                           name="{{ $key }}">
                    <span class="input-group-btn">
                        <button class="btn btn-sm btn-clear-date-filter" type="button">
                            <i class="fa fa-times"></i>
                        </button>
                    </span>
                </div>
            </th>
        @endif
    @endforeach

    @if ( isset($table['actions']) && count($table['actions']) > 0 )
        <th class="text-center">
            <a href="javascript" class="btn-filter-cancel">{{ __('Reset filter') }}</a>
        </th>
    @endif
</tr>
