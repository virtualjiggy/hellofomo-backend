@if ( !isset($table['columns']) || count($table['columns']) === 0 )
    <span>{{ __('Please set columns to display table') }}</span>
@else
    <div class="row">

        <div class="col-md-12">
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                @include('includes.datatable.heading-add-new')


                @if ( array_get($table, 'filter_navigation', true) )
                    <div class="row pull-right" id="filter-navigation">
                        <div class="col-md-12">
                            <div class="btn-group" data-toggle="buttons">
                                <a class="action-filter active" id="datatable-btn-all">{{ __('All') }}</a> | <a class="action-filter" id="datatable-btn-trashed">{{ __('Trashed') }}</a>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="portlet-body">
                    <div class="table-container">

                        @if ( array_get($table, 'checkbox_column', false) )
                            <div class="table-actions-wrapper">
                                <select data-service="{{ array_get($table, 'service', false) ? $table['service'] : ''}}" class="table-group-action-input form-control input-inline input-small input-sm">
                                    <option value="">{{ __('Bulk actions') }}</option>
                                    <option value="bulk_trash" data-method="{{ array_get($table, 'actions_ajax.bulk_trash_multi.method', '') != '' ? array_get($table, 'actions_ajax.bulk_trash_multi.method') : 'get' }}" data-ajax-url="{{ array_get($table, 'actions_ajax.bulk_trash_multi.route', '') != '' ? route(array_get($table, 'actions_ajax.bulk_trash_multi.route')) : '' }}">{{ __('Move to trash') }}</option>
                                    <option value="restore" data-method="{{ array_get($table, 'actions_ajax.restore_multi.method', '') != '' ? array_get($table, 'actions_ajax.restore_multi.method') : 'get' }}" data-ajax-url="{{ array_get($table, 'actions_ajax.restore_multi.route', '') != '' ? route(array_get($table, 'actions_ajax.restore_multi.route')) : '' }}" style="display: none;">{{ __('Restore') }}</option>
                                    <option value="delete" data-method="{{ array_get($table, 'actions_ajax.delete_multi.method', '') != '' ? array_get($table, 'actions_ajax.delete_multi.method') : 'get' }}" data-ajax-url="{{ array_get($table, 'actions_ajax.delete_multi.route', '') != '' ? route(array_get($table, 'actions_ajax.delete_multi.route')) : '' }}" style="display: none;">{{ __('Delete') }}</option>
                                    {{-- Only display these actions in News module --}}
                                    @if ( array_get($table, 'id', '') == 'news-datatable' )
                                        <option value="set_categories">{{ __('Set categories') }}</option>
                                        <option value="set_tags">{{ __('Set tags') }}</option>
                                    @endif
                                </select>
                                <button class="btn btn-sm default table-group-action-submit">
                                    <i class="fa fa-check"></i>&nbsp;{{ __('Apply') }}
                                </button>
                            </div>
                        @endif

                        <table class="table table-striped table-bordered table-hover table-checkable"
                               id="{{ array_get($table, 'id', 'default-datatable') }}"
                               data-sort-default="{{ array_get($table, 'order_default.column', 'id') }}"
                               data-sort-order-default="{{ array_get($table, 'order_default.order', 'asc') }}"
                               data-sort-index-column="{{ array_get($table, 'order_default.index_sort_column', 0) }}"
                               data-ajax-src="{{ array_get($table, 'ajax.src', '') != '' ? route(array_get($table, 'ajax.src')) : '' }}"
                               data-only-trashed="{{ array_get($table, 'ajax.only_trashed', '') }}"
                               data-record-per-page="{{config('elidev.default_record_per_page')}}">

                            <thead>
                            @include('includes.datatable.heading-text')
                            @include('includes.datatable.heading-filter')
                            </thead>

                            <tbody></tbody>
                        </table>
                    </div>
                </div>

                @include('includes.listing_confirm_delete_modal')

            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>

    </div>
@endif
