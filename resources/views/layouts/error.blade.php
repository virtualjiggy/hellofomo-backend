<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ isset($title) ? $title : '' }}</title>
    <link rel="icon" type="image/x-icon" href="assets/img/favicons/favicon.ico">
</head>
<body>
@yield('content')
</body>
</html>
