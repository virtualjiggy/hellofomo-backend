@extends('layouts.app')

@section('title', __('Thank you for signing up'))

@section('body_class')
    page-sign-up page-sign-up--thanks
@endsection


@section('content')
    <div class="signup-blk thky-blk">
        <div class="p-container">
            <h2 class="p-ttl">{{__('Thank you!')}}</h2>
            <div class="p-desc">
                {{ $thankYou }}
            </div>
        </div>
    </div>
@endsection
