@extends('layouts.app')

@section('title', __('Profile'))

@section('body_class')
    page-logged page-logged-personal js-page-logged
@endsection

@section('sub_header')
    @component('partial.components.sub_headers.green')
        @slot('row')
            <div class="blk-welcome">
                <p>{{ __('My Personal Settings') }}</p>
            </div>
        @endslot
    @endcomponent
@endsection

@php
    $classCss = 'hidden';
    $classMessageCss = '';
    if($user->avatar){
        $classCss = '';
        $classMessageCss = 'hidden';
    }

@endphp


@section('content')
    <div class="blk-personal--setting">
        <div class="container container-custom">
            <div class="blk-all-form frm-personal--setting">
                <form action="{{ route('users.update', ['id' => $user->id]) }}" data-url="{{ route('users.update', ['id' => $user->id]) }}" enctype="multipart/form-data" method="POST" id="edit-user-profile-form" >
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group field-group">
                                <input class="form-control ipt--field" type="text" name="name" value="{{$user->name}}" required>
                                <label class="label--field" for="name">* {{__('NAME')}}</label>
                                <label class="error" style="display: none" for="name"></label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group field-group">
                                <input class="form-control ipt--field"
                                       type="text"
                                       name="email"
                                       value="{{$user->email}}"
                                       data-ajax-url="{{route('emailExisted')}}"
                                       required
                                >
                                <label class="label--field" for="email">* {{__('EMAIL')}}</label>
                                <label class="error" style="display: none" for="email"></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group field-group">
                                <input class="form-control ipt--field" type="password" name="password" id="password" required>
                                <label class="label--field" for="password">{{__('New password')}}</label>
                                <label class="error" style="display: none" for="password"></label>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group field-group">
                                <input class="form-control ipt--field" type="password" name="password_confirmation" required>
                                <label class="label--field" for="password_confirmation">{{__('Repeat new password')}}</label>
                                <label class="error" style="display: none" for="password_confirmation"></label>
                            </div>
                        </div>
                    </div>
                    <div class="blk-upload--profile">


                        <div class="blk-upload--profile">
                            <a class="btn-upload-img js-upload--profile {{ !empty($user) && $user->avatar ? 'have-img' : '' }}"
                               type="button"
                            >
                                <span class="ttl--upload {{ $classMessageCss }}"> {{__('Upload')}} <br>{{__('Profile Photo')}}</span>
                                <span class="type--upload {{ $classMessageCss }}">@include('partial.image_extension_logo', compact('imageRules'))</span>
                                <input
                                        class="ipt-upl js-ipt--url"
                                        type="file"
                                        name="avatar"
                                        onchange="NameField.previewUpload(this)"
                                >
                                <span class="preview--img  {{$classCss}}" style="background-image: url('{{ $user->avatar }}');"></span>
                                <span class="remove-img--preview {{$classCss}} " onclick="NameField.removePreview(this)">
                                   <img src="{{ asset('img/ico-close-white.png') }}" alt="">
                                </span>
                            </a>

                            <br>
                            <p class="message">
                                <label class="error" style="display: none" for="avatar"></label>
                            </p>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group select-group"><label class="label-select--field" for="select-country">{{__('COUNTRY')}}</label>
                                <select class="form-control js-render-select--option" name="country" id="select-country" allow-search>
                                    <option value="-1" > {{__('Select')}}  </option>
                                    @foreach($countries as $country)
                                        @php
                                            $code = strtolower($country['code']);
                                            $name = strtolower($country['name']);
                                        @endphp
                                        <option value="{{ $code }}" @if(strtolower($user->country) == $code) selected @endif>{{ $country['name'] }}</option>
                                    @endforeach
                                </select>
                                <div class="wrapper-spinner wrapper-spinner-green load-data--dropdown"><i class="icon icon-circle-o-notch"></i></div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group field-group">
                                <input class="form-control ipt--field" value="{{$user->city}}" type="text" name="city"  required>
                                <label class="label--field" for="city">{{__('City')}}</label>
                                <label class="error" style="display: none" for="city"></label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <div class="form-group field-group @if($user->country == -1 || empty($user->country)) un-active @endif">
                                <input class="form-control ipt--field" type="text" id="phone"  data-country="" name="phone" value="{{$user->phone}}" @if($user->country == -1 || empty($user->country)) readonly="readonly" @endif>
                                <label class="label--field phone-label" for="phone">{{__('PHONE')}}</label>
                                <label class="error" style="display: none" for="phone"></label>
                            </div>
                        </div>

                    </div>
                    <div class="form-action-row">
                        <div class="message"></div>

                    <button class="btn btn-green btn-save" type="submit">{{__('Save')}}</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection


