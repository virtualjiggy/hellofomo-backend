@extends('layouts.app')

@section('title', __('Unsubscribe receiving notification email'))

@section('body_class')
    page-deed
@endsection

@section('content')
    <div class="signup-blk thky-blk">
        <div class="p-container">
            <div class="p-desc">{{ $message }}.</div>
        </div>
    </div>
@endsection



