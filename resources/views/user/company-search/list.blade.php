@if(isset($users) && count($users) > 0)
    @foreach($users as $user)
        <option value="{{ $user->id }}" @if ( in_array($user->id, $selectedUsers) ) selected @endif>{{ $user->name }}</option>
    @endforeach
@else
    <option>{{__('No results found')}}</option>
@endif
