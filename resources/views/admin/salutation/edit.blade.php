@extends('admin.layout.admin')

@section('title', __('Edit'))

@section('content')
    @include('admin.salutation.form', [
        'title'         => __('Edit'),
        'route'         => route('admin.salutations.update', ['category' => $category->id]),
        'btnSubmitText' => __('Save Changes'),
    ])
@endsection
