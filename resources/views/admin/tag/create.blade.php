@extends('admin.layout.admin')

@section('title', __('Create new tag'))

@section('content')
    @include('admin.tag.form', [
        'title'         => __('Create new tag'),
        'route'         => route('admin.tags.store'),
        'btnSubmitText' => __('Create'),
    ])
@endsection
