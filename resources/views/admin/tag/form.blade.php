<div class="row">
    <div class="col-md-12">
        <div class="profile-content">
            <div class="row">
                <form role="form" action="{{ empty($route) ? '' : $route }}" method="POST" class="form-horizontal form-validation submit-frm">
                    <div class="col-md-12">
                        <div class="portlet light bordered no-padding-bottom">
                            <div class="portlet-title">
                                <div class="caption font-green">{{ empty($title) ? __('Form Tag') : $title }}</div>
                                @include('admin.components.portlet-tools')
                            </div>

                            <div class="portlet-body form">
                                {{ csrf_field() }}

                                @if ( isset($tag) )
                                    {{ method_field('PUT') }}
                                @endif

                                <div class="form-body">
                                    <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label" for="name">{{ __('Name') }} *</label>
                                        <div class="col-md-8">
                                            <input type="text"
                                                   id="name"
                                                   name="name"
                                                   class="form-control"
                                                   value="{{ old('name', isset($tag->name) ? $tag->name : '') }}"
                                                   required>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block">{{ $errors->first('name') }}</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
