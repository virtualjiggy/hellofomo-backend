@extends('admin.layout.admin')

@section('title', $pageTitle ? $pageTitle : 'Overview')

@section('content')
    @include('includes.datatable.datatable')
@endsection
