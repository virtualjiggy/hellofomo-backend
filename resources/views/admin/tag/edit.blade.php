@extends('admin.layout.admin')

@section('title', __('Edit'))

@section('content')
    @include('admin.tag.form', [
        'title'         => __('Edit'),
        'route'         => route('admin.tags.update', ['tag' => $tag->id]),
        'btnSubmitText' => __('Save Changes'),
    ])
@endsection
