@extends('admin.layout.admin')

@section('title', __('Create new category'))

@section('content')
    @include('admin.category.form', [
        'title'         => __('Create new category'),
        'route'         => route('admin.categories.store'),
        'btnSubmitText' => __('Create'),
    ])
@endsection
