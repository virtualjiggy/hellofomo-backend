@extends('admin.layout.admin')

@section('title', $pageTitle ? $pageTitle : __("Overview"))

@section('content')
    @include('includes.datatable.datatable')
@endsection
