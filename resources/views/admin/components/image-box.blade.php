<div class="portlet light bordered no-padding-bottom" data-acl-role="files.read">
    <div class="portlet-title">
        <div class="caption font-green">{{ !empty($title) ? $title : '' }}</div>
        @include('admin.components.portlet-tools')
    </div>
    <div class="portlet-body form">
        <div id="crop-avatar">
            <div class="image-box avatar-view">
                <input type="hidden" name="image_id" value="{{ !empty($data) ? $data->image_id : '' }}" class="image-data">
                <img style="@if (!isset($data) || empty($data->image_src)) display: none; @endif" src="{{ !empty($data) ? get_image_url($data->image_src, 'thumb') : '' }}"
                     alt="{{ __('Preview image') }}"
                     class="rvMedia_preview_file preview_image img-responsive" data-rv-media="[@if (!empty($data)){{ json_encode(['selected_file_id' => $data->image_id, 'file_type' => 'image']) }}@else {{ json_encode(['file_type' => 'image']) }} @endif]"  />

                <div class="image-box-actions">
                    @php
                        $displayBrowser = empty($data->image_src);
                    @endphp
                    <a style="@if(!$displayBrowser) display: none @endif" href="javascript:;" class="btn-upload-image" data-rv-media="[{{ json_encode([ 'file_type' => 'image']) }}]">{{ __('Browse an image') }}</a>
                    <a style="@if($displayBrowser) display: none @endif" href="javascript:;" class="btn-remove-image">{{ __('Remove the image') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
