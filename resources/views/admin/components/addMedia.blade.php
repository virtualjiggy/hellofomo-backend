<a data-acl-role="files.read" href="javascript:;"
   class="btn btn-default tooltips green add-media-to-textbox"
   data-add-media="app-add-media"
   data-original-title="Add media"
   data-destination="{{ isset($mediaDestination) ? $mediaDestination : '#description' }}"
   data-rv-media="[{{ json_encode([ 'file_type' => 'image']) }}]"
><i class="fa fa-picture-o" aria-hidden="true"></i> {{ __('Add Media') }}
</a>
<style>
    .add-media-to-textbox{
        margin-bottom: 5px;
    }
</style>
@section('scripts')
    <script src="{{ asset('admin-assets/internal/js/jquery.addMedia.js') }}" type="text/javascript"></script>
@endsection