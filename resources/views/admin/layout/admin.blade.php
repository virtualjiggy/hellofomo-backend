<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->

<!-- BEGIN HEAD -->
<head>
    @include('admin.layout.include.head')
</head>
<!-- END HEAD -->


<body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white" data-gr-c-s-loaded="true" cz-shortcut-listen="true" style="">
<div class="page-wrapper">
    <!-- BEGIN HEADER -->
    @include('admin.partials.page_header')
    <!-- END HEADER -->

    <!-- BEGIN HEADER & CONTENT DIVIDER -->
    <div class="clearfix"> </div>
    <!-- END HEADER & CONTENT DIVIDER -->

    <!-- BEGIN CONTAINER -->
    <div class="page-container">
        <!-- BEGIN SIDEBAR -->
        @include('admin.partials.left.sidebar')
        <!-- END SIDEBAR -->

        <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <!-- BEGIN CONTENT BODY -->
            <div class="page-content" style="min-height: calc(100vh - 85px);">
                <!-- BEGIN PAGE HEADER-->

                <!-- BEGIN PAGE BAR -->
                @include('admin.partials.breadcrumb')
                <!-- END PAGE BAR -->

                <!-- BEGIN PAGE TITLE-->
                <h1 class="page-title">@yield('main_title')</h1>
                <!-- END PAGE TITLE-->

                <div class="row">
                    <div class="col-md-12">
                        @include('includes.form_message')
                    </div>

                </div>


                @yield('content')


            </div>
            <!-- END CONTENT BODY -->
        </div>
        <!-- END CONTENT -->

    </div>
    <!-- END CONTAINER -->

    <!-- BEGIN FOOTER -->
    @include('admin.partials.page_footer')
    <!-- END FOOTER -->
</div>

<!-- BEGIN JAVASCRIPTS -->
@include('admin.layout.include.footer')
<!-- END JAVASCRIPTS -->

@stack('footer')

</body>
