<script src="/admin-assets/global/plugins/respond.min.js"></script>
<script src="/admin-assets/global/plugins/ie8.fix.min.js"></script>
<!-- BEGIN CORE PLUGINS -->
<script src="/admin-assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<script src="/admin-assets/internal/js/init_ajax.js"></script>


<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="/admin-assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/moment.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.de.js" charset="UTF-8" type="text/javascript"></script>

<script src="/admin-assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
@if ( Session::get('locale') == 'de' )
    <script src="/admin-assets/global/plugins/jquery-validation/js/localization/messages_de.min.js" type="text/javascript"></script>
@endif
<script src="/admin-assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->


<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="/admin-assets/global/scripts/app.min.js" type="text/javascript"></script>
<!-- END THEME GLOBAL SCRIPTS -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="/admin-assets/pages/scripts/components-bootstrap-select.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/icheck/icheck.min.js" type="text/javascript"></script>

<script src="/admin-assets/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>
<script src="/admin-assets/pages/scripts/ui-modals.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-toastr/toastr.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/jquery.pulsate.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/sortable/Sortable.min.js" type="text/javascript"></script>
<script src="/admin-assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="/admin-assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<!-- END THEME LAYOUT SCRIPTS -->

<!-- DEV JS -->
<script src="/vendor/acl/js/acl.js"></script>

<script src="{{ '/admin-assets/internal/js/app.js' }}"></script>
@yield('scripts')

@push('footer')
    @include('media::partials.media')
@endpush

<script>
    var alertTitle = "{{ __("Are you sure?") }}";
    var alertText = "{{ __("Yes, delete it!") }}";
</script>
