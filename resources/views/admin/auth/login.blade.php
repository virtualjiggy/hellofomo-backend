@extends('admin.layout.login')

@section('content')
<!-- BEGIN LOGIN -->
<div class="content">
    <form class="login-form" role="form" method="POST" action="{{ route('admin.login') }}">
        {{ csrf_field() }}

        <h3 class="form-title font-green">{{ __('Sign In') }}</h3>

        <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
            <input id="email" type="email" class="form-control" placeholder="{{ __('Email') }}" name="email" value="{{ old('email') }}" required autofocus>

            @if ( $errors->has('email') )
                <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
            <input type="password" class="form-control" name="password" placeholder="{{ __('Password') }}" required>

            @if ( $errors->has('password') )
                <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
        </div>

        <div class="form-actions">
            <button type="submit" class="btn green uppercase">{{ __('Login') }}</button>
            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" value="1">{{ __('Remember me') }}
                <span></span>
            </label>
        </div>

        <a href="{{ route('admin.password.request') }}" id="forget-password" class="forget-password">{{ __('Forgot password?') }}</a>
    </form>
    <!-- /.login-box-body -->
</div>
<!-- END LOGIN -->
@endsection
