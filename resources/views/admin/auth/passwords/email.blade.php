@extends('admin.layout.login')

@section('content')
    <div class="content">
        <form class="reset-password-form" role="form" method="POST" action="{{ route('admin.password.email') }}">
            {{ csrf_field() }}

            <h3 class="form-title font-green">{{ __('Forgot password') }}</h3>

            @php $className = !empty(session('status')) ? 'has-success' : ''; @endphp
            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : $className }}">
                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="{{ __('Email address') }}" required autofocus>

                @if ( $errors->has('email') )
                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                @endif

                @if ( session('status') )
                    <span class="help-block">{{ session('status') }}</span>
                @endif
            </div>

            <div class="form-actions">
                <button type="submit" class="btn green">{{ __('Send password reset link') }}</button>
            </div>

            <a href="{{ route('admin-show-login') }}" id="forget-password" class="forget-password">{{ __('Go back to login page') }}</a>
        </form>
    </div>
@endsection
