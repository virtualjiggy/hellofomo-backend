@extends('admin.layout.login')

@section('content')
    <div class="content">
        <form class="reset-pass-frm" role="form" method="POST" action="{{ route('admin.reset.password') }}">
            {{ csrf_field() }}
            <input type="hidden" name="token" value="{{ $token }}">

            <h3 class="form-title font-green">{{ __('Reset your password') }}</h3>

            <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                <input id="email" type="email" class="form-control" name="email" placeholder="{{ __('Email address') }}" value="{{ old('email') }}" required autofocus>

                @if ( $errors->has('email') )
                    <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
                @endif
            </div>

            <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                <input id="password" type="password" class="form-control" name="password" placeholder="{{ __('Password') }}" required>

                @if ( $errors->has('password') )
                    <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
                @endif
            </div>

            <div class="form-group has-feedback {{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" placeholder="{{ __('Confirm Password') }}" required>

                @if ( $errors->has('password_confirmation') )
                    <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
                @endif
            </div>

            <div class="form-actions">
                <button type="submit" class="btn green">{{ __('Reset password') }}</button>
            </div>
        </form>
    </div>
@endsection
