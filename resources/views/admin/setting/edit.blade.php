@extends('admin.layout.admin')

@php
    $title            = __('Edit Setting');
    $supportLanguages = config('fomo.support_languages');
    $dateTimeKeyEn    = config('fomo.settings.date_time_en');
    $dateTimeKeyDe    = config('fomo.settings.date_time_de');
    $languageKey      = config('fomo.settings.language');
    $file_types       = config('fomo.settings.news_files_types');
@endphp

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <form role="form" action="{{ route('admin.settings.update') }}" method="POST" class="form-horizontal form-validation submit-frm">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ empty($title) ? __('Form General Settings') : $title }}</div>
                                    <div class="actions">
                                        @include('admin.components.full-screen')
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <div class="tabbable-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_1_1" data-toggle="tab"> {{ __('General') }} </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_1_2" data-toggle="tab"> {{ __('Date & Time') }} </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_1_3" data-toggle="tab"> {{ __('News') }} </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1_1_1">
                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has($languageKey) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $languageKey }}">{{ __('Language') }}</label>

                                                    <div class="col-md-6">
                                                        <select name="{{ $languageKey }}" id="{{ $languageKey }}" class="form-control">
                                                            @foreach($supportLanguages as $name => $code)
                                                                <option value="{{ $code }}" @if (isset($settings[$languageKey]) && $settings[$languageKey] == $code) selected @endif >{{ $name }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($languageKey) }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab_1_1_2">
                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has($dateTimeKeyEn) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $dateTimeKeyEn }}">{{ __('English') }}</label>

                                                    <div class="col-md-6">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $dateTimeKeyEn }}"
                                                               id="{{ $dateTimeKeyEn }}"
                                                               placeholder="{{ __('Y/m/d H:i') }}"
                                                               value="{{ old($dateTimeKeyEn, isset($settings[$dateTimeKeyEn]) ? $settings[$dateTimeKeyEn] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($dateTimeKeyEn) }}</span>
                                                    </div>
                                                </div>

                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has($dateTimeKeyDe) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $dateTimeKeyDe }}">{{ __('Germany') }}</label>

                                                    <div class="col-md-6">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $dateTimeKeyDe }}"
                                                               id="{{ $dateTimeKeyDe }}"
                                                               placeholder="{{ __('d.m.Y H:i') }}"
                                                               value="{{ old($dateTimeKeyDe, isset($settings[$dateTimeKeyDe]) ? $settings[$dateTimeKeyDe] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($dateTimeKeyDe) }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab_1_1_3">
                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has($file_types) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $file_types }}">{{ __('Files Types') }}</label>

                                                    <div class="col-md-6">
                                                        <input type="text"
                                                               class="form-control"
                                                               name="{{ $file_types }}"
                                                               id="{{ $file_types }}"
                                                               placeholder="{{ __('For example: png,jpeg,jpg,gif') }}"
                                                               value="{{ old($file_types, isset($settings[$file_types]) ? $settings[$file_types] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($file_types) }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
