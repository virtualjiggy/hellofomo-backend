@extends('admin.layout.admin')

@section('title', __('Users Overview'))

@section('content')
    @include('includes.datatable.datatable')
@endsection
