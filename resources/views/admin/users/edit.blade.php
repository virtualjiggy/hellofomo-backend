@extends('admin.layout.admin')

@section('title', __('User Edit'))

@section('content')
    @include('admin.users.form', [
        'title'         => __('Edit user'),
        'route'         => route('admin.users.update', ['user' => $user->id]),
        'btnSubmitText' => __('Save Changes'),
    ])
@endsection
