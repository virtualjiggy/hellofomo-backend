<div class="row">
    <div class="col-md-12">
        <div class="profile-content">
            <form role="form" action="{{ empty($route) ? '' : $route }}" method="POST" class="form-horizontal form-validation submit-frm" enctype="multipart/form-data">
                <div class="col-md-9">
                    <div class="portlet light bordered no-padding-bottom">

                        <div class="portlet-title">
                            <div class="caption font-green">{{ empty($title) ? __('Form User') : $title }}</div>
                            @include('admin.components.portlet-tools')
                            <div class="actions">
                                <button class="btn btn-icon-only btn-default fullscreen" data-original-title="Vollbild" title="{{ __('Full screen') }}"></button>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            {{ csrf_field() }}

                            @if ( isset($user) )
                                {{ method_field('PUT') }}
                            @endif
                            <div class="form-body">

                                <div class="form-group  {{ $errors->has('name') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="name">{{ __('Name') }} *</label>
                                    <div class="col-md-9">
                                        <input type="text"
                                               id="name"
                                               name="name"
                                               maxlength="50"
                                               class="form-control"
                                               value="{{ old('name', isset($user->name) ? $user->name : '') }}"
                                               required>
                                        <div class="form-control-focus"></div>
                                        <span class="help-block">{{ $errors->first('name') }}</span>
                                    </div>
                                </div>

                                <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="email">{{ __('Email') }} *</label>
                                    <div class="col-md-9">
                                        <input type="text"
                                               id="email"
                                               name="email"
                                               maxlength="100"
                                               class="form-control"
                                               data-rule-email="true"
                                               value="{{ old('email', isset($user->email) ? $user->email : '') }}"
                                               required>
                                        <div class="form-control-focus"></div>
                                        <span class="help-block help-block-error has-email-error">{{ $errors->first('email') }}</span>
                                    </div>
                                </div>

                                <div class="form-group {{ $errors->has('language') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="language">{{ __('Language') }}</label>
                                    <div class="col-md-9">
                                        <select name="language" class="form-control select2" style="width: 100%">
                                            <option value="">{{ __('Select') }}</option>
                                            <option value="de" {{ isset($user) && !empty($user->language) && $user->language == "de" ?"selected" : "" }}>{{ __('DE') }}</option>
                                            <option value="en" {{ isset($user) && !empty($user->language) && $user->language == "en" ?"selected" : "" }}>{{ __('EN') }}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="password">{{ __('Password') }} {{!isset($user) ? '*' : ''}}</label>
                                    <div class="col-md-9">
                                        <input type="password"
                                               id="password"
                                               name="password"
                                               class="form-control"
                                               value=""
                                               minlength="6"
                                               maxlength="100"
                                               {{!isset($user) ? 'required' : ''}}>
                                        <div class="form-control-focus"></div>
                                        <span class="help-block">{{ $errors->first('password') }}</span>
                                    </div>
                                </div>

                                <div class="form-group  {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="password_confirmation">{{ __('Password confirmation') }} {{!isset($user) ? '*' : ''}}</label>
                                    <div class="col-md-9">
                                        <input type="password"
                                               id="password_confirmation"
                                               name="password_confirmation"
                                               class="form-control"
                                               minlength="6"
                                               maxlength="100"
                                               equalTo="#password"
                                               value=""
                                               {{!isset($user) ? 'required' : ''}}>
                                        <div class="form-control-focus"></div>
                                        <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                    </div>
                                </div>

                                <div class="form-group  {{ $errors->has('phone') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="phone">{{ __('Phone') }}</label>
                                    <div class="col-md-9">
                                        <input type="text"
                                               id="phone"
                                               name="phone"
                                               maxlength="100"
                                               class="form-control"
                                               value="{{ old('phone', isset($user->phone) ? $user->phone : '') }}"
                                               >
                                        <div class="form-control-focus"></div>
                                        <span class="help-block">{{ $errors->first('phone') }}</span>
                                    </div>
                                </div>

                                <div class="form-group  {{ $errors->has('status') ? 'has-error' : '' }}">
                                    <label class="col-md-3 control-label" for="status">{{ __('Active') }}</label>
                                    <div class="col-md-9">
                                        <div class="md-checkbox">
                                            <input type="checkbox" id="status" name="status" class="md-check" {{ old('status', (isset($user) && $user->status == $list_default_status['publish'])) ? 'checked' : ''}}>
                                            <label for="status">
                                                <span></span>
                                                <span class="check"></span>
                                                <span class="box"></span></label>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                    </div>
                </div>
                <div class="col-md-3">
                    @include('admin.components.image-box', ['title' => __('Avatar'), 'data' => isset($user) ? $user : ''])
                </div>
            </form>

        </div>
    </div>
</div>

@push('footer')
    @include('media::partials.media')
@endpush
