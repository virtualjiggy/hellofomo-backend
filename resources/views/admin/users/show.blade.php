@extends('admin.layout.admin')

@section('title', __('User Detail'))

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <div class="col-md-9">
                        <div class="portlet light bordered no-padding-bottom">
                            <div class="portlet-title">
                                <div class="caption font-green">{{ __('User data') }}</div>
                                @include('admin.components.portlet-tools')
                                <div class="actions">
                                    <button class="btn btn-icon-only btn-default fullscreen" data-original-title="Vollbild" title="{{ __('Full screen') }}"></button>
                                </div>
                            </div>

                            <div class="portlet-body form">
                                <table class="table fomo-show-author-tbl">
                                    <tbody>
                                    <tr>
                                        <th width="10%">{{ __('Name') }}</th>
                                        <td>{{ $user->name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="10%">{{ __('Email') }}</th>
                                        <td>{{ $user->email }}</td>
                                    </tr>
                                    <tr>
                                        <th width="10%">{{ __('Phone') }}</th>
                                        <td>{{ $user->phone }}</td>
                                    </tr>
                                    <tr>
                                        <th width="10%">{{ __('Status') }}</th>
                                        <td>{{ ($user->status == config('elidev.list_default_status.publish')) ? __('Activated') : __("Pending") }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-3">
                        <div class="portlet light bordered no-padding-bottom">
                            <div class="portlet-title">
                                <div class="caption font-green">{{ __('Avatar') }}</div>
                                @include('admin.components.portlet-tools')
                            </div>

                            <div class="portlet-body form">
                                <div id="crop-avatar">
                                    @if ($user->image_src)
                                        <div class="form-body">
                                            <div class="form-group">
                                                <!-- CURRENT AVATAR -->
                                                <div class="avatar-view">
                                                    <img class="img-responsive" src="{{ $user->image_src }}" alt="{{ __('Avatar') }}">
                                                </div>
                                            </div>
                                        </div>
                                    @else
                                        {{ __("Not available") }}
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection