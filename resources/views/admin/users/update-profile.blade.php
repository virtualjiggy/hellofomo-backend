<div class="profile-content">
    <div class="portlet-body form">
        <div class="form-body">
            <div class="form-group" style="overflow: hidden">
                <label class="col-md-2 control-label" for="name">{{ __('Avatar') }} *</label>
                <div class="col-md-10">
                    <div id="crop-avatar">
                        <div class="image-box avatar-view">
                            @php
                                $dataRVMedia = ['view_in' => 'my_media'];
                                if (!empty($user->image_src)) {
                                    $dataRVMedia['selected_file_id'] = $user->image_id;
                                }
                            @endphp
                            <input type="hidden" name="image_id" value="{{ $user->image_id ? $user->image_id : '' }}" class="image-data">
                            <img style="width: 150px; height: 150px;" src="{{ $user->image_id ? get_image_url($user->image_src, 'thumb') : asset(config('fomo.image.avatar_default')) }}"
                                 alt="{{ __('Preview image') }}"
                                 class="rvMedia_preview_file rvMedia_preview_file_avatar preview_image img-responsive"
                                 data-default-src="{{ asset(config('fomo.image.avatar_default')) }}"
                                 data-rv-media="[{{ json_encode($dataRVMedia) }}]"  />

                            <div class="image-box-actions">
                                @php
                                    $displayBrowser = empty($user->image_src);
                                @endphp
                                <a style="@if($displayBrowser) display: none @endif" href="javascript:;" class="btn-remove-image">{{ __('Remove the image') }}</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="name">{{ __('Name') }} *</label>
                <div class="col-md-10">
                    <input type="text"
                           id="name"
                           name="name"
                           maxlength="50"
                           class="form-control"
                           value="{{ old('name', isset($user->name) ? $user->name : '') }}"
                           required>
                    <div class="form-control-focus"></div>
                    <span class="help-block">{{ $errors->first('name') }}</span>
                </div>
            </div>

            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="email">{{ __('Email') }} *</label>
                <div class="col-md-10">
                    <input type="text"
                           id="email"
                           name="email"
                           maxlength="100"
                           class="form-control"
                           data-rule-email="true"
                           value="{{ old('email', isset($user->email) ? $user->email : '') }}"
                           required>
                    <div class="form-control-focus"></div>
                    <span class="help-block help-block-error has-email-error">{{ $errors->first('email') }}</span>
                </div>
            </div>

            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="email">{{ __('Language') }}</label>
                <div class="col-md-10">
                    <select name="language" class="form-control select2" style="width: 100%">
                        <option value="">Select language</option>
                        <option value="de" {{ $user->language == "de" ?"selected" : "" }}>{{ __("DE") }}</option>
                        <option value="en" {{ $user->language == "en" ?"selected" : "" }}>{{ __("EN") }}</option>
                    </select>

                </div>
            </div>

            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="password">{{ __('Password') }} {{!isset($user) ? '*' : ''}}</label>
                <div class="col-md-10">
                    <input type="password"
                           id="password"
                           name="password"
                           class="form-control"
                           value=""
                           minlength="6"
                           maxlength="100"
                            {{!isset($user) ? 'required' : ''}}>
                    <div class="form-control-focus"></div>
                    <span class="help-block">{{ $errors->first('password') }}</span>
                </div>
            </div>

            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="password_confirmation">{{ __('Password confirmation') }} {{!isset($user) ? '*' : ''}}</label>
                <div class="col-md-10">
                    <input type="password"
                           id="password_confirmation"
                           name="password_confirmation"
                           class="form-control"
                           minlength="6"
                           maxlength="100"
                           equalTo="#password"
                           value=""
                            {{!isset($user) ? 'required' : ''}}>
                    <div class="form-control-focus"></div>
                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                </div>
            </div>

            <div class="form-group form-md-line-input">
                <label class="col-md-2 control-label" for="phone">{{ __('Phone') }}</label>
                <div class="col-md-10">
                    <input type="text"
                           id="phone"
                           name="phone"
                           maxlength="100"
                           class="form-control"
                           value="{{ old('phone', isset($user->phone) ? $user->phone : '') }}"
                    >
                    <div class="form-control-focus"></div>
                    <span class="help-block">{{ $errors->first('phone') }}</span>
                </div>
            </div>
        </div>
    </div>
</div>