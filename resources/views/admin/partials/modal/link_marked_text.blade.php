<div class="modal fade" id="fomo-link-marked-text" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div id="link-marked-text-form">
            {{ csrf_field() }}
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">{{ __('Link marked text') }}</h4>
                </div>

                <div class="modal-body row">
                    <div class="form-group col-md-12">
                        <label class="col-md-2 control-label">{{ __('News: ') }}</label>
                        <div class="col-md-10">
                            <select name="fomo-news-internal-link"
                                    id="fomo-news-internal-link"
                                    class="form-control select2"
                                    data-ajax-src="{{ route('admin.news.search') }}"
                                    data-news-detail-url="{{ route('admin.news.edit', ['news' => -1]) }}"
                                    data-placeholder="{{ __('Search by news title') }}"
                                    data-per-page="{{ config('elidev.default_limit', 10) }}">
                            </select>
                            <div class="form-control-focus"></div>
                            <span class="help-block"></span>
                        </div>
                    </div>
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline btn-close" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="button" class="btn green btn-link-marked-text" action="0" pattern-href="{{config('elidev.pattern_href_link_marked_text')}}">{{ __('Apply') }}</button>
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
