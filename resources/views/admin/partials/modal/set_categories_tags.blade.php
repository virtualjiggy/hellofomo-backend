<div class="modal fade" id="fomo-news-listing-set-categories-tags" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <form method="" action="" id="apply-categories-tags-form">
            {{ csrf_field() }}
            <input type="hidden" name="news" id="news"/>
            <input type="hidden" name="type" id="category"/>
            <input type="hidden" name="ajax-url" id="ajax-url" value="{{ route('admin.news.apply') }}"/>

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">{{ __('Modal Title') }}</h4>
                </div>

                <div class="modal-body">

                    {{-- Render categories checkboxes --}}
                    @if ( isset($categories) && count($categories) > 0 )
                        <div class="categories-list hidden">
                            <div>
                                @foreach($categories as $key => $category)
                                    <label>
                                        <input type="checkbox" name="categories[]" id="category-checkbox-{{ $key }}"  data-id="{{ $key }}" value="{{ $key }}"/>
                                        <span></span>{{--do not remove this line--}}
                                        {{ $category }}
                                    </label>
                                    <br/>
                                @endforeach
                            </div>
                        </div>
                    @endif

                    {{-- Render tags checkboxes --}}
                    @if ( isset($tags) && count($tags) > 0 )
                        <div class="tags-list hidden">
                            <div>
                                @foreach($tags as $key => $tag)
                                    <label>
                                        <input type="checkbox" name="tags[]" id="tag-checkbox-{{ $key }}"  data-id="{{ $key }}" value="{{ $key }}"/>
                                        <span></span>{{--do not remove this line--}}
                                        {{ $tag }}
                                    </label>
                                    <br/>
                                @endforeach
                            </div>
                        </div>
                    @endif
                </div>

                <div class="modal-footer">
                    <button type="button" class="btn dark btn-outline btn-close" data-dismiss="modal">{{ __('Close') }}</button>
                    <button type="button" class="btn green btn-set-categories-tags">{{ __('Apply') }}</button>
                </div>
            </div>
        </form>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
