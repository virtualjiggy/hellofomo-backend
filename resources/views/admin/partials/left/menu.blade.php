@foreach($menus as $menu)
    @if (isset($menu['children']) && !empty($menu['children']))
        <li class="treeview @if ($menu['active']) active @endif">
            <a href="{{ $menu['url'] }}">
                @if (!empty($menu['icon']))<i class="{{ $menu['icon'] }}"></i> @endif<span>{{ __($menu['name']) }}</span>
                <span class="pull-right-container"><i class="fa fa-angle-left pull-right"></i></span>
            </a>

            <ul class="treeview-menu">
                @foreach (array_get($menu, 'children', []) as $child)
                    <li class="@if ($child['active']) active @endif">
                        <a href="{{ $child['url'] }}">
                            <i class="fa fa-circle-o"></i> <span>{{ __($child['name']) }}</span>
                        </a>
                    </li>
                @endforeach
            </ul>
        </li>
    @else
        <li class="@if ($menu['active']) active @endif">
            <a href="{{ $menu['url'] }}">
                @if (!empty($menu['icon']))<i class="{{ $menu['icon'] }}"></i> @endif<span>{{ __($menu['name']) }}</span>
            </a>
        </li>
    @endif
@endforeach
