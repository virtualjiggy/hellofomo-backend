<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="fa fa-home"></i>
            <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a>

            @if ( isset($breadcrumbs) )
                <i class="fa fa-angle-right"></i>
            @endif
        </li>

        @if ( isset($breadcrumbs) && count($breadcrumbs) > 0 )
            @foreach($breadcrumbs as $key => $breadcrumb)
                <li>
                    @if ( array_get($breadcrumb, 'url', '') == '' )
                        <span>{{ array_get($breadcrumb, 'text', '') }}</span>
                    @else
                        <a href="{{ array_get($breadcrumb, 'url', 'javascript:;') }}">{{ array_get($breadcrumb, 'text', '') }}</a>
                    @endif
                </li>
                @if ( !$loop->last )
                    <li><i class="fa fa-angle-right"></i></li>
                @endif
            @endforeach
        @endif
    </ul>

    <?php
    try {
        $params = request()->route()->parameters;
        $routeName = request()->route()->getName();
        list(, $mod, $action) = explode('.', $routeName);
        $key = key($params);
        $id = reset($params);
    }
    catch(Exception $e) {}
    ?>
    @if (isset($action))

        <?php
        try {

            $isACLRoute = strpos(\Request::route()->getName(), 'acl.');
            $prefix = $isACLRoute !== false ? 'acl':'admin';
            $createRoute = route(sprintf("%s.$mod.create", $prefix));
        }
        catch(Exception $e){}
        ?>
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <div class="actions">

                    {{-- Save App dashboard layout settings >> --}}
                    @if ( $routeName == 'admin.app.layout.settings' )
                        @php $routeKey = Request::is('admin/app/layout/settings/dashboard') ? 'dashboard' : 'news'; @endphp
                        <a href="javascript:;"
                           id="fomo-save-app-dashboard-layout-btn"
                           class="btn btn-icon-only btn-default tooltips"
                           data-placement="bottom"
                           data-original-title="{{ __("Save") }}"
                           data-ajax-url="{{ route('admin.app.layout.settings.save', ['key' => $routeKey]) }}">
                            <i class="fa fa-save"></i>
                        </a>
                    @endif
                    {{-- Save App dashboard layout settings << --}}

                    {{-- Save App layout setting contact --}}
                    @if ( in_array($routeName, ['admin.settings.edit', 'admin.app.layout.setting', 'admin.app.settings.edit']) )
                        <a id="fomo-save-btn" data-placement="bottom" data-original-title="{{ __("Save") }}" href="" class="btn btn-icon-only btn-default tooltips ">
                            <i class="fa fa-save"></i>
                        </a>
                    @endif
                    {{-- Save App layout setting contact --}}


                    @if ($action == 'show')
                        <?php
                        try {
                            $editRoute = !empty($key) && !empty($id) ? route('admin.'.$mod.'.edit', [$key  => $id]) : '';
                        }
                        catch(Exception $e) {}
                        ?>
                        @if (!empty($editRoute))
                            <a data-placement="bottom" data-original-title="{{ __("Edit") }}" data-acl-role="{{ $mod }}.edit" href="{{ $editRoute }}" class="btn btn-icon-only btn-default tooltips">
                                <i class="fa fa-edit"></i>
                            </a>
                        @endif
                    @endif

                    @if (($action == 'create' || $action == 'edit' || $action == 'edit-assign') && $routeName != 'admin.settings.edit' )
                        <?php $action = $action != 'create' ? 'edit':$action ?>
                        <a id="fomo-save-btn" data-placement="bottom"
                           data-error-msg="{{ __("There are some errors in the form, please review") }}"
                           data-original-title="{{ __("Save") }}" data-acl-role="{{ $mod }}.{{$action}}" href="" class="btn btn-icon-only btn-default tooltips {{ isset($overrideClass) && $overrideClass ? 'fomo-override-save-action' : '' }}" data-ajax-url="{{ isset($dataAjaxUrl) ? $dataAjaxUrl : '' }}" user-id="{{ isset($user) ? $user->id : '' }}">
                            <i class="fa fa-save"></i>
                        </a>
                    @endif

                    @if (!empty($id) && !empty($key) && $action != 'show' && !in_array($mod, ['tags', 'categories', 'salutations']))
                        <?php
                        try {
                            $viewRoute = route('admin.'.$mod.'.show', [$key  => $id]);
                        }
                        catch(Exception $e) {}
                        ?>
                        @if (!empty($viewRoute))
                            <a id="fomo-view-btn" data-placement="bottom" data-original-title="{{ __("View") }}" data-acl-role="{{ $mod }}.read" href="{{ isset($viewRoute) ? $viewRoute : '' }}" class="btn btn-icon-only btn-default tooltips">
                                <i class="fa fa-search"></i>
                            </a>
                        @endif
                    @endif

                    @if ($action != 'create' && !empty($createRoute))
                        <a id="fomo-create-btn" data-placement="bottom" data-original-title="{{ __("New") }}" data-acl-role="{{ $mod }}.create" href="{{ $createRoute }}" class="btn btn-icon-only btn-default tooltips green">
                            <i class="fa fa-plus"></i>
                        </a>
                    @endif

                    @if (($action == 'edit' || $action == 'show') && !empty($key) && !empty($id))
                        <?php
                        try {
                            $trashRoute = route('admin.'.$mod.'.trash', [$key  => $id]);
                        }
                        catch(Exception $e) {}
                        ?>
                        @if (! empty($trashRoute))
                            <a data-placement="bottom" data-original-title="{{ __("Move to trash") }}" data-acl-role="{{ $mod }}.trash" href="{{ isset($trashRoute) ? $trashRoute : '' }}" class="btn btn-icon-only btn-default tooltips red-intense">
                                <i class="fa fa-trash"></i>
                            </a>
                        @endif
                    @endif

                    @if (in_array($action, ['edit', 'show']) && in_array($mod, ['categories', 'salutations', 'tags']))
                        <?php
                        try {
                            $destroyRoute = route('admin.'.$mod.'.destroy', [$key  => $id]);
                        }
                        catch(Exception $e) {}
                        ?>
                        @if (!empty($destroyRoute))
                            <a id="fomo-single-delete-btn" data-placement="bottom" data-original-title="{{ __("Delete forever") }}" data-acl-role="{{ $mod }}.destroy" href="" class="btn btn-icon-only btn-default tooltips red-intense">
                                <i class="fa fa-remove"></i>
                            </a>
                            <div class="modal fade" id="fomo-single-confirm-delete-modal" tabindex="-1" role="basic" aria-hidden="true">
                                <div class="modal-dialog">
                                    <form method="POST" action="{{ $destroyRoute }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                                <h4 class="modal-title">{{ __("Delete permanently") }}</h4>
                                            </div>
                                            <div class="modal-body">{{ __('Do you want to delete permanently?') }}</div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn dark btn-outline btn-close" data-dismiss="modal">{{ __('Close') }}</button>
                                                <button type="submit" class="btn red btn-action">{{ __('Delete permanently') }}</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endif
                    @endif

                </div>
            </div>
        </div>
    @endif

</div>
