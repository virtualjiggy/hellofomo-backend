<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner">
        &copy; {{ date('Y') }} {{ __('Fomosapiens') }}
    </div>
</div>
<!-- END FOOTER -->
