@extends('admin.layout.admin')

@section('styles')
    <link href="{{ asset('admin-assets/pages/css/blog.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <div id="app-layout-dashboard-wrapper" class="col-md-6">
                        <div class="portlet light bordered no-padding-bottom">
                            <div class="portlet-title">
                                <div class="caption font-green">{{ $title }}</div>
                                @include('admin.components.portlet-tools')
                            </div>

                            <div id="app-layout-dashboard-portlet-body" class="portlet-body form">
                                <form role="form" action="" method="POST" id="frm-new-section" class="form-validation submit-frm" enctype="multipart/form-data">
                                    @if ( !empty($layoutSettings) )
                                        @foreach($layoutSettings as $setting)
                                            <div id="frm-section{{ $loop->index }}" class="frm-section">
                                                <div class="portlet box box-item-layout-section">
                                                    <div class="portlet-title">
                                                        <div class="caption text-capitalize font-dark">{{ $setting['section_name'] }}</div>
                                                        @include('admin.components.portlet-tools')
                                                        <div class="actions">
                                                            <a data-placement="top" data-original-title="{{ __('Delete') }}" class="delete-section tooltips font-red"><i class="fa fa-times"></i></a>
                                                        </div>
                                                    </div>

                                                    <div class="portlet-body form">
                                                        @include('admin.app.includes.new_section_detail', [
                                                            'index'           => $loop->index,
                                                            'setting'         => $setting,

                                                            // when rendering the right panel, no need to check the "Data" type is category or tag. Default is category.
                                                            // when rendering each section of the left panel, need to check the "Data" type is category or tag.
                                                            'isCheckDataType' => true,
                                                        ])
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        @php unset($setting) @endphp
                                    @endif
                                </form>
                            </div>
                        </div>
                    </div>

                    <div id="app-layout-new-action-wrapper" class="col-md-6">
                        @include('admin.app.includes.new_section')
                    </div>

                    <div id="box-item-container" class="hidden frm-section">
                        <div class="portlet box box-item-layout-section">
                            <div class="portlet-title">
                                <div class="caption text-capitalize font-dark">{{ __('News List') }}</div>
                                @include('admin.components.portlet-tools', ['class_portlet' => 'expand'])
                                <div class="actions">
                                    <a data-placement="top" data-original-title="{{ __('Delete') }}" class="delete-section tooltips font-red"><i class="fa fa-times"></i></a>
                                </div>
                            </div>

                            <div class="portlet-body form" style="display: none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
