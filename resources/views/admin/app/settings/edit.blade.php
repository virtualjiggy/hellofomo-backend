@extends('admin.layout.admin')

@php
    $title        = __('Edit App Settings');
    $fontKey      = config('fomo.app_settings.font');
    $supportFonts = config('fomo.support_fonts');

    $fCMSenderID = config('fomo.app_settings.fcm_sender_id');
    $fCMServerKey = config('fomo.app_settings.fcm_server_key');
    $pushNotificationMaxLengthKey = config('fomo.app_settings.push_notification_max_length');
@endphp

@section('title', $title)

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="profile-content">
                <div class="row">
                    <form role="form" action="{{ route('admin.app.settings.update') }}" method="POST" class="form-horizontal form-validation submit-frm">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        <div class="col-md-12">
                            <div class="portlet light bordered no-padding-bottom">
                                <div class="portlet-title">
                                    <div class="caption font-green">{{ empty($title) ? __('App Settings') : $title }}</div>
                                    <div class="actions">
                                        @include('admin.components.full-screen')
                                    </div>
                                </div>

                                <div class="portlet-body form">
                                    <div class="tabbable-custom">
                                        <ul class="nav nav-tabs">
                                            <li class="active">
                                                <a href="#tab_1_1_1" data-toggle="tab"> {{ __('General') }} </a>
                                            </li>
                                            <li>
                                                <a href="#tab_1_1_2" data-toggle="tab"> {{ __('Push Notification') }} </a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="tab_1_1_1">
                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has($fontKey) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $fontKey }}">{{ __('App font') }}</label>

                                                    <div class="col-md-6">
                                                        <select name="{{ $fontKey }}" id="{{ $fontKey }}" class="form-control">
                                                            <option value="" @if (isset($settings[$fontKey]) && $settings[$fontKey] == '') selected @endif >{{ __('Select') }}</option>
                                                            @foreach($supportFonts as $name => $font)
                                                                <option value="{{ $font }}" @if (isset($settings[$fontKey]) && $settings[$fontKey] == $font) selected @endif >{{ $font }}</option>
                                                            @endforeach
                                                        </select>
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($fontKey) }}</span>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="tab-pane" id="tab_1_1_2">
                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has($fCMSenderID) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $fCMSenderID }}">{{ __('FCM Sender ID') }}</label>

                                                    <div class="col-md-6">
                                                        <input name="{{ $fCMSenderID }}"
                                                               id="{{ $fCMSenderID }}"
                                                               class="form-control"
                                                               value="{{ old($fCMSenderID, isset($settings[$fCMSenderID]) ? $settings[$fCMSenderID] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($fCMSenderID) }}</span>
                                                    </div>
                                                </div>

                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has($fCMServerKey) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $fCMServerKey }}">{{ __('FCM Server Key') }}</label>

                                                    <div class="col-md-6">
                                                        <input name="{{ $fCMServerKey }}"
                                                               id="{{ $fCMServerKey }}"
                                                               class="form-control"
                                                               value="{{ old($fCMServerKey, isset($settings[$fCMServerKey]) ? $settings[$fCMServerKey] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($fCMServerKey) }}</span>
                                                    </div>
                                                </div>

                                                <div class="form-group Elidev\ACL\Controllers {{ $errors->has($pushNotificationMaxLengthKey) ? 'has-error' : '' }}">
                                                    <label class="col-md-2 control-label" for="{{ $pushNotificationMaxLengthKey }}">{{ __('Max length of custom text') }}</label>

                                                    <div class="col-md-6">
                                                        <input name="{{ $pushNotificationMaxLengthKey }}"
                                                               id="{{ $pushNotificationMaxLengthKey }}"
                                                               class="form-control"
                                                               max="1024"
                                                               value="{{ old($pushNotificationMaxLengthKey, isset($settings[$pushNotificationMaxLengthKey]) ? $settings[$pushNotificationMaxLengthKey] : '') }}">
                                                        <div class="form-control-focus"></div>
                                                        <span class="help-block">{{ $errors->first($pushNotificationMaxLengthKey) }}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
