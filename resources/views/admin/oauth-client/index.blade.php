@extends('admin.layout.admin')

@section('title', __('Client Information'))

@section('content')
    @if ( !isset($client) )
        <h3>Nothing to see</h3>
    @else
        <div class="row">
            <div class="col-md-12">
                <div class="profile-content">
                    <div class="portlet light bordered no-padding-bottom">
                        <div class="portlet-title">
                            <div class="caption font-green">{{ __('Client Information') }}</div>
                            @include('admin.components.portlet-tools')
                            <div class="actions">
                                @include('admin.components.full-screen')
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <table class="table table-striped table-bordered table-hover table-checkable">
                                <tbody>
                                    <tr>
                                        <td>Client ID</td>
                                        <td>{{ $client->id }}</td>
                                    </tr>
                                    <tr>
                                        <td>Client secret</td>
                                        <td>{{ $client->secret }}</td>
                                    </tr>
                                    <tr>
                                        <td>Client name</td>
                                        <td>{{ $client->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Created at</td>
                                        <td>{{ $client->created_at }}</td>
                                    </tr>
                                    <tr>
                                        <td>Updated at</td>
                                        <td>{{ $client->updated_at }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection
