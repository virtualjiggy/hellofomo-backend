@extends('admin.layout.admin')

@section('title', __('Authors Detail'))

@section('action_buttons')
    <div class="clearfix">
        <div class="btn-group pull-right">
            <a href="{{ route('admin.authors.edit', [ 'author' => $author->id ]) }}" class="btn btn-default">{{ __('Edit') }}</a>

            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-angle-down"></i>
            </button>

            <ul class="dropdown-menu pull-left" role="menu">
                <li>
                    <a href="{{ route('admin.authors.trash', [ 'author' => $author->id ]) }}">{{ __('Trash') }}</a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="profile-content">
            <div class="row">
                <div class="col-md-10">
                    <div class="portlet light bordered no-padding-bottom">
                        <div class="portlet-title">
                            <div class="caption font-green">{{ __('Personal data') }}</div>
                            <div class="actions">
                                <button class="btn btn-icon-only btn-default fullscreen" data-original-title="Vollbild" title="{{ __('Full screen') }}"></button>
                            </div>
                        </div>

                        <div class="portlet-body form">
                            <table class="table fomo-show-author-tbl">
                                <tbody>
                                    <tr>
                                        <th width="130">{{ __('Salutation') }}</th>
                                        <td>{{ $author->salutation_name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="130">{{ __('Title') }}</th>
                                        <td>{{ $author->title_name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="130">{{ __('First name') }}</th>
                                        <td>{{ $author->first_name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="130">{{ __('Last name') }}</th>
                                        <td>{{ $author->last_name }}</td>
                                    </tr>
                                    <tr>
                                        <th width="130">{{ __('Display name') }}</th>
                                        <td>{{ $author->display_name }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="portlet light bordered no-padding-bottom">
                        <div class="portlet-title">
                            <div class="caption font-green">{{ __('Description') }}</div>
                            <div class="actions">
                                @include('admin.components.full-screen')
                            </div>
                        </div>

                        <div class="portlet-body form">
                            {!! $author->description !!}
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="portlet light bordered no-padding-bottom">
                        <div class="portlet-title">
                            <div class="caption font-green">{{ __('Primary Image') }}</div>
                        </div>

                        <div class="portlet-body form">
                            <div id="crop-avatar">
                                @if ($author->image_src)
                                    <div class="form-body">
                                        <div class="form-group">
                                            <!-- CURRENT AVATAR -->
                                            <div class="avatar-view">
                                                <img class="img-responsive" src="{{ $author->image_src }}" alt="{{ __('Image') }}">
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    {{ __("Not available") }}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
