@extends('admin.layout.admin')

@section('title', __('Authors Overview'))
@section('main_title', __('Authors'))

@section('content')
    @include('includes.datatable.datatable')
@endsection