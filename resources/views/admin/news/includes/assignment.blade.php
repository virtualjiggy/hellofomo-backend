<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Assignments') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body form">
        <div class="col-xs-12">
            @include('admin.news.includes.categories')
            @include('admin.news.includes.tags')
            <div class="form-group {{ $errors->has('authors') ? 'has-error' : '' }}" data-acl-role="authors.read">
                <label class="control-label horizontal-control-label">{{ __('Authors') }}</label>
                <select name="authors[]"
                        class="js-author-data-ajax form-control select2"
                        data-ajax-src="{{ route('admin.authors.search') }}"
                        multiple="multiple"
                        data-placeholder="{{ __('Select authors') }}"
                        data-per-page="{{ config('elidev.default_limit', 10) }}">
                    @if ( isset($news) )
                        @foreach($news->authors as $key => $author)
                            <option value="{{ $author->id }}" selected>{{ $author->display_name }}</option>
                        @endforeach
                    @endif
                </select>
                <div class="form-control-focus"></div>
                <span class="help-block help-block-error">{{ $errors->first('authors') }}</span>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
