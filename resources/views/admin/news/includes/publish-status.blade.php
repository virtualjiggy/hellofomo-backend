@php
    $format        = \App\Utils\Utils::dateTimeFormatBasedOnLanguage();
    $dateFormat    = $format['date_format'];
    $availableDate = isset($news->available_date) && $dateFormat ? $news->available_date->format($dateFormat) : '';
    $availableTime = isset($news->available_date) ? $news->available_date->format('H:i') : '00:00';
@endphp

<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Status') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body form publish-group">
        <div class="col-xs-12">
            <div data-acl-role="news.publish" class="form-group {{ $errors->has('status') ? 'has-error' : '' }}" data-acl-role="news.publish">
                @php $statuses = collect(config('elidev.list_default_status'))->except('draft'); @endphp
                <select class="form-control" name="status" id="status">
                    <option value="">{{ __('Select') }}</option>
                    @foreach($statuses as $key => $value)
                        <option @if ( isset($news) && $news->status == $value ) selected @endif value="{{ $key }}">{{ ucfirst(__($value)) }}</option>
                    @endforeach
                </select>
                <div class="form-control-focus"></div>
                <span class="help-block">{{ $errors->first('title_id') }}</span>
            </div>
            <div class="form-group {{ $errors->has('available_date') ? 'has-error' : '' }}">
                <div class="row">
                    <label class="col-sm-3 control-label text-right" for="available_date">{{ __('Date') }}</label>
                    <div class="col-sm-9">
                        <div class="input-group date form_datetime form_datetime bs-datetime">
                            <input type="text"
                                   size="16"
                                   class="form-control date-time-picker"
                                   name="available_date"
                                   id="available_date"
                                   onkeydown="return false"
                                   data-date-format="{{ \App\Utils\Utils::convertToJSDateTimeFormat($dateFormat, true) }}"
                                   value="{{ old('available_date', $availableDate) }}" />
                            <span class="help-block">{{ $errors->first('available_date') }}</span>
                            <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <span class="fa fa-fw fa-calendar"></span>
                            </button>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group {{ $errors->has('available_time') ? 'has-error' : '' }}">
                <div class="row">
                    <label class="control-label col-sm-3 text-right">{{ __('Time') }}</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <input type="text"
                                   name="available_time"
                                   class="form-control timepicker timepicker-24"
                                   value="{{ old('available_time', $availableTime) }}"
                            >
                            <span class="help-block">{{ $errors->first('available_date') }}</span>
                            <span class="input-group-btn">
                            <button class="btn default" type="button">
                                <i class="fa fa-clock-o"></i>
                            </button>
                        </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
