<div class="portlet light bordered no-padding-bottom" data-acl-role="files.read">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Gallery') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body form">
        <div class="gallery-images">
            <ul>

                @if ( isset($news) && !empty($news->gallery_ids) && isset($news->galleries) )
                    @foreach($news->galleries as $key => $mediaFile)
                        <li data-id="{{ $mediaFile->id }}">
                            <img src="{{ $mediaFile->url }}" alt="{{ $mediaFile->name }}" data-rv-media="[{{ json_encode(['selected_file_id' => $mediaFile->id]) }}]" class="rvMedia_preview_file">
                            <input type="hidden" name="galleries[]" value="{{ $mediaFile->id }}" class="image-data"/>
                            <a href="javascript:;"
                               class="btn-delete-gallery-img font-red-thunderbird"
                               data-id="{{ $mediaFile->id }}">
                                <i class="fa fa-times"></i>
                            </a>
                        </li>
                    @endforeach
                @endif

                <li class="image-placeholder hidden">
                    <img src="" class="image-data">
                    <input type="hidden"/>
                    <a href="javascript:;" class="btn-delete-gallery-img font-red-thunderbird" data-id=""><i class="fa fa-times"></i></a>
                </li>
            </ul>
        </div>

        <div class="clearfix"></div>

        <p style="margin-top: 1em;" class="text-center">
            <a href="javascript:;" class="btn-gallery-upload-image btn-upload-icon"><i class="fa fa-upload"></i></a>
        </p>
    </div>
</div>
