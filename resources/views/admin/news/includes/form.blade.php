<div class="row">
    <div class="col-md-12">
        <div class="profile-content">
            <div class="row">
                <form role="form" action="{{ empty($route) ? '' : $route }}" method="POST" class="form-horizontal form-validation submit-frm" enctype="multipart/form-data">
                    <div class="col-md-9">
                        <div class="portlet light bordered no-padding-bottom">
                            <div class="portlet-title">
                                <div class="caption font-green">{{ empty($title) ? __('Form News') : $title }}</div>
                                @include('admin.components.portlet-tools')
                                <div class="actions">
                                    @include('admin.components.full-screen')
                                </div>

                            </div>

                            <div class="portlet-body form">
                                {{ csrf_field() }}

                                @if ( isset($news) )
                                    {{ method_field('PUT') }}
                                @endif

                                <div class="form-body">
                                    <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label label-required" for="title">{{ __('Title') }}</label>
                                        <div class="col-md-10">
                                            <input type="text"
                                                   name="title"
                                                   id="title"
                                                   class="form-control"
                                                   value="{{ old('title', isset($news->title) ? $news->title : '') }}"
                                                   required>
                                            <div class="form-control-focus"></div>
                                            <span class="help-block">{{ $errors->first('title') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('introduction') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label" for="introduction">{{ __('Introduction') }}</label>
                                        <div class="col-md-10">
                                            <textarea name="introduction" id="introduction" class="form-control" rows="5">{{ old('introduction', isset($news->introduction) ? $news->introduction : '') }}</textarea>
                                            <span class="help-block">{{ $errors->first('introduction') }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('description') ? 'has-error' : '' }}">
                                        <label class="col-md-2 control-label" for="description">{{ __('Description') }}</label>
                                        <div class="col-md-10">
                                            @include('admin.components.addMedia')
                                            <a href="#" class="link-marked-text btn btn-default">{{ __('Link marked text') }}</a>
                                            @include('admin.partials.modal.link_marked_text')
                                            <textarea name="description"
                                                      id="description"
                                                      class="summernote form-control"
                                                      rows="15"
                                                      data-editor="summernote"
                                                      data-error-container="#description_error"
                                                      aria-describedby="description-error">{{ old('description', isset($news->description) ? $news->description : '') }}</textarea>
                                            <div id="description_error">
                                                <span id="description-error" class="help-block help-block-error">{{ $errors->first('description') }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @include('admin.news.includes.meta_tags')
                        @include('admin.news.includes.related_news')
                        @include('admin.news.includes.gallery')
                        @include('admin.news.includes.attachments')
                    </div>

                    <div class="col-md-3">
                        @include('admin.news.includes.publish-status')
                        @include('admin.news.includes.assignment')
                        @include('admin.components.image-box', ['title' => __('Cover picture'), 'data' => isset($news) ? $news : null])
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@push('footer')
    @include('media::partials.media')
@endpush

