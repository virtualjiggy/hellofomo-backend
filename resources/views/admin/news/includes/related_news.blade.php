<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Related News') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body">
        <div class="form-body custom-select2-container">
            <div class="form-group {{ $errors->has('related_news') ? 'has-error' : '' }}">
                <div class="col-md-12">
                    <select name="related_news[]"
                            id="related_news[]"
                            class="js-news-data-ajax form-control select2"
                            @if ( isset($news) ) data-excerpt-news-id="{{ $news->id }}" @endif
                            data-ajax-src="{{ route('admin.news.search') }}"
                            data-news-detail-url="{{ route('admin.news.edit', ['news' => -1]) }}"
                            multiple="multiple" data-placeholder="{{ __('Search by news title') }}">
                        @if ( isset($news) )
                            @foreach($news->related_news_full as $key => $newsItem)
                                <option value="{{ $newsItem->id }}" selected>{{ $newsItem->title }}</option>
                            @endforeach
                        @endif
                    </select>
                    <div class="form-control-focus"></div>
                    <span class="help-block">{{ $errors->first('related_news') }}</span>

                    <div class="js-related-news-container">
                        @if ( isset($news) )
                            <ul>
                                @foreach($news->related_news_full as $key => $newsItem)
                                    <li class="tag-selected">
                                        <a class="destroy-tag-selected">×</a>
                                        {{ $newsItem->title }}
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
