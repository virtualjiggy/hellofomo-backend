<div class="portlet light bordered no-padding-bottom box-related-news">
    <div class="mt-element-list">
        <div class="mt-list-head list-news ext-1 font-white bg-grey-gallery">
            <div class="list-head-title-container portlet-title">
                <h3 class="list-title caption">{{ __('Related News') }}</h3>
            </div>
            <div class="list-count pull-right bg-red">{{ $news->related_news_full->count() }}</div>
        </div>
        <div class="mt-list-container list-news ext-1">
            @if ( $news->related_news_full->isEmpty() )
                <p>&nbsp; {{ __('There is no related news') }}</p>
            @else
                <ul id="data-related-news" style="position: relative;">
                    <li class="hide"></li>
                </ul>
                <div class="loader">
                    <div class="load-more b-btn" style="display: block;">
                        <a href="javascript:;"
                           id="load-more-related-news"
                           class="btn tooltips"
                           data-container="#data-related-news"
                           data-parent=".load-more"
                           data-ride="ap-more"
                           data-holder="#data-related-news>:last"
                           data-sourcetype="url"
                           data-original-title="View more"
                           data-sourcedata="{{ route("admin.news.moreRelatedNews") }}?id={{ $news-> id}}&page=1&per_page=5"
                           data-blockuihtml='<a><i class="fa fa-spinner fa-spin fa-3x"></i></a>'
                           data-blockuicss='{ "border": "none", "background": "none" }'>{{ __('View more') }}
                            <i class="fa fa-angle-down"></i>
                        </a>
                    </div>
                </div>
            @endif
        </div>
    </div>
</div>