<div class="portlet light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Gallery') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body">
        <div class="blog-single-sidebar-ui">
            <div class="ui-margin gallery-images news-detail-page">
                @if ( isset($news->galleries) && $news->galleries->count() > 0 )
                    <ul>
                        @foreach($news->galleries as $key => $mediaFile)
                            <li class="col-xs-4 ui-padding">
                                <a href="{{ $mediaFile->url }}" class="fancybox" style="width: 100%">
                                    <img src="{{ $mediaFile->url }}" alt="{{ $mediaFile->name }}">
                                </a>
                            </li>
                        @endforeach
                    </ul>
                @else
                    <div>{{ __('There is no gallery') }}</div>
                @endif

            </div>
        </div>
    </div>
</div>