<div class="portlet portlet-news-show-attachments light bordered no-padding-bottom">
    <div class="portlet-title">
        <div class="caption font-green">{{ __('Files') }}</div>
        @include('admin.components.portlet-tools')
    </div>

    <div class="portlet-body box-portlet-body-attachments">
        @if ( isset($news) && !empty($news->attachment_ids) && isset($news->attachments) )
            <div class="mt-element-list">
                <div class="mt-list-container list-default">
                    <ul>
                        @foreach($news->attachments as $key => $mediaFile)
                            <li class="mt-list-item">
                                <div class="list-icon-container">
                                    <i class="{{ $mediaFile->icon }}"></i>
                                </div>
                                <div class="list-datetime">{{ number_format($mediaFile->size / 1024, 2) }} Kb</div>
                                <div class="list-item-content">
                                    <h3 class="bold">
                                        <a href="{{ url($mediaFile->url) }}" target="_blank" title="{{ $mediaFile->name }}">{{ $mediaFile->name }}</a>
                                    </h3>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        @else
            <div><br/>{{ __('There is no files') }}</div>
        @endif

    </div>
</div>