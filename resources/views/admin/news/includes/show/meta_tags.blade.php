<div class="portlet light bordered no-padding-bottom box-related-news">
    <div class="mt-element-list">
        <div class="mt-list-head list-news ext-1 font-white bg-grey-gallery">
            <div class="list-head-title-container portlet-title">
                <h3 class="list-title caption">{{ __('SEO meta tags') }}</h3>
            </div>
        </div>

        <div class="mt-list-container list-news">
            <ul>
                <li class="mt-list-item">
                    <div class="list-item-content">
                        <h4>{{ __('SEO title') }}</h4>
                        <p>{{ $news->meta_title or __('No data available.') }}</p>
                    </div>
                </li>

                <li class="mt-list-item">
                    <div class="list-item-content">
                        <h4>{{ __('Permalink') }}</h4>
                        <p>{{ $news->meta_permalink or __('No data available.') }}</p>
                    </div>
                </li>

                <li class="mt-list-item">
                    <div class="list-item-content">
                        <h4>{{ __('Meta description') }}</h4>
                        <p>{{ $news->meta_description or __('No data available.') }}</p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
