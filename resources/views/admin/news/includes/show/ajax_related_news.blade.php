@foreach($relatedNews as $key => $newsItem)
    <li class="mt-list-item">
        <div class="list-icon-container">
            <a href="{{ route('admin.news.show', ['news' => $newsItem->id]) }}" target="_blank">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
        <div class="list-thumb">
            <a href="{{ route('admin.news.show', ['news' => $newsItem->id]) }}" target="_blank">
                <img src="{{ !isset($newsItem->image_src) || empty($newsItem->image_src) ? asset(config('fomo.image.default')) : $newsItem->image_src }}" alt="{{ __('Image') }}">
            </a>
        </div>
        <div class="list-datetime bold uppercase font-red">{{ Carbon\Carbon::parse($newsItem->available_date)->format(\App\Utils\Utils::dateTimeFormatBasedOnLanguage()['date_time_format']) }}</div>
        <div class="list-item-content">
            <h3 class="uppercase">
                <a href="{{ route('admin.news.show', ['news' => $newsItem->id]) }}" target="_blank">{{ $newsItem->title }}</a>
            </h3>
            <p class="news-intodiction">{{ \App\Utils\Utils::truncation($newsItem->introduction, 200)['text'] }}</p>
        </div>
    </li>
@endforeach