<div class="form-group {{ $errors->has('tags') ? 'has-error' : '' }}" data-acl-role="tags.read">
    <label class="control-label horizontal-control-label">{{ __('Tags') }}</label>

    @if ( count($tags) <= 0 )
        <a href="{{ route('admin.tags.create') }}" class="pull-right tags-create-new" target="_blank">{{ __('Create new') }}</a>
    @endif

    @php
        $newsTags = [];
        if ( old('tags') != null ) {
            $newsTags = old('tags');
        } else if ( isset($news) ) {
            $newsTags = $news->tags->pluck('id')->all();
        }
        $totalTagsDb = count($newsTags);
    @endphp
    <select name="tags[]"
            class="form-control select2-multiple"
            multiple="multiple"
            data-placeholder="{{ __('Please choose') }}">
        @foreach($tags as $key => $tag)
            <option value="{{ $tag->id }}" @if ( $totalTagsDb > 0 && in_array($tag->id, $newsTags) ) selected @endif>{{ $tag->name }}</option>
        @endforeach
    </select>
    <span id="tags-error" class="help-block help-block-error">{{ $errors->first('tags') }}</span>
</div>
