<div class="form-group {{ $errors->has('categories') ? 'has-error' : '' }}" data-acl-role="categories.read">
    <label class="control-label horizontal-control-label">{{ __('Categories') }}</label>

    @if ( count($categories) <= 0 )
        <a href="{{ route('admin.categories.create') }}" class="pull-right categories-create-new" target="_blank">{{ __('Create new') }}</a>
    @endif

    @php
        $newsCategories = [];
        if ( old('categories') != null ) {
            $newsCategories = old('categories');
        } else if ( isset($news) ) {
            $newsCategories = $news->categories->pluck('id')->all();
        }
        $totalCategoriesDb = count($newsCategories);
    @endphp
    <select name="categories[]"
            class="form-control select2-multiple"
            multiple="multiple"
            data-placeholder="{{ __('Please choose') }}">
        @foreach($categories as $key => $category)
            <option value="{{ $category->id }}" @if ( $totalCategoriesDb > 0 && in_array($category->id, $newsCategories) ) selected @endif>{{ $category->name }}</option>
        @endforeach
    </select>
    <span id="categories-error" class="help-block help-block-error">{{ $errors->first('categories') }}</span>
</div>
