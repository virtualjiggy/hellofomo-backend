@extends('admin.layout.admin')

@section('title', __('Create news'))

@section('content')
    @include('admin.news.includes.form', [
        'title'         => __('Basic Information'),
        'route'         => route('admin.news.store'),
        'btnSubmitText' => __('Create'),
    ])
@endsection