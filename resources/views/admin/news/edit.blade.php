@extends('admin.layout.admin')

@section('title', __('News Edit'))

@section('action_buttons')
    <div class="clearfix">
        <div class="btn-group pull-right">
            <a href="{{ route('admin.news.show', [ 'news' => $news->id ]) }}" class="btn btn-default">{{ __('Show detail') }}</a>

            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                <i class="fa fa-angle-down"></i>
            </button>

            <ul class="dropdown-menu pull-left" role="menu">
                <li>
                    <a href="{{ route('admin.news.trash', [ 'news' => $news->id ]) }}">{{ __('Trash') }}</a>
                </li>
            </ul>
        </div>
    </div>
@endsection

@section('content')
    @include('admin.news.includes.form', [
        'title'         => __('Basic Information'),
        'route'         => route('admin.news.update', ['news' => $news->id]),
        'btnSubmitText' => __('Save'),
    ])
@endsection
