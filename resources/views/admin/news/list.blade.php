@extends('admin.layout.admin')

@section('title', __('News Overview'))

@section('content')
    @include('includes.datatable.datatable')

    {{-- Include modal to set categories/tags --}}
    @include('admin.partials.modal.set_categories_tags')
@endsection
