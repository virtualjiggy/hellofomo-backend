@extends('layouts.app')

@section('title', __('Join us'))

@section('body_class')

@endsection


@section('content')
    <div class="section blog-post">
        <div class="p-container">
            @include('partial.join-now')
        </div>
    </div>
@endsection
