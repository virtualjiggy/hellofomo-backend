jQuery(document).ready(function() {
    if ( $('.btn_gallery').is(':visible') ) {
        $('.btn_gallery').rvMedia({
            multiple: false,
            onSelectFiles: function (files, $el) {
                var firstItem = _.first(files);
                $el.closest('.image-box').find('.image-data').val(firstItem.id);
                $el.closest('.image-box').find('.preview_image').attr('src', firstItem.thumb).show();
            }
        });

        $('.btn_remove_image').on('click', function (event) {
            event.preventDefault();
            $(this).closest('.image-box').find('img').hide();
            $(this).closest('.image-box').find('input').val('');
        });
    }
});
