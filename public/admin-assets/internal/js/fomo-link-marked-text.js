FomoLinkMarkedText = function () {
    var fomoRange = null;
    var fomoPopupBtnApply = '#fomo-link-marked-text .btn-link-marked-text';
    var fomoPopupSelectNews = '#fomo-news-internal-link';
    var fomoPopupLinkMarkedText = '#fomo-link-marked-text';
    var fomoBtnLinkMarkedText = '.link-marked-text';
    var fomoClassAttr = 'fomo-custom-link';

    function handleSubmitButton() {
        $(fomoPopupBtnApply).off('click').on('click', function(e) {
            // not process when button is disabled
            e.preventDefault();
            if ( $(this).prop('disabled') || fomoRange == null) {
                showHideCustomLinkPopup('hide', 0);
                return;
            }
            var current = $(e.currentTarget);
            var rng = fomoRange;
            var text = rng.toString();
            rng = rng.deleteContents();
            //get url
            var newsId = $(fomoPopupSelectNews).val();
            var newsTitle = $(fomoPopupSelectNews + ' option:selected').text();
            var linkUrl = '';
            if(newsId == '' || newsId == null) {
                return;
            } else {
                linkUrl = $(fomoPopupBtnApply).attr('pattern-href') + newsId;
            }
            if(rng.nodes().length > 0) {
                if (current.attr('action') != 0) {
                    var anchor = rng.nodes()[0].parentNode;
                    current.attr('action', 0);
                } else {
                    if(text == '') {
                        return;
                    }
                    if ($(rng.nodes()[0]).prop("tagName") != "A") {
                        var anchor = rng.insertNode($('<A>' + text + '</A>')[0]);
                    } else {
                        var anchor = rng.nodes()[0];
                    }
                    anchor.text = text;
                }
            } else {
                if(text == '') {
                    return;
                }
                var anchor = rng.insertNode($('<A>' + text + '</A>')[0]);
            }

            $(anchor).attr('class', 'fomo-custom-link');
            $(anchor).attr('href', linkUrl);
            $(anchor).attr('data-news-id', newsId);
            $(anchor).attr('title', newsTitle);
            $(anchor).attr('data-type', 'text link');
            $('#description').summernote('editor.insertText', '');
            fomoRange = null;
            showHideCustomLinkPopup('hide', 0);
        });

    }

    function showHideCustomLinkPopup(action, type) {
        var markedLinkModal = $(fomoPopupLinkMarkedText);
        if(action == 'show') {
            if ( fomoRange != null
                && typeof fomoRange.nodes()[0] !== 'undefined'
                && $(fomoRange.nodes()[0].parentNode).prop('tagName') == 'A') {
                var newsId = $(fomoRange.nodes()[0].parentNode).attr('data-news-id');
                var newsTitle = $(fomoRange.nodes()[0].parentNode).attr('title');
                var select = $(fomoPopupSelectNews + ' option[value="'+ newsId +'"]');
                if (select.length <= 0 ) {
                    $(fomoPopupSelectNews).append('<option value="'+ newsId +'">'+ newsTitle +'</option>');
                }
                $(fomoPopupSelectNews).val(newsId).trigger('change');
                //get news title

            } else {
                $(fomoPopupSelectNews).val('').trigger('change');
            }
        }

        var btnApply = $(fomoPopupBtnApply);
        if ( btnApply.length > 0 ) {
            btnApply.attr('action', type);
        }

        markedLinkModal.modal(action);
    }

    function handleShowCustomLinkDialog() {
        var classLink = '.' + fomoClassAttr;
        $('.note-editor .note-editable').off('click', classLink).on('click', classLink, function(e) {
            e.preventDefault();
            fomoRange =  $('#description').summernote('createRange');
            showHideCustomLinkPopup('show', 1);
        });

    }

    function searchNewsPopup() {
        if ( $(fomoPopupSelectNews).size() > 0 && $(fomoPopupSelectNews).attr('data-ajax-src') != '' ) {
            // get per page
            var perPage = $(fomoPopupSelectNews).attr('data-per-page');
            if ( typeof perPage == 'undefined' || perPage == '' ) {
                perPage = 10;
            }
            $(fomoPopupSelectNews).select2({
                placeholder: $(fomoPopupSelectNews).attr('data-placeholder'),
                width: '100%',
                ajax: {
                    url: $(fomoPopupSelectNews).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function (params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function (data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: { more: (params.page * perPage) < data.total_count }
                        };
                    },
                }
            });
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            handleSubmitButton();
            handleShowCustomLinkDialog();
            searchNewsPopup();
            $(fomoBtnLinkMarkedText).off('click').on('click' , function(e) {
                // description
                e.preventDefault();
                fomoRange =  $('#description').summernote('createRange');
                if(fomoRange.toString() == '') {
                    return;
                }
                showHideCustomLinkPopup('show', 0);
            });
        }
    };

}();

jQuery(document).ready(function() {
    FomoLinkMarkedText.init();
});
