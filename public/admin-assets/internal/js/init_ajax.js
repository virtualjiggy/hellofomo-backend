FOMO = {
    blockUI:  {
        message:'<div class="loader"></div>',
        css: { "border": "none", "background": "none" }
    },
    blockMetronicUI: {
        message:'<i class="fa fa-spinner fa-spin fa-2x fa-fw"></i>',
        css: { "border": "none", "background": "none" },
        centerX: true,
        centerY: true,
    },
    'saveBtnForm': '#fomo-save-btn',
    'overrideSaveFrmClass': 'fomo-override-save-action',
    'listeningFrm': 'form.submit-frm',
};


(function($) {
    $(function() {
        /**
         * Get query string parameter by name
         *
         * @param name
         * @param url
         * @returns {*}
         */
        function getQueryStringParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        // Handle ajax error
        $(document).ajaxError(function myErrorHandler(event, xhr, ajaxOptions, thrownError) {

            if (xhr.status === 401) {

            }
        });

        // handle finish ajax calls
        $(document).ajaxStop(function(){
            $('.tooltips').tooltip();
        });

    });
}) (jQuery)
