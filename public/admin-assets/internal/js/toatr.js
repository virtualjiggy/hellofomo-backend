var FomoToastr = function () {

    /**
     * Handle display message by toastr
     * @author vulh
     * */
    function handleDisplayAlertMessageByToastr() {
        if ($('#fomo-alert-success').length) {
            toastr.success($('#fomo-alert-success').html());
        }

        if ($('#fomo-alert-error').length) {
            toastr.success($('#fomo-alert-error').html());
        }
    }

    return {
        //main function to initiate the module
        init: function () {
            handleDisplayAlertMessageByToastr();
        }
    };

}();

jQuery(document).ready(function() {
    FomoToastr.init();
});
