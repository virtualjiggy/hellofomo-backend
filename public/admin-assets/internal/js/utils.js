(function($) {
    $(function() {
        /**
         * Generate preview image
         * @param input
         * @author Sang Nguyen
         */
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var $selector = $(input).closest('.form-group').find('.form-preview-image');
                    if ( $selector.is('img') ) {
                        if ( input.files[0]['type'].split('/')[0] == 'image' ) {
                            $selector.attr('src', e.target.result);
                        }
                    } else {
                        $selector.css({'background-image': 'url(' + e.target.result + ')', 'display' : 'block'});
                    }
                };

                reader.readAsDataURL(input.files[0]);
            } else {
                $(input).closest('.form-group').find('.form-preview-image').hide();
            }
        }

        $(document).ready(function() {
            if ( $('.form-image-input').is(':visible') ) {
                $('.form-image-input').on('change', function () {
                    readURL(this);
                });
            }
        });

    });
}) (jQuery)
