AppPushNotification = function () {

    var $pushBtn = $('.fomo-push-noti-btn'),
        $customText = $('#fomo-push-custom-text');

    /**
     * Auto focus on the fist child
     * */
    function pushManualMessage() {
        $pushBtn.on('click', function(e) {

            e.preventDefault();

            var $this = $(this),
                content = $this.closest('.tab-pane').find('textarea').val(),
                form = $this.closest('form');

            // Find news id
            var news_id = $this.closest('#push-tab-news').find('select').val();

            if (!content && !news_id) {
                toastr.error($this.data().errorMessage);
                return;
            }

            $.ajax({
                url: $this.data().source,
                type: 'POST',
                cache: false,
                processData: false,
                contentType: false,
                data: new FormData($(form)[0]),
                beforeSend: function () {
                    $.blockUI(FOMO.blockMetronicUI);
                },
                success: function( response ) {
                    $.unblockUI();
                    toastr.success(response.data.message);
                },
                error: function(jqXHR, exception) {
                    $.unblockUI();
                    if ( jqXHR.status === 400 ) {
                        toastr.error(jqXHR.responseJSON.message);
                    }
                }
            });
        });
    }

    /**
     * Validate max characters of custom message before push notification
     */
    function validateMaxLengthOfCustomMessage() {
        $customText.maxlength({
            alwaysShow: true,
            warningClass: "label label-success",
            limitReachedClass: "label label-danger",
            preText: 'You typed ',
            separator: ' out of ',
            postText: ' chars available.',
            validate: true
        });
    }

    /**
     * Set active tab to hidden input
     */
    function trackActiveTab() {
        $('.tabbable-custom a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var $form = $(this).closest('form'),
                target = $(e.target).attr('href'),
                active = target == '#push-tab-news' ? 'news' : 'custom';
            $form.find('input[name="active_tab"]').val(active);
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            pushManualMessage();
            validateMaxLengthOfCustomMessage();
            trackActiveTab();
        }
    };

}();

jQuery(document).ready(function() {
    AppPushNotification.init();
});
