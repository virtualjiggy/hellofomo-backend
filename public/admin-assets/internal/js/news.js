jQuery(document).ready(function() {
    if ( $('.date-time-picker').size() > 0 ) {
        $('.date-time-picker').datetimepicker({
            orientation: "left",
            autoclose: true,
            format: 'dd.mm.yyyy hh:ii',
            language: 'de'
        });
    }
});
