/* ========================================================================
 * AddMedia.js v1.0
 * Requires Botble Media
 *
 * ======================================================================== */

+function ($) {
    'use strict';

    var AddMedia = function (element, options) {
        this.options     = options;
        $(element).rvMedia({
            multiple: true,
            onSelectFiles: function (files, $el) {
                if (typeof files != 'undefined') {
                    if ( typeof $(options.destination).attr('data-editor') != 'undefined' && $(options.destination).attr('data-editor') == 'summernote' ) {
                        handleInsertImagesForSummerNote(files);
                    } else {
                        var editor = $(options.destination).data("wysihtml5").editor;
                        handleInsertImagesForWysihtml5Editor(editor, files);
                    }
                }
            }
        });
    };

    AddMedia.VERSION  = '1.1.0';

    /**
     * Insert images to summernote editor
     * @param files
     */
    function handleInsertImagesForSummerNote(files) {
        if ( files.length == 0 ) {
            return;
        }

        for (var i = 0; i < files.length; i++) {
            if ( !files[i].type.match('image') ) {
                continue;
            }
            let image_url = typeof base_url != 'undefined' ? base_url + files[i].url : files[i].url;
            $('.summernote').summernote('insertImage', image_url, files[i].basename);
        }
    }

    /**
     * Insert images to Wysihtml5 editor
     * @param editor
     * @param files
     */
    function handleInsertImagesForWysihtml5Editor(editor, files) {
        if ( files.length == 0 ) {
            return;
        }

        // insert images for the wysihtml5 editor
        let s = '';
        for (var i = 0; i < files.length; i++) {
            if (!files[i].type.match('image')) {
                continue;
            }
            s += '<img src="' + RV_MEDIA_URL.base_url + files[i].url + '">'
        }

        if (editor.getValue().length > 0) {
            let length = editor.getValue();
            editor.composer.commands.exec("insertHTML", s);
            if (editor.getValue() == length)
                editor.setValue(editor.getValue() + s);
        } else {
            editor.setValue(editor.getValue() + s);
        }
    }

    function CallAction(option) {
        return this.each(function () {
            var $this   = $(this)
            var data    = $this.data('bs.media')
            var options = $.extend({}, $this.data(), typeof option == 'object' && option)
            if (!data) $this.data('bs.media', (data = new AddMedia(this, options)))
        })
    }

    $.fn.addMedia             = CallAction
    $.fn.addMedia.Constructor = AddMedia

    $(window).on('load', function () {
        $('[data-add-media="app-add-media"]').each(function () {
            var $addMedia = $(this)
            CallAction.call($addMedia, $addMedia.data())
        })
    })

}(jQuery);
