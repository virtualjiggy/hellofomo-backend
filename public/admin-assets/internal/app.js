/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 31);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */,
/* 1 */,
/* 2 */,
/* 3 */,
/* 4 */,
/* 5 */,
/* 6 */,
/* 7 */
/***/ (function(module, exports) {

var AppPushNotification = function () {

    var $pushBtn = $('.fomo-push-noti-btn');

    /**
     * Auto focus on the fist child
     * */
    function pushManualMessage() {
        $pushBtn.on('click', function (e) {

            e.preventDefault();

            var $this = $(this),
                content = $this.closest('.tab-pane').find('textarea').val(),
                form = $this.closest('form');

            // Find news id
            var news_id = $this.closest('#push-tab-news').find('select').val();

            if (!content && !news_id) {
                toastr.error($this.data().errorMessage);
                return;
            }

            $.ajax({
                url: $this.data().source,
                type: 'POST',
                cache: false,
                processData: false,
                contentType: false,
                data: new FormData($(form)[0]),
                beforeSend: function beforeSend() {
                    $.blockUI(FOMO.blockMetronicUI);
                },
                success: function success(response) {
                    $.unblockUI();
                    toastr.success(response.data.message);
                },
                error: function error(jqXHR, exception) {
                    $.unblockUI();
                    if (jqXHR.status === 400) {
                        toastr.error(jqXHR.responseJSON.message);
                    }
                }
            });
        });
    }

    return {
        //main function to initiate the module
        init: function init() {
            pushManualMessage();
        }
    };
}();

jQuery(document).ready(function () {
    AppPushNotification.init();
});

/***/ }),
/* 8 */
/***/ (function(module, exports) {

/**
 * Handle app dashboard layout behaviours
 */
var FomoAppDashboardLayout = function () {
    var form = '#frm-new-section',
        originalSection = '#new-section-box',
        appendDestination = '#app-layout-dashboard-portlet-body form#frm-new-section',
        sectionLayout = '.box-item-layout-section',
        sectionLayoutTitle = '.portlet-title .caption',
        btnAddSection = '.group-button-action button.btn-block',
        btnDeleteSection = '#app-layout-dashboard-wrapper .frm-section .delete-section',
        btnSectionCollapse = '#app-layout-dashboard-wrapper .frm-section .tools .collapse',
        btnSectionExpand = '#app-layout-dashboard-wrapper .frm-section .tools .expand',
        btnSaveLayout = '#fomo-save-app-dashboard-layout-btn',
        btnEachSectionSave = '.group-button-action .btn-each-section-save',
        selectDataType = 'select[name="type[]"]',
        selectLayoutType = '#app-layout-dashboard-wrapper select[name="layout[]"]',
        indexBox = $('div[id^=frm-section]').length,
        originalForm = null;

    /**
     * Handle add section button
     */
    function handleAddSectionButton() {
        $(document).on('click', btnAddSection, function (e) {
            e.preventDefault();
            cloneSection();
        });
    }

    /**
     * Handle delete section button
     */
    function handleDeleteSectionButton() {
        $(document).on('click', btnDeleteSection, function (e) {
            e.preventDefault();
            var itemDelete = $(this).closest('.frm-section');
            swal({
                title: "Are you sure?",
                type: "warning",
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: true,
                showCancelButton: true
            }, function () {
                itemDelete.remove();
            });
        });
    }

    /**
     * Handle change background
     */
    function handleChangeBackground() {
        /**
         * Handle change background when collapse the section
         */
        $(document).on('click', btnSectionCollapse, function (e) {
            e.preventDefault();
            $(this).closest(sectionLayout).addClass('green').find(sectionLayoutTitle).removeClass('font-dark');
        });

        /**
         * Handle change background when expand the section
         */
        $(document).on('click', btnSectionExpand, function (e) {
            e.preventDefault();
            $(this).closest(sectionLayout).removeClass('green').find(sectionLayoutTitle).addClass('font-dark');
        });
    }

    /**
     * Handle save for "Save" button of each section in Left Panel
     */
    function handleSaveEachSectionInLeftPanel() {
        $(document).on('click', btnEachSectionSave, function (e) {
            e.preventDefault();
            $(btnSaveLayout).trigger('click');
        });
    }

    /**
     * Handle save settings
     */
    function handleSaveAppDashboardLayoutButton() {
        $(document).on('click', btnSaveLayout, function (e) {
            e.preventDefault();

            // send ajax to save layout settings
            var ajaxUrl = $(this).attr('data-ajax-url');
            if (typeof ajaxUrl != 'undefined' && ajaxUrl != '') {
                $.ajax({
                    url: ajaxUrl,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: new FormData($(form)[0]),
                    beforeSend: function beforeSend() {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function success(response) {
                        $.unblockUI();
                        if (response.errors == false) {
                            toastr.success(response.data.message);

                            // after saved form -> update the original form
                            originalForm = $(form).serialize();
                        }
                    },
                    error: function error(jqXHR, exception) {
                        $.unblockUI();
                        if (jqXHR.status === 400) {
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    }
                });
            }
        });
    }

    /**
     * Handle select data type change
     */
    function handleSelectDataTypeChange() {
        $(selectDataType).on('change', function () {
            var divSelectDataTypeValue = $(this).parent().parent().siblings().closest('div.select-data-type-value'),
                lblDataTypeValue = divSelectDataTypeValue.find('label[for="input-type-value"]'),
                selectCategory = divSelectDataTypeValue.find('#input-category-type-value'),
                selectTag = divSelectDataTypeValue.find('#input-tag-type-value');

            switch (this.value) {
                case 'all':
                    $(selectCategory).prop('disabled', false);
                    $(selectTag).prop('disabled', 'disabled');

                    divSelectDataTypeValue.addClass('hidden');
                    $(selectCategory).addClass('hidden');
                    $(selectTag).addClass('hidden');
                    break;
                case 'category':
                    $(selectCategory).prop('disabled', false);
                    $(selectTag).prop('disabled', 'disabled');

                    divSelectDataTypeValue.removeClass('hidden');
                    $(selectCategory).removeClass('hidden');
                    $(selectTag).addClass('hidden');

                    $(lblDataTypeValue).text('Category');
                    break;
                case 'tag':
                    $(selectTag).prop('disabled', false);
                    $(selectCategory).prop('disabled', 'disabled');

                    divSelectDataTypeValue.removeClass('hidden');
                    $(selectTag).removeClass('hidden');
                    $(selectCategory).addClass('hidden');

                    $(lblDataTypeValue).text('Tag');
                    break;
            }
        });
    }

    /**
     * Handle rename each section when layout type changed
     */
    function handleRenameEachSectionWhenLayoutTypeChange() {
        $(selectLayoutType).on('change', function () {
            renameSectionTitle($(this));
        });
    }

    /**
     * Rename each section title when layout type change
     * @param $element
     */
    function renameSectionTitle($element) {
        var $sectionTitle = $element.closest('div.portlet-body.form').siblings().find('div.caption.text-capitalize'),
            $selectSectionType = $element.parent().parent().parent().siblings().find('select[name="section_type[]"]');

        $sectionTitle.html($selectSectionType.val() + " " + $element.val());
    }

    /**
     * Handle validate data offset
     */
    function handleValidateDataOffset() {
        $(document).on('blur', '#app-layout-new-action-wrapper input[name="data_offset[]"]', function () {
            $(this).val(parseInt($(this).val()));
        });

        $(document).on('blur', '#app-layout-dashboard-wrapper input[name="data_offset[]"]', function () {
            $(this).val(parseInt($(this).val()));
        });
    }

    /**
     * Clone new section to main form
     */
    function cloneSection() {
        var $original = $(originalSection),
            $cloned = $original.clone(true).attr('id', 'new_form' + indexBox);

        // get original selects into a jq object
        var $originalSelects = $original.find('select');
        $cloned.find('select').each(function (index, item) {
            // set new select to value of old select
            $(item).val($originalSelects.eq(index).val());
        });

        // get original textareas into a jq object
        var $originalTextareas = $original.find('textarea');
        $cloned.find('textarea').each(function (index, item) {
            // set new textareas to value of old textareas
            $(item).val($originalTextareas.eq(index).val());
        });

        // remove button "Add Section" and add button "Save"
        $cloned.find('.group-button-action button').remove();
        $cloned.find('.group-button-action').append('<button type="button" class="btn green pull-right btn-each-section-save"><i class="fa fa-save"></i>&nbsp;Save</button>');

        var html = $("#box-item-container").clone(true).removeClass('hidden').attr('id', 'frm-section' + indexBox);
        html.find('.caption').html($cloned.find("select[name='section_type[]']").val() + " " + $cloned.find("select[name='layout[]']").val());
        $cloned.appendTo(html.find('.portlet-body'));
        html.appendTo($(appendDestination));
        indexBox++;

        // subscribe change event to modify section title when Layout type changed
        $($cloned.find("select[name='layout[]']")).on('change', function () {
            renameSectionTitle($(this));
        });
    }

    /**
     * Set min height right panel
     */
    function setMinHeightRightPanel() {
        $("#app-layout-new-action-wrapper > .portlet").css('min-height', $("#app-layout-dashboard-wrapper > .portlet").css('height'));
    }

    /**
     * Set min height left panel
     */
    function setMinHeightLeftPanel() {
        $("#app-layout-dashboard-wrapper > .portlet").css('min-height', $("#app-layout-new-action-wrapper > .portlet").css('height'));
    }

    /**
     * Save original form to ask user before they are leaving
     */
    function saveOriginalForm() {
        setTimeout(function () {
            if ($(form)) {
                originalForm = $(form).serialize();
            }
        }, 1000);
    }

    /**
     * When the form has changed, ask user before they leave
     */
    function _askBeforeLeave() {
        // if the form has changed and we are in app dashboard layout setting page
        if (typeof originalForm != 'undefined' && originalForm && $(form) && $(form).serialize() != originalForm) {
            // Refer: https://stackoverflow.com/questions/1119289/how-to-show-the-are-you-sure-you-want-to-navigate-away-from-this-page-when-ch
            return true;
        }
    }

    return {
        init: function init() {
            // enable sort by dragging items
            var sortElement = document.getElementById('frm-new-section');
            if (sortElement != null) {
                Sortable.create(sortElement, {});
            }

            // collapse all sections if have
            $('#app-layout-dashboard-portlet-body a.collapse').trigger('click');

            setMinHeightLeftPanel();

            // register event handlers
            handleAddSectionButton();
            handleDeleteSectionButton();
            handleChangeBackground();
            handleSaveAppDashboardLayoutButton();
            handleSelectDataTypeChange();
            handleSaveEachSectionInLeftPanel();
            handleRenameEachSectionWhenLayoutTypeChange();
            handleValidateDataOffset();
            saveOriginalForm();
        },
        askBeforeLeave: function askBeforeLeave() {
            return _askBeforeLeave();
        }
    };
}();

jQuery(document).ready(function () {
    FomoAppDashboardLayout.init();

    $(window).on("beforeunload", function () {
        return FomoAppDashboardLayout.askBeforeLeave();
    });
});

/***/ }),
/* 9 */
/***/ (function(module, exports) {

var FomoForm = function () {

    /**
     * Auto focus on the fist child
     * */
    function autoFocusOnTheFistElm() {
        $('form').find('input:not(:hidden)').first().focus();
    }

    function enableDateTimePicker() {
        if ($('.fomo-list-page-date-filter').size() > 0) {
            $('.fomo-list-page-date-filter').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                minView: 2
            });
        }
        if ($('input.fomo-list-page-time-filter').size() > 0) {
            $('input.fomo-list-page-time-filter').inputmask('hh:mm', {
                placeholder: "00:00"
            });
        }
        if ($('.fomo-list-page-datetime-filter').size() > 0) {
            $('.fomo-list-page-datetime-filter').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                minView: 2
            });
        }

        if ($('.date-time-picker').size() > 0) {
            $('.date-time-picker').datetimepicker({
                orientation: "left",
                autoclose: true,
                language: locale,
                startDate: new Date(),
                pickTime: false,
                minView: 2
            });
        }

        if ($('.btn-clear-date-filter').size() > 0) {
            $(document).on('click', '.btn-clear-date-filter', function () {
                $(this).parent().siblings('input').val('');
                $(this).parent().siblings('input').trigger('change');
            });
        }
    }

    function enableUploadFile() {
        var btnUploadImage = '.btn-upload-image',
            btnRemoveImage = '.btn-remove-image';

        if (!$.fn.rvMedia) {
            return;
        }

        // upload image
        $(btnUploadImage).rvMedia({
            multiple: false,
            onSelectFiles: function onSelectFiles(files, $el) {
                var firstItem = _.first(files);
                if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && !firstItem.type.match('image')) {
                    alert('Please select an image!');
                } else if (typeof $el !== 'undefined') {
                    $el.closest('.image-box').find('.image-data').val(firstItem.id);
                    $el.closest('.image-box').find('.preview_image').attr('src', firstItem.thumb).data('rv-media', [{ 'selected_file_id': firstItem.id }]).show();
                    $el.closest('.image-box').find(btnRemoveImage).show();
                    $el.closest('.image-box').find(btnUploadImage).hide();
                }
            }
        });

        // remove image
        $('.image-box').on('click', btnRemoveImage, function (event) {
            event.preventDefault();
            $(this).closest('.image-box').find('input').val('');

            var $previewImg = $(this).closest('.image-box').find('img.preview_image'),
                defaultSrc = $previewImg.attr('data-default-src');
            if (typeof defaultSrc !== 'undefined' && defaultSrc !== '') {
                $previewImg.attr('src', defaultSrc).show();
            } else {
                $(this).closest('.image-box').find('img').hide();
            }

            $(this).closest('.image-box').find(btnUploadImage).show();
            $(this).closest('.image-box').find(btnRemoveImage).hide();
        });
        previewImageMedia();
    }

    //preview image
    function previewImageMedia() {
        $(".rvMedia_preview_file").each(function () {
            $(this).rvMedia({
                multiple: false,
                onSelectFiles: function onSelectFiles(files, $el) {
                    var firstItem = _.first(files);
                    if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && firstItem.type.match('image') && typeof $el !== 'undefined') {
                        $el.parent().find('.image-data').val(firstItem.id);
                        $el.attr('src', firstItem.thumb).data('rv-media', [{ 'selected_file_id': firstItem.id }]).show();
                    }
                }
            });
        });
    };

    function enableUploadGalleryImages() {
        var btnUploadImage = '.btn-gallery-upload-image';
        if ($(btnUploadImage).is(':visible')) {
            // upload gallery images
            $(btnUploadImage).rvMedia({
                multiple: true,
                onSelectFiles: function onSelectFiles(files, $el) {
                    var $gallery = $('.gallery-images'),
                        imagePlaceHolder = $gallery.find('li.image-placeholder');
                    if (typeof files !== 'undefined') {
                        for (var i = 0; i < files.length; i++) {
                            // only pics are processed
                            if (!files[i].type.match('image')) {
                                continue;
                            }

                            var $image = imagePlaceHolder.clone().removeClass('image-placeholder').removeClass('hidden').insertBefore(imagePlaceHolder);
                            if (typeof $image !== 'undefined' && $image) {
                                $image.attr('data-id', files[i].id);
                                $image.find('img').addClass('rvMedia_preview_file').attr('src', files[i].url).data('rv-media', [{ 'selected_file_id': files[i].id }]);
                                $image.find('a.btn-delete-gallery-img').attr('data-id', files[i].id);
                                $image.find('input[type="hidden"]').attr('value', files[i].id);
                                $image.find('input[type="hidden"]').attr('name', 'galleries[]');
                            }
                        }
                        previewImageMedia();
                    }
                }
            });

            // handle remove gallery images
            $(document).on('click', '.btn-delete-gallery-img', function (e) {
                var dataId = $(this).attr('data-id');
                if (typeof dataId !== 'undefined' && dataId && $(this).parent().attr('data-id') === dataId) {
                    $(this).parent().remove();
                }
            });
        }
    }

    function handleSelectFiles() {
        var btnTrigger = '.btn-gallery-select-file';
        if ($(btnTrigger).is(':visible')) {
            $(btnTrigger).rvMedia({
                multiple: true,
                onSelectFiles: function onSelectFiles(files, $el) {
                    var $list = $('.list-files'),
                        filePlaceHolder = $list.find('li.file-placeholder');
                    for (var i = 0; i < files.length; i++) {
                        var $file = filePlaceHolder.clone().removeClass('file-placeholder').removeClass('hidden').insertBefore(filePlaceHolder);
                        $file.attr('data-id', files[i].id);
                        $file.find('.file-icon').attr('class', files[i].icon);
                        $file.find('.file-item').attr('href', files[i].url).text(files[i].basename);
                        $file.find('a.btn-delete-file').attr('data-id', files[i].id);
                        $file.find('input[type="hidden"]').attr('value', files[i].id).attr('name', 'attachments[]');
                    }
                }
            });

            // handle remove gallery images
            $(document).on('click', '.btn-delete-file', function (e) {
                var dataId = $(this).attr('data-id');
                if (typeof dataId !== 'undefined' && dataId && $(this).parent().attr('data-id') === dataId) {
                    $(this).parent().remove();
                }
            });
        }
    }

    function initResources() {
        $('.select2, .select2-multiple').select2({
            placeholder: $(this).data('placeholder'),
            width: null
        });

        if (jQuery().timepicker) {
            $('.timepicker-default').timepicker({
                autoclose: true,
                showSeconds: true,
                minuteStep: 1
            });

            $('.timepicker-no-seconds').timepicker({
                autoclose: true,
                minuteStep: 5,
                defaultTime: false
            });

            $('.timepicker-24').timepicker({
                autoclose: true,
                minuteStep: 5,
                showSeconds: false,
                showMeridian: false
            });

            // handle input group button click
            $('.timepicker').parent('.input-group').on('click', '.input-group-btn', function (e) {
                e.preventDefault();
                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
            });
        }
    }

    return {
        //main function to initiate the module
        init: function init() {
            enableDateTimePicker();
            enableUploadFile();
            enableUploadGalleryImages();
            initResources();
            handleSelectFiles();
            autoFocusOnTheFistElm();
        }
    };
}();

jQuery(document).ready(function () {
    FomoForm.init();
});

/***/ }),
/* 10 */
/***/ (function(module, exports) {

var FomoLinkMarkedText = function () {
    var fomoRange = null;
    var fomoPopupBtnApply = '#fomo-link-marked-text .btn-link-marked-text';
    var fomoPopupSelectNews = '#fomo-news-internal-link';
    var fomoPopupLinkMarkedText = '#fomo-link-marked-text';
    var fomoBtnLinkMarkedText = '.link-marked-text';
    var fomoClassAttr = 'fomo-custom-link';

    function handleSubmitButton() {
        $(fomoPopupBtnApply).off('click').on('click', function (e) {
            // not process when button is disabled
            e.preventDefault();
            if ($(this).prop('disabled') || fomoRange == null) {
                showHideCustomLinkPopup('hide', 0);
                return;
            }
            var current = $(e.currentTarget);
            var rng = fomoRange;
            var text = rng.toString();
            rng = rng.deleteContents();
            //get url
            var newsId = $(fomoPopupSelectNews).val();
            var newsTitle = $(fomoPopupSelectNews + ' option:selected').text();
            var linkUrl = '';
            if (newsId == '' || newsId == null) {
                return;
            } else {
                linkUrl = $(fomoPopupBtnApply).attr('pattern-href') + newsId;
            }
            if (rng.nodes().length > 0) {
                if (current.attr('action') != 0) {
                    var anchor = rng.nodes()[0].parentNode;
                    current.attr('action', 0);
                } else {
                    if (text == '') {
                        return;
                    }
                    if ($(rng.nodes()[0]).prop("tagName") != "A") {
                        var anchor = rng.insertNode($('<A>' + text + '</A>')[0]);
                    } else {
                        var anchor = rng.nodes()[0];
                    }
                    anchor.text = text;
                }
            } else {
                if (text == '') {
                    return;
                }
                var anchor = rng.insertNode($('<A>' + text + '</A>')[0]);
            }

            $(anchor).attr('class', 'fomo-custom-link');
            $(anchor).attr('href', linkUrl);
            $(anchor).attr('data-news-id', newsId);
            $(anchor).attr('title', newsTitle);
            $(anchor).attr('data-type', 'text link');
            $('#description').summernote('editor.insertText', '');
            fomoRange = null;
            showHideCustomLinkPopup('hide', 0);
        });
    }

    function showHideCustomLinkPopup(action, type) {
        var markedLinkModal = $(fomoPopupLinkMarkedText);
        if (action == 'show') {
            if (fomoRange != null && typeof fomoRange.nodes()[0] !== 'undefined' && $(fomoRange.nodes()[0].parentNode).prop('tagName') == 'A') {
                var newsId = $(fomoRange.nodes()[0].parentNode).attr('data-news-id');
                var newsTitle = $(fomoRange.nodes()[0].parentNode).attr('title');
                var select = $(fomoPopupSelectNews + ' option[value="' + newsId + '"]');
                if (select.length <= 0) {
                    $(fomoPopupSelectNews).append('<option value="' + newsId + '">' + newsTitle + '</option>');
                }
                $(fomoPopupSelectNews).val(newsId).trigger('change');
                //get news title
            } else {
                $(fomoPopupSelectNews).val('').trigger('change');
            }
        }

        var btnApply = $(fomoPopupBtnApply);
        if (btnApply.length > 0) {
            btnApply.attr('action', type);
        }

        markedLinkModal.modal(action);
    }

    function handleShowCustomLinkDialog() {
        var classLink = '.' + fomoClassAttr;
        $('.note-editor .note-editable').off('click', classLink).on('click', classLink, function (e) {
            e.preventDefault();
            fomoRange = $('#description').summernote('createRange');
            showHideCustomLinkPopup('show', 1);
        });
    }

    function searchNewsPopup() {
        if ($(fomoPopupSelectNews).size() > 0 && $(fomoPopupSelectNews).attr('data-ajax-src') != '') {
            // get per page
            var perPage = $(fomoPopupSelectNews).attr('data-per-page');
            if (typeof perPage == 'undefined' || perPage == '') {
                perPage = 10;
            }
            $(fomoPopupSelectNews).select2({
                placeholder: $(fomoPopupSelectNews).attr('data-placeholder'),
                width: '100%',
                ajax: {
                    url: $(fomoPopupSelectNews).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function data(params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function processResults(data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: { more: params.page * perPage < data.total_count }
                        };
                    }
                }
            });
        }
    }

    return {
        //main function to initiate the module
        init: function init() {
            handleSubmitButton();
            handleShowCustomLinkDialog();
            searchNewsPopup();
            $(fomoBtnLinkMarkedText).off('click').on('click', function (e) {
                // description
                e.preventDefault();
                fomoRange = $('#description').summernote('createRange');
                if (fomoRange.toString() == '') {
                    return;
                }
                showHideCustomLinkPopup('show', 0);
            });
        }
    };
}();

jQuery(document).ready(function () {
    FomoLinkMarkedText.init();
});

/***/ }),
/* 11 */
/***/ (function(module, exports) {

var FomoNewsForm = function () {
    function searchAuthors() {
        var selector = '.js-author-data-ajax';
        if ($(selector).size() > 0 && $(selector).attr('data-ajax-src') != '') {
            // get per page
            var perPage = $(selector).attr('data-per-page');
            if (typeof perPage == 'undefined' || perPage == '') {
                perPage = 10;
            }

            $(selector).select2({
                placeholder: $(selector).attr('data-placeholder'),
                ajax: {
                    url: $(selector).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function data(params) {
                        return {
                            q: params.term, // search term
                            page: params.page
                        };
                    },
                    processResults: function processResults(data, params) {
                        params.page = params.page || 1;

                        return {
                            results: data.items,
                            pagination: { more: params.page * perPage < data.total_count }
                        };
                    }
                }
            });
        }
    }

    function searchNews() {
        var selector = '.js-news-data-ajax';
        if ($(selector).size() > 0 && $(selector).attr('data-ajax-src') != '') {
            var excerptNewsId = $(selector).attr('data-excerpt-news-id');
            if (typeof excerptNewsId == 'undefined' || excerptNewsId == '') {
                excerptNewsId = -1;
            }

            $(selector).select2({
                ajax: {
                    url: $(selector).attr('data-ajax-src'),
                    dataType: 'json',
                    delay: 250,
                    data: function data(params) {
                        return {
                            q: params.term, // search term
                            excerpt_news_id: excerptNewsId // excerpt current news
                        };
                    },
                    processResults: function processResults(data, params) {
                        return {
                            results: data.items
                        };
                    }
                }
            }).on('change', function () {
                /**
                 * Hook to on change event of Select2 plugin to display select result into new div
                 */
                var $selected = $(this).find('option:selected');
                var $container = $('.js-related-news-container');

                var $list = $('<ul>');
                $selected.each(function (k, v) {
                    // build detail news link
                    var detailUrl = 'javascript:;';
                    if (typeof $(selector).attr('data-news-detail-url') != 'undefined') {
                        detailUrl = $(selector).attr('data-news-detail-url').replace('-1', $(v).attr('value'));
                    }
                    var detailNewsLink = '<a target="_blank" href="' + detailUrl + '">' + $(v).text() + '</a>';

                    var $li = $('<li class="tag-selected"><a class="destroy-tag-selected">×</a>' + detailNewsLink + '</li>');
                    $li.children('a.destroy-tag-selected').off('click.select2-copy').on('click.select2-copy', function (e) {
                        var $opt = $(this).data('select2-opt');
                        $opt.attr('selected', false);
                        $opt.attr('value', ''); // no need to store duplicated data here
                        $opt.parents('select').trigger('change');
                    }).data('select2-opt', $(v));
                    $list.append($li);
                });
                $container.html('').append($list);
            }).trigger('change');
        }
    }

    return {
        //main function to initiate the module
        init: function init() {
            searchAuthors();
            searchNews();
        }
    };
}();

jQuery(document).ready(function () {
    FomoNewsForm.init();
});

/***/ }),
/* 12 */
/***/ (function(module, exports) {

var FomoNewsServices = function () {

    var modalSelector = '#fomo-news-listing-set-categories-tags',
        applyCategoryTagForm = '#apply-categories-tags-form',
        modalTitleSelector = modalSelector + ' .modal-title',
        categoriesList = modalSelector + ' .categories-list',
        tagsList = modalSelector + ' .tags-list',
        newsSelector = modalSelector + ' #news',
        typeHiddenInputSelector = modalSelector + ' input[name="type"]',
        categoryCheckboxSelector = modalSelector + ' input#category-checkbox-',
        tagCheckboxSelector = modalSelector + ' input#tag-checkbox-',
        btnSubmitSelector = modalSelector + ' .modal-footer button.btn-set-categories-tags',
        type = '',
        newsIds = [],
        goodArray = [],
        badArray = [],
        indeterminateArray = [];

    /**
     * Set categories for each selected news
     */
    function _setCategories() {
        if (newsIds.length == 0) {
            return;
        }

        $(newsSelector).val(newsIds); // assign selected news IDs to hidden input
        $(typeHiddenInputSelector).val(type); // assign type of bulk action : set categories
        openModal();
        closeModal();
        selectCheckboxes();
    }

    /**
     * Set tags for each selected news
     */
    function _setTags() {
        if (newsIds.length == 0) {
            return;
        }

        $(newsSelector).val(newsIds); // assign selected news IDs to hidden input
        $(typeHiddenInputSelector).val(type); // assign type of bulk action : set tags
        openModal();
        closeModal();
        selectCheckboxes();
    }

    /**
     * Select checkboxes when opening the modal
     */
    function selectCheckboxes() {
        var array1 = [],
            array2 = [],
            currentIds = [];

        // not do if not know set categories or set tags
        if (typeof type == 'undefined' || type == '' || $.inArray(type, ['category', 'tag']) == -1) {
            console.log('invalid type');
            return;
        }

        // not do if not have selected news
        if (newsIds.length == 0) {
            console.log('invalid selected news');
            return;
        }

        goodArray = [], badArray = [], indeterminateArray = [];
        var selector = type == 'category' ? categoryCheckboxSelector : tagCheckboxSelector,
            dataSelector = type == 'category' ? 'data-categories' : 'data-tags';
        $.each(newsIds, function (index, newsId) {
            // get id (category Ids or tag Ids) of each news and convert to array
            currentIds = $("input.checkboxes[data-news-id='" + newsId + "']").attr(dataSelector);
            currentIds = currentIds.split(',');

            // for the first loop, just add these ids to the array
            if (goodArray.length == 0) {
                goodArray = $.unique(currentIds);
                return true; // continue to next loop
            }

            array1 = $.unique(goodArray); // previous ids
            array2 = $.unique(currentIds); // current ids
            if (array1.length != array2.length || array1.join(',') !== array2.join(',')) {
                // get different items from 2 arrays
                var difference = $.unique($.merge($(array1).not(array2).get(), $(array2).not(array1).get()));
                $.merge(badArray, difference);
            }
        });

        // always disable submit button for the first time the popup dialog open
        $(btnSubmitSelector).prop('disabled', true);

        // get unique elements
        goodArray = $.unique(goodArray);
        badArray = jQuery.grep($.unique(badArray), function (n, i) {
            return n !== "" && n != null && n != 0; // remove empty item
        });

        // if element is existing in badArray, it's not allowed to exist in goodArray
        goodArray = jQuery.grep(goodArray, function (value) {
            return $.inArray(value, badArray);
        });

        // mark these checkboxes as checked state
        if (goodArray.length > 0) {
            $.each(goodArray, function (index, id) {
                $(selector + id).prop('checked', true);
            });
        }

        // mark these checkboxes as indeterminate state
        if (badArray.length > 0) {
            indeterminateArray = badArray;
            $.each(badArray, function (index, id) {
                $(selector + id).prop('indeterminate', true);
                $(selector + id).prop('checked', false);
            });
        }
    }

    /**
     * Enable submit button if "indeterminate" checkboxes are all checked
     *
     * @param string selector
     * @param int totalCheckboxesAreSelected
     */
    function enableSubmitButton(selector, totalCheckboxesSelected) {
        if (typeof selector == 'undefined' || selector == '') {
            return;
        }

        var checkboxSelector = type == 'category' ? '.categories-list input[name="categories[]"]:checked' : '.tags-list input[name="tags[]"]:checked',
            dataSelector = type == 'category' ? 'data-categories' : 'data-tags',
            currentSelectedIds = $(checkboxSelector).map(function () {
            return this.value;
        }).get(),
            finalSelectedIds = indeterminateArray.concat(currentSelectedIds),
            isDisabled = false;

        // must set categories & tags for each news row
        $.each(newsIds, function (index, newsId) {
            var currentIds = $("input.checkboxes[data-news-id='" + newsId + "']").attr(dataSelector).split(',');
            var finalIdsExistInCurrentRow = finalSelectedIds.filter(function (elem) {
                return currentIds.indexOf(elem) != -1;
            }).length;
            if (finalIdsExistInCurrentRow == 0 && totalCheckboxesSelected <= 0) {
                $(btnSubmitSelector).prop('disabled', true);
                isDisabled = true;
                return false;
            }
        });

        if (isDisabled) {
            return;
        }

        if (totalCheckboxesSelected > 0 || totalCheckboxesSelected == 0 && indeterminateArray.length == badArray.length) {
            $(btnSubmitSelector).removeAttr('disabled');
        }
    }

    /**
     * Open modal for 2 type: category or tag
     */
    function openModal() {
        if (type == 'category') {
            $(modalTitleSelector).text('Set categories for selected news');
            $(categoriesList).removeClass('hidden');
            $(tagsList).addClass('hidden');
            $(modalSelector).modal('show');

            // listen on change each checkboxes to enable submit button
            $(document).on('change', modalSelector + ' input[name="categories[]"]', function () {
                handleStateOfIndeterminateCheckboxes($(this));
                var totalSelectedCheckboxes = $('.categories-list input[name="categories[]"]:checked').length;
                enableSubmitButton(modalSelector + ' input[name="categories[]"]', totalSelectedCheckboxes);
            });
        } else if (type == 'tag') {
            $(modalTitleSelector).text('Set tags for selected news');
            $(categoriesList).addClass('hidden');
            $(tagsList).removeClass('hidden');
            $(modalSelector).modal('show');

            // listen on change each checkboxes to enable submit button
            $(document).on('change', modalSelector + ' input[name="tags[]"]', function () {
                handleStateOfIndeterminateCheckboxes($(this));
                var totalSelectedCheckboxes = $('.tags-list input[name="tags[]"]:checked').length;
                enableSubmitButton(modalSelector + ' input[name="tags[]"]', totalSelectedCheckboxes);
            });
        }
    }

    /**
     * On closing the modal, reset controls to default value and remove all event listeners
     */
    function closeModal() {
        $(modalSelector).on('hidden.bs.modal', function () {
            // restore these controls to default value
            $(modalSelector + ' input[name="categories[]"]').removeAttr('checked');
            $(modalSelector + ' input[name="categories[]"]').prop('indeterminate', false);
            $(modalSelector + ' input[name="tags[]"]').removeAttr('checked');
            $(modalSelector + ' input[name="tags[]"]').prop('indeterminate', false);
            $(btnSubmitSelector).removeAttr('disabled');
            $(applyCategoryTagForm + ' input[name="indeterminate_checkboxes"]').remove();
            type = '', goodArray = [], badArray = [], indeterminateArray = [], newsIds = [];

            // remove event listeners
            $(modalSelector).off('hidden.bs.modal');
            $(modalSelector + ' input[name="categories[]"]').off('change');
            $(modalSelector + ' input[name="tags[]"]').off('change');
            $(btnSubmitSelector).off('click');
        });
    }

    /**
     * Call ajax to store categories/tags for selected news
     */
    function handleSubmitButton() {
        $(btnSubmitSelector).off('click');
        $(document).on('click', btnSubmitSelector, function () {
            // not process when button is disabled
            if ($(this).prop('disabled')) {
                return;
            }

            var ajaxUrl = $(modalSelector).find('input#ajax-url').val();
            if (typeof ajaxUrl != 'undefined' && ajaxUrl != '') {

                // also send indeterminate checkboxes to the server
                $(applyCategoryTagForm + ' input[name="indeterminate_checkboxes"]').remove();
                $('<input>').attr({
                    type: 'hidden',
                    name: 'indeterminate_checkboxes',
                    value: indeterminateArray.join(',')
                }).appendTo(applyCategoryTagForm);

                $.ajax({
                    url: ajaxUrl,
                    type: 'POST',
                    cache: false,
                    processData: false,
                    contentType: false,
                    data: new FormData($(applyCategoryTagForm)[0]),
                    beforeSend: function beforeSend() {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function success(response) {
                        $.unblockUI();
                        $(modalSelector).modal('hide');
                        if (response.errors == false) {
                            toastr.success(response.data.message);

                            // update UI for newly categories/tags
                            if (typeof response.data.news != 'undefined' && response.data.news.length) {
                                $.each(response.data.news, function (index, newsItem) {
                                    // update categories
                                    if (response.data.type == 'category') {
                                        $('input[data-news-id="' + newsItem.id + '"]').attr('data-categories', newsItem.category_ids);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.category-cell').html(newsItem.category_names_with_link);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.category-cell').pulsate({
                                            color: "#51a351",
                                            repeat: false
                                        });
                                    } else if (response.data.type == 'tag') {
                                        // update tags
                                        $('input[data-news-id="' + newsItem.id + '"]').attr('data-tags', newsItem.tag_ids);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.tag-cell').html(newsItem.tag_names_with_link);
                                        $('input[data-news-id="' + newsItem.id + '"]').closest('tr').find('td.tag-cell').pulsate({
                                            color: "#51a351",
                                            repeat: false
                                        });
                                    }
                                });
                            }
                        }
                    },
                    error: function error(jqXHR, exception) {
                        $.unblockUI();
                        if (jqXHR.status === 400) {
                            toastr.error(jqXHR.responseJSON.message);
                        }
                    }
                });
            }
        });
    }

    /**
     * Change to checked instead of un-check when the checkbox is in "indeterminate" state
     * @param checkbox $element
     */
    function handleStateOfIndeterminateCheckboxes($element) {
        // if this checkbox is one of indeterminate checkboxes
        if ($.inArray($element.attr('data-id'), indeterminateArray) != -1) {
            // if after clicked, it's still not check -> check it
            if (!$element.prop('checked')) {
                $element.indeterminate = false;
                $element.prop('checked', true);
            }

            // remove indeterminate item
            var removeItem = $element.attr('data-id');
            indeterminateArray = jQuery.grep(indeterminateArray, function (value) {
                return value != removeItem;
            });
        }
    }

    function _bulkActions(ids, ajaxUrl, method, filter) {
        if (ids.length == 0) {
            return;
        }
        $.ajax({
            url: ajaxUrl,
            type: method,
            cache: false,
            data: { 'ids': ids },
            beforeSend: function beforeSend() {
                $.blockUI(FOMO.blockMetronicUI);
            },
            success: function success(response) {
                $.unblockUI();
                $('.table-container .table-group-action-input').val('');
                if (response.errors == false) {
                    toastr.success(response.data.message);
                    TableDatatablesAjax.filterAllOrTrashed(filter);
                }
            },
            error: function error(jqXHR, exception) {
                $.unblockUI();
                if (jqXHR.status === 400) {
                    toastr.error(jqXHR.responseJSON.message);
                }
            }
        });
    }

    return {
        init: function init() {
            handleSubmitButton();
        },

        setCategories: function setCategories(ids) {
            newsIds = ids;
            type = 'category';
            _setCategories();
        },

        setTags: function setTags(ids) {
            newsIds = ids;
            type = 'tag';
            _setTags();
        },
        bulkActions: function bulkActions(ids, ajaxUrl, method, filter) {
            if (typeof method == 'undefined') {
                method = 'post';
            }
            filter = filter == 'bulk_trash' ? '' : 1;
            _bulkActions(ids, ajaxUrl, method, filter);
        }
    };
}();

jQuery(document).ready(function () {
    FomoNewsServices.init();
});

/***/ }),
/* 13 */
/***/ (function(module, exports) {

var FormValidation = function () {

    var handleFormValidation = function handleFormValidation() {
        // for more info visit the official plugin documentation:
        // http://docs.jquery.com/Plugins/Validation

        var forms = $('.form-validation');
        var error = $('.alert-danger', forms);
        var success = $('.alert-success', forms);

        forms.validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block help-block-error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "", // validate all fields including form hidden input

            invalidHandler: function invalidHandler(event, validator) {
                //display error alert on form submit
                success.hide();
                error.show();
                App.scrollTo(error, -200);
            },

            errorPlacement: function errorPlacement(error, element) {
                // render error placement for each input type
                var cont = $(element).parent('.input-group');
                if (cont.size() > 0) {
                    cont.after(error);
                } else {
                    element.after(error);
                }
            },

            highlight: function highlight(element) {
                // hightlight error inputs

                $(element).closest('.form-group').addClass('has-error'); // set error class to the control group
            },

            unhighlight: function unhighlight(element) {
                // revert the change done by hightlight
                $(element).closest('.form-group').removeClass('has-error'); // set error class to the control group
            },

            success: function success(label) {
                label.closest('.form-group').removeClass('has-error'); // set success class to the control group
            }

            //submitHandler: function (form) {
            //    success.show();
            //    error.hide();
            //}
        });

        // validate these controls when changing
        $('.select2, .multi-select').change(function () {
            forms.validate().element($(this));
        });
    };

    var handleWysihtml5 = function handleWysihtml5() {
        if (!jQuery().wysihtml5) {
            return;
        }

        if ($('.wysihtml5').size() > 0) {
            $('.wysihtml5').wysihtml5({
                "stylesheets": ["../assets/global/plugins/bootstrap-wysihtml5/wysiwyg-color.css"],
                "events": {
                    "blur": function blur() {
                        // hide error message if the editor is not empty
                        if (this.textareaElement.value.length > 0) {
                            $(this.textareaElement).closest('.form-group').removeClass('has-error');
                            $(this.textareaElement).siblings().find('.help-block.help-block-error').hide();
                        }
                    }
                }
            });
        }
    };

    var handleSummernote = function handleSummernote() {
        if ($('.summernote').size() > 0) {
            $('.summernote').summernote({
                // customize summernote toolbar, @ticket #13187
                toolbar: [['style', ['style']], ['font', ['bold', 'italic', 'underline', 'clear']], ['color', ['color']], ['para', ['ul', 'ol', 'paragraph']], ['insert', ['link']], ['misc', ['fullscreen']]],
                height: 300,
                callbacks: {
                    onBlur: function onBlur() {
                        if ($(this).summernote('isEmpty')) {
                            $(this).parent().parent('.form-group').addClass('has-error');
                            $(this).parent().find('.help-block.help-block-error').show();
                            $(this).summernote('code', '');
                        } else {
                            // hide error message if the editor is not empty
                            $(this).parent().parent('.form-group').removeClass('has-error');
                            $(this).parent().find('.help-block.help-block-error').hide();
                        }
                    }
                }
            });
        }
    };

    return {
        //main function to initiate the module
        init: function init() {

            handleSummernote();
            handleWysihtml5();
            handleFormValidation();
        }

    };
}();

jQuery(document).ready(function () {
    FormValidation.init();
});

/***/ }),
/* 14 */
/***/ (function(module, exports) {

var FomoPageBar = function () {
    var originForm;

    /**
     * Handle click on the Save icon on the page bar
     * @author vulh
     * */
    function handleSaveFormBtn() {
        $('.page-bar').on('click', FOMO.saveBtnForm, function (e) {
            if ($(this).hasClass(FOMO.overrideSaveFrmClass)) {
                return false;
            }
            e.preventDefault();
            $(this).closest('.page-content').find('form.form-validation:not(.rv-form)').submit();
        });
    }

    /**
     * Handle click on the Delete forever icon on the page bar
     * @author vulh
     * */
    function handleDeleteForeverFormBtn() {
        $('.page-bar').on('click', '#fomo-single-delete-btn', function (e) {
            e.preventDefault();
            $('#fomo-single-confirm-delete-modal').modal('show');
        });
    }

    /**
     * Handle click on the View icon on the page bar
     * */
    function handleViewFormBtn() {
        $('.page-bar').on('click', '#fomo-view-btn', function (e) {
            askBeforeLeave(e);
        });
    }

    /**
     * Handle click on the Create icon on the page bar
     * */
    function handleCreateFormBtn() {
        $('.page-bar').on('click', '#fomo-create-btn', function (e) {
            askBeforeLeave(e);
        });
    }

    /**
     * Ask to confirm user before leave to new page (handle for View & Create buttons in the breadcrumb page bar)
     */
    function askBeforeLeave(e) {
        // if the form has changed
        if (typeof originForm != 'undefined' && originForm && $('form.form-validation:not(.rv-form)').serialize() != originForm) {
            e.preventDefault();

            var $anchor = $(e.currentTarget);
            swal({
                type: 'info',
                title: 'Confirm',
                text: 'Do you want to leave?',
                allowOutsideClick: true,
                showCancelButton: true,
                confirmButtonClass: 'green'
            }, function (isConfirmOK) {
                if (isConfirmOK) {
                    window.location.href = $anchor.attr('href');
                }
            });
        }
    }

    return {
        //main function to initiate the module
        init: function init() {

            setTimeout(function () {
                if ($('form.form-validation:not(.rv-form)')) {
                    originForm = $('form.form-validation:not(.rv-form)').serialize();
                }
            }, 1000);

            handleSaveFormBtn();
            handleDeleteForeverFormBtn();

            // ask user before leave
            handleViewFormBtn();
            handleCreateFormBtn();
        }
    };
}();

jQuery(document).ready(function () {
    FomoPageBar.init();
});

/***/ }),
/* 15 */
/***/ (function(module, exports) {

var TableDatatablesAjax = function () {

    var dataTable;

    var initPickers = function initPickers() {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: App.isRTL(),
            autoclose: true
        });
    };

    var addColumnsDynamically = function addColumnsDynamically() {
        // set defined columns name
        var columns = [];
        $("table[id$='datatable'] thead tr th.heading-columns").each(function (key, item) {
            var columnName = $(item).attr('data-col-name');
            if (typeof columnName != 'undefined' && columnName != '') {
                // add custom class names for each cell
                var columnClassName = columnName == 'status' ? 'dt-body-center' : '';
                if (columnName == 'category_names_with_link') {
                    columnClassName = 'category-cell';
                }
                if (columnName == 'tag_names_with_link') {
                    columnClassName = 'tag-cell';
                }

                columns.push({
                    data: columnName,
                    name: columnName,
                    orderable: $(item).attr('data-col-orderable') === '1',
                    className: columnClassName,

                    // if data is displayed by HTML format (i.e: status, image) -> render html
                    mRender: function mRender(data, type, row) {
                        var html = $(item).attr('data-html');
                        if (typeof html != 'undefined' && html != '') {
                            if (columnName == 'status' && data == 'pending') {
                                html = html.replace('fa-check font-green-jungle', 'fa-refresh font-red');
                                html = html.replace('Publish', 'Pending');
                            }
                            return html.replace('#data#', row[columnName]);
                        }

                        return data;
                    }
                });
            }
        });

        // render "Actions" column
        var $tableActions = $("table[id$='datatable'] .table-actions");
        if (typeof $tableActions != 'undefined' && $tableActions.length > 0) {
            columns.push({
                orderable: false,
                className: "dt-body-center",
                mRender: function mRender(data, type, row) {
                    var showRoute = $tableActions.attr('data-show-action-route'),
                        editRoute = $tableActions.attr('data-edit-action-route'),
                        deleteRoute = $tableActions.attr('data-delete-action-route'),
                        assignRoute = $tableActions.attr('data-assign-action-route'),
                        restoreRoute = $tableActions.attr('data-restore-action-route'),
                        trashRoute = $tableActions.attr('data-trash-action-route');

                    var showAction = '',
                        editAction = '',
                        deleteAction = '',
                        assignAction = '',
                        restoreActon = '',
                        trashActon = '';
                    if (typeof showRoute != 'undefined' && showRoute != '') {
                        showAction = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-show-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-show-role") + '" href="' + showRoute + '" class="tooltips btn btn-table-show ' + $tableActions.attr("data-show-action-color") + '"><i class="' + $tableActions.attr("data-show-action-icon") + '"></i> ' + $tableActions.attr("data-show-action-name") + '</a>';
                        showAction = showAction.replace("-1", row.id);
                    }
                    if (typeof editRoute != 'undefined' && editRoute != '') {
                        editAction = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-edit-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-edit-role") + '" href="' + editRoute + '" class="tooltips btn btn-table-edit ' + $tableActions.attr("data-edit-action-color") + '"><i class="' + $tableActions.attr("data-edit-action-icon") + '"></i> ' + $tableActions.attr('data-edit-action-name') + '</a>';
                        editAction = editAction.replace("-1", row.id);
                    }
                    if (typeof assignRoute != 'undefined' && assignRoute != '') {
                        assignAction = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-assign-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-assign-role") + '" href="' + assignRoute + '" class="tooltips btn btn-table-assign ' + $tableActions.attr("data-assign-action-color") + '"><i class="' + $tableActions.attr("data-assign-action-icon") + '"></i> ' + $tableActions.attr('data-assign-action-name') + '</a>';
                        assignAction = assignAction.replace("-1", row.id);
                    }
                    if (typeof restoreRoute != 'undefined' && restoreRoute != '') {
                        restoreActon = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-restore-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-restore-role") + '" href="' + restoreRoute + '" class="tooltips btn btn-table-restore ' + $tableActions.attr("data-restore-action-color") + '"><i class="' + $tableActions.attr("data-restore-action-icon") + '"></i> ' + $tableActions.attr('data-restore-action-name') + '</a>';
                        restoreActon = restoreActon.replace("-1", row.id);
                    }
                    if (typeof trashRoute != 'undefined' && trashRoute != '') {
                        trashActon = '<a data-placement="top" data-original-title="' + $tableActions.attr("data-trash-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-trash-role") + '" href="' + trashRoute + '" class="tooltips btn btn-table-trash ' + $tableActions.attr("data-trash-action-color") + '"><i class="' + $tableActions.attr("data-trash-action-icon") + '"></i> ' + $tableActions.attr('data-trash-action-name') + '</a>';
                        trashActon = trashActon.replace("-1", row.id);
                    }
                    if (typeof deleteRoute != 'undefined' && deleteRoute != '') {
                        deleteAction = '<a data-placement="top" data-original-title="' + $tableActions.attr("tooltips data-delete-action-tooltips") + '" data-acl-role="' + $tableActions.attr("data-delete-role") + '" href="#listing-confirm-delete-modal" data-toggle="modal" data-href="' + deleteRoute + '" ' + ' class="btn btn-delete btn-table-destroy ' + $tableActions.attr("data-delete-action-color") + '"><i class="' + $tableActions.attr("data-delete-action-icon") + '"></i> ' + $tableActions.attr('data-delete-action-name') + '</a>';
                        deleteAction = deleteAction.replace("-1", row.id);
                    }

                    return showAction + " " + editAction + " " + trashActon + " " + restoreActon + " " + deleteAction + " " + assignAction;
                }
            });
        }

        return columns;
    };

    var initTable = function initTable($table) {

        var grid = new Datatable();

        //check action filter before
        if (localStorage.getItem("datatable_filter") == "trash") {
            localStorage.removeItem('datatable_filter');
            $("table[id$='datatable']").attr('data-only-trashed', 1);
            activeFilterTrash();
        }

        var recordPerPage = parseInt($("table[id$='datatable']").attr('data-record-per-page'));
        grid.init({
            src: $table,
            onSuccess: function onSuccess(grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function onError(grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function onDataLoad(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical datatable settings from http://datatables.net/usage/options

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the datatable cells. The default datatable layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/datatable.js).
                // So when dropdowns used the scrollable div should be removed.
                "dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                // save datatable state(pagination, sort, etc) in cookie.
                "bStateSave": true,

                // save custom filters to the state
                "fnStateSaveParams": function fnStateSaveParams(oSettings, sValue) {
                    $("table[id$='datatable'] tr.filter .form-control").each(function () {
                        sValue[$(this).attr('name')] = $(this).val();
                    });

                    return sValue;
                },

                // read the custom filters from saved state and populate the filter inputs
                "fnStateLoadParams": function fnStateLoadParams(oSettings, oData) {
                    //Load custom filters
                    $("table[id$='datatable'] tr.filter .form-control").each(function () {
                        var element = $(this);
                        if (oData[element.attr('name')]) {
                            element.val(oData[element.attr('name')]);
                        }
                    });

                    return true;
                },

                "lengthMenu": [[recordPerPage, recordPerPage + 10, recordPerPage + 20, -1], [recordPerPage, recordPerPage + 10, recordPerPage + 20, "All"] // change per page values here
                ],
                "pageLength": recordPerPage, // default record count per page
                "ajax": {
                    "url": $("table[id$='datatable']").attr('data-ajax-src'), // ajax source
                    "data": function data(d) {
                        d.page = d.start / d.length + 1; // set "page" param before send AJAX request to server

                        if ($("table[id$='datatable']").attr('data-only-trashed') === '1') {
                            d.only_trashed = true;
                        }

                        // set default sorting is DESC
                        if (d.draw == 1 && d.page == 1 && typeof d.order[0] != 'undefined' && d.order[0].dir == 'asc') {
                            var $table = $('table[id$="datatable"]').dataTable();
                            if (typeof $table != 'undefined') {
                                $table.fnSort([[0, 'desc']]);
                            }
                        }

                        $.blockUI(FOMO.blockMetronicUI);
                    }
                },
                "columns": addColumnsDynamically(),
                "ordering": true,
                fnServerParams: function fnServerParams(data) {
                    data['order'].forEach(function (items, index) {
                        data['order'][index]['column'] = data['columns'][items.column]['data'];
                    });

                    var $table = $('table[id$="datatable"]').dataTable();
                    if (typeof $table != 'undefined' && $("table[id$='datatable']").data('sort-default') != null && $("table[id$='datatable']").data('sort-default') != '') {
                        data['order'][0]['column'] = $("table[id$='datatable']").data('sort-default');
                        data['order'][0]['dir'] = $("table[id$='datatable']").data('sort-order-default');
                        $("table[id$='datatable']").data('sort-default', '');
                        $("table[id$='datatable']").data('sort-order-default', '');
                    }
                },
                // hide pagination feature if only one page
                fnDrawCallback: function fnDrawCallback(oSettings) {
                    $.unblockUI();

                    // set filter select drop down searchable
                    $("select[name='category_names_with_link']").select2();
                    $("select[name='tag_names_with_link']").select2();

                    // remove sorting in checkbox column
                    $("table[id$='datatable'] thead tr th.heading-columns.heading-columns-checkbox").removeClass('sorting_asc').removeClass('sorting_desc').addClass('sorting_disabled');

                    // after change page (next|prev) -> reset Select All checkbox
                    if ($('table[id$="datatable"] thead input[name="select_all"]').is(':visible') && $('table[id$="datatable"] thead input[name="select_all"]').prop('checked')) {
                        $('table[id$="datatable"] thead input[name="select_all"]').prop('checked', false);
                    }

                    // show/hide buttons
                    var onlyTrash = $("table[id$='datatable']").attr('data-only-trashed');
                    if ($('#filter-navigation').is(':visible') && typeof onlyTrash != 'undefined') {
                        // if "All"
                        if (onlyTrash === '') {
                            $('.btn-table-restore').hide();
                            $('.btn-table-destroy').hide();

                            $('.btn-table-show').show();
                            $('.btn-table-edit').show();
                            $('.btn-table-trash').show();
                        } else {
                            // if filter by trashed
                            $('.btn-table-restore').show();
                            $('.btn-table-destroy').show();

                            $('.btn-table-show').hide();
                            $('.btn-table-edit').hide();
                            $('.btn-table-trash').hide();
                        }
                    }

                    aclFn.init();
                }
            }
        });

        // handle group action submit button click
        grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
            e.preventDefault();
            var action = $(".table-group-action-input", grid.getTableWrapper());
            if (action.val() != "" && grid.getSelectedRowsCount() > 0) {

                // set multiple categories/tags for news
                try {
                    switch (action.val()) {
                        case 'set_categories':
                            FomoNewsServices.setCategories(grid.getSelectedRows());
                            return;
                        case 'set_tags':
                            FomoNewsServices.setTags(grid.getSelectedRows());
                            return;
                        case 'bulk_trash':
                        case 'restore':
                        case 'delete':
                            FomoNewsServices.bulkActions(grid.getSelectedRows(), action.find(':selected').data('ajax-url'), action.find(':selected').data('method'), action.val());
                            return;
                    }
                } catch (err) {}
                grid.setAjaxParam("customActionType", "group_action");
                grid.setAjaxParam("customActionName", action.val());
                grid.setAjaxParam("id", grid.getSelectedRows());
                grid.getDataTable().ajax.reload();
                grid.clearAjaxParams();
            } else if (action.val() == "") {
                if (typeof toastr != 'undefined') {
                    toastr.error('Please select an action');
                } else {
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'Please select an action',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            } else if (grid.getSelectedRowsCount() === 0) {
                if (typeof toastr != 'undefined') {
                    toastr.error('No record selected');
                } else {
                    App.alert({
                        type: 'danger',
                        icon: 'warning',
                        message: 'No record selected',
                        container: grid.getTableWrapper(),
                        place: 'prepend'
                    });
                }
            }
        });

        //grid.setAjaxParam("customActionType", "group_action");
        //grid.getDataTable().ajax.reload();
        //grid.clearAjaxParams();

        dataTable = grid.getDataTable();
    };

    /**
     * @var status
     *      + '' : load all items
     *      + 1  : load only trashed items
     *
     * @param status
     */
    var filterTableItems = function filterTableItems(status) {
        // hide un-necessary alerts
        $('div.ustom-alerts.alert').hide();

        // change table status (load trashed item or not)
        $("table[id$='datatable']").attr('data-only-trashed', status);

        // re-get data
        dataTable.draw();
    };

    return {

        //main function to initiate the module
        init: function init() {
            var tables = $("table[id$='datatable']");
            if (tables.size() > 0) {
                $(tables).each(function () {
                    var $tbl = $(this);
                    initPickers();
                    initTable($tbl);
                });
            }
        },

        getDataTable: function getDataTable() {
            return dataTable;
        },

        filterAllOrTrashed: function filterAllOrTrashed(status) {
            filterTableItems(status);
        }

    };
}();

function activeFilterTrash() {
    $('.table-container .table-group-action-input').find("option[value*='bulk_trash']").hide();
    $('.table-container .table-group-action-input').find("option[value*='restore']").show();
    $('.table-container .table-group-action-input').find("option[value*='delete']").show();
    $('.table-container .table-group-action-input').val('');
    $("#filter-navigation .btn-group .action-filter").removeClass('active');
    $("#datatable-btn-trashed").addClass('active');
}

function activeFilterAll() {
    $('.table-container .table-group-action-input').find("option").show();
    $('.table-container .table-group-action-input').find("option[value*='restore']").hide();
    $('.table-container .table-group-action-input').find("option[value*='delete']").hide();
    $('.table-container .table-group-action-input').val('');
    $("#filter-navigation .btn-group .action-filter").removeClass('active');
    $("#datatable-btn-all").addClass('active');
}

jQuery(document).ready(function () {
    TableDatatablesAjax.init();

    var dataTable = TableDatatablesAjax.getDataTable();

    // filter column data by input text
    var timer = null,
        inputColumnIndex = null,
        inputValue = null;
    $(document).on('keyup', 'input.form-control.form-filter.input-sm', function () {
        clearTimeout(timer);
        if ($(this).val().length == 0 || $(this).val().length >= 2) {
            inputColumnIndex = $(this).data().columnIndex;
            inputValue = $(this).val();
            timer = setTimeout(function () {
                if (typeof dataTable != 'undefined' && typeof inputColumnIndex != 'undefined' && typeof inputValue != 'undefined') {
                    dataTable.columns(inputColumnIndex).search(inputValue, true, false).draw();
                    inputColumnIndex = null;
                    inputValue = null;
                }
            }, 500);
        }
    });

    // filter column data by select drop down
    $(document).on('change', 'select.filter-select', function () {
        var columnIndex = $(this).data().columnIndex;
        if (typeof dataTable != 'undefined' && typeof columnIndex != 'undefined') {
            dataTable.columns(columnIndex).search($(this).val()).draw();
        }
    });

    // filter date column
    $(document).on('change', 'input.fomo-datetime-filter', function () {
        var columnIndex = $(this).data().columnIndex;
        if (typeof dataTable != 'undefined' && typeof columnIndex != 'undefined') {
            dataTable.columns(columnIndex).search($(this).val()).draw();
        }
    });

    /***
     * delete button clicked
     */
    $(document).on('click', '.btn-delete', function (e) {
        e.preventDefault();
        $('div.modal').find('.btn-action').attr('data-href', $(this).attr('data-href'));
    });

    /***
     * confirm button clicked
     */
    $('div.modal').on('click', '.btn-action', function (e) {
        if ($('div.modal').find('form').length > 0) {
            $('div.modal').find('form').attr('action', $(this).attr('data-href'));
        } else {
            e.preventDefault();
            window.location.replace($(this).attr('data-href'));
        }
    });

    /***
     * restore button clicked
     */
    $(document).on('click', 'a.btn-table-restore', function (e) {
        localStorage.setItem("datatable_filter", "trash");
    });

    /***
     * delete button clicked
     */
    $(document).on('submit', '#listing-confirm-delete-modal form', function (e) {
        localStorage.setItem("datatable_filter", "trash");
    });

    /***
     * Filter data by trashed items
     */

    $(document).on('click', '#datatable-btn-trashed', function () {
        activeFilterTrash();
        TableDatatablesAjax.filterAllOrTrashed(1);
    });

    /***
     * Get all (not trashed items)
     */
    $(document).on('click', '#datatable-btn-all', function () {
        activeFilterAll();
        TableDatatablesAjax.filterAllOrTrashed('');
    });

    // handle filter cancel button click
    if ($('.btn-filter-cancel').length) {
        dataTable.on('click', '.btn-filter-cancel', function (e) {
            e.preventDefault();

            // clear filter data
            $('textarea.form-filter, select.form-filter, input.form-filter').each(function () {
                $(this).val('');
            });
            $('input.form-filter[type="checkbox"]').each(function () {
                $(this).attr('checked', false);
            });

            // reset pagination to default page length
            dataTable.page.len(parseInt($("table[id$='datatable']").attr('data-record-per-page')));

            // reset sorting to desc
            var $table = $('table[id$="datatable"]').dataTable();
            $table.fnSort([[0, 'desc']]);

            dataTable.search('').columns().search('').draw();
        });
    }

    // updates "Select all" control in a data table
    function updateDataTableSelectAllCtrl() {
        var $table = dataTable.table().node();
        var $chkboxAll = $('tbody input[type="checkbox"]', $table);
        var $chkboxChecked = $('tbody input[type="checkbox"]:checked', $table);
        var chkboxSelectAll = $('thead input[name="select_all"]', $table).get(0);

        // if none of the checkboxes are checked
        if ($chkboxChecked.length === 0) {
            chkboxSelectAll.checked = false;
            if ('indeterminate' in chkboxSelectAll) {
                chkboxSelectAll.indeterminate = false;
            }
        } else if ($chkboxChecked.length === $chkboxAll.length) {
            // if all of the checkboxes are checked
            chkboxSelectAll.checked = true;
            if ('indeterminate' in chkboxSelectAll) {
                chkboxSelectAll.indeterminate = false;
            }
        } else {
            // if some of the checkboxes are checked
            chkboxSelectAll.checked = true;
            if ('indeterminate' in chkboxSelectAll) {
                chkboxSelectAll.indeterminate = true;
            }
        }
    }

    // array holding selected row IDs
    var rows_selected = [];

    // handle click on checkbox
    $('table[id$="datatable"] tbody').on('click', 'input[type="checkbox"]', function (e) {
        var $row = $(this).closest('tr');

        // get row data
        var data = dataTable.row($row).data();

        // get row ID
        var rowId = data.id;

        // determine whether row ID is in the list of selected row IDs
        var index = $.inArray(rowId, rows_selected);

        // if checkbox is checked and row ID is not in list of selected row IDs
        if (this.checked && index === -1) {
            rows_selected.push(rowId);
        } else if (!this.checked && index !== -1) {
            // otherwise, if checkbox is not checked and row ID is in list of selected row IDs
            rows_selected.splice(index, 1);
        }

        // update state of "Select all" control
        updateDataTableSelectAllCtrl();

        // prevent click event from propagating to parent
        e.stopPropagation();
    });

    // handle click on table cells with checkboxes
    $('table[id$="datatable"]').on('click', 'tr td:first-child, thead th:first-child', function (e) {
        $(this).parent().find('input[type="checkbox"]').trigger('click');
    });

    // handle click on "Select all" control
    if (typeof dataTable != 'undefined' && dataTable) {
        $('thead input[name="select_all"]', dataTable.table().container()).on('click', function (e) {
            if (this.checked) {
                $('table[id$="datatable"] tbody input[type="checkbox"]:not(:checked)').trigger('click');
            } else {
                $('table[id$="datatable"] tbody input[type="checkbox"]:checked').trigger('click');
            }

            // prevent click event from propagating to parent
            e.stopPropagation();
        });
    }
});

/***/ }),
/* 16 */
/***/ (function(module, exports) {

var FomoToastr = function () {

    /**
     * Handle display message by toastr
     * @author vulh
     * */
    function handleDisplayAlertMessageByToastr() {
        if ($('#fomo-alert-success').length) {
            toastr.success($('#fomo-alert-success').html());
        }

        if ($('#fomo-alert-error').length) {
            toastr.error($('#fomo-alert-error').html());
        }
    }

    return {
        //main function to initiate the module
        init: function init() {
            handleDisplayAlertMessageByToastr();
        }
    };
}();

jQuery(document).ready(function () {
    FomoToastr.init();
});

/***/ }),
/* 17 */
/***/ (function(module, exports) {

var FomoUserForm = function () {

    /**
     * Handle click on the Save icon on the page bar
     * @author vandd
     * */
    function handleSaveFormBtn() {
        $('.page-bar').on('click', FOMO.saveBtnForm, function (e) {
            if ($(this).attr('data-ajax-url') != '') {
                $.ajax({
                    url: $(this).attr('data-ajax-url'),
                    type: 'POST',
                    data: {
                        'email': $('#email').val(),
                        'user_id': $(this).attr('user-id')
                    },
                    beforeSend: function beforeSend() {
                        $.blockUI(FOMO.blockMetronicUI);
                    },
                    success: function success(response) {
                        $.unblockUI();
                        response = JSON.parse(response);
                        var emailElement = $('#email').closest('.form-group');
                        if (response.data.email) {
                            var errorText = emailElement.find('#email-error');
                            if (!emailElement.hasClass('has-error')) {
                                emailElement.addClass('has-error');
                            }

                            if (errorText.length > 0) {
                                $(errorText).text(response.data.message);
                            }
                        } else {
                            $(FOMO.saveBtnForm).closest('.page-content').find('form:not(.rv-form)').submit();
                        }
                    },
                    error: function error(jqXHR, exception) {
                        $.unblockUI();
                    }
                });
            }
        });
    }

    return {
        //main function to initiate the module
        init: function init() {
            handleSaveFormBtn();
        }
    };
}();

jQuery(document).ready(function () {
    FomoUserForm.init();
});

/***/ }),
/* 18 */
/***/ (function(module, exports) {

FomoUserUpdateProfile = function () {

    /**
     *
     * @author hieuluong
     * */
    function handle() {
        $(document).on('click', '#admin-bar-edit-profile', function (e) {
            e.preventDefault();
            $.ajax({
                url: $(this).data('url'),
                type: 'GET',
                beforeSend: function beforeSend() {
                    $.blockUI(FOMO.blockMetronicUI);
                    $("#fomo-modal-update-profile").modal('show').find(".modal-content .spinner-loading").show();
                },

                success: function success(response) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    $("#fomo-modal-update-profile .modal-body").html(response);
                    $("#fomo-modal-update-profile .rvMedia_preview_file_avatar, #fomo-modal-update-profile .btn-upload-image").rvMedia({
                        multiple: false,
                        onSelectFiles: function onSelectFiles(files, $el) {
                            var firstItem = _.first(files);
                            if (typeof firstItem !== 'undefined' && typeof firstItem.type !== 'undefined' && firstItem.type.match('image') && typeof $el !== 'undefined') {
                                console.log($el.parent().find('.preview_image'));
                                $el.parent().find('.image-data').val(firstItem.id);
                                $el.parent().find('.preview_image').attr('src', firstItem.thumb).data('rv-media', [{ 'selected_file_id': firstItem.id }]).show();
                                $el.parent().find(".btn-remove-image").show();
                            }
                        }
                    });
                    $("#fomo-modal-update-profile .select2").select2();
                },
                error: function error(jqXHR, exception) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                }
            });
        });

        $(document).on('submit', '#fomo-modal-update-profile form', function (e) {
            e.preventDefault();
            var formData = $(this).serializeArray();
            $.each(formData, function (i, input) {
                if (input.name != 'password' && input.name != 'language' && input.value == '') delete input.name;
            });
            formData.push({ name: 'image_id', value: $("#fomo-modal-update-profile form input[name=image_id]").val() });

            $.ajax({
                url: $(this).data('url'),
                type: 'PUT',
                data: formData,
                beforeSend: function beforeSend() {
                    $.blockUI(FOMO.blockMetronicUI);
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").show();
                },
                success: function success(response) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    $("#fomo-modal-update-profile").modal('hide');
                    if (response.errors == false) {
                        toastr.success(response.data.message);
                    }
                },
                error: function error(jqXHR, exception) {
                    $.unblockUI();
                    $("#fomo-modal-update-profile .modal-content .spinner-loading").hide();
                    if (jqXHR.status === 422) {
                        errors = "";
                        data = jqXHR.responseJSON;
                        for (var k in data) {
                            if (data.hasOwnProperty(k)) {
                                data[k].forEach(function (val) {
                                    errors += val;
                                });
                            }
                        }
                        toastr.error(errors);
                    }
                }
            });
        });

        $(document).on('click', '#fomo-modal-update-profile .btn-remove-image', function (e) {
            $(this).closest('.image-box').find('.image-data').val('');

            var $previewImg = $(this).closest('.image-box').find('img.preview_image'),
                defaultSrc = $previewImg.attr('data-default-src');
            if (typeof defaultSrc !== 'undefined' && defaultSrc !== '') {
                $previewImg.attr('src', defaultSrc).show();
            } else {
                $(this).closest('.image-box').find('img').hide();
            }

            $(this).closest('.image-box').find(".btn-upload-image").show();
            $(this).closest('.image-box').find(".btn-remove-image").hide();
        });
    }

    return {
        //main function to initiate the module
        init: function init() {
            handle();
        }
    };
}();

jQuery(document).ready(function () {
    FomoUserUpdateProfile.init();
});

/***/ }),
/* 19 */,
/* 20 */,
/* 21 */,
/* 22 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 23 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 24 */,
/* 25 */,
/* 26 */,
/* 27 */,
/* 28 */,
/* 29 */,
/* 30 */,
/* 31 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(15);
__webpack_require__(13);
__webpack_require__(9);
__webpack_require__(11);
__webpack_require__(12);
__webpack_require__(14);
__webpack_require__(17);
__webpack_require__(16);
__webpack_require__(18);
__webpack_require__(10);
__webpack_require__(8);
__webpack_require__(7);
__webpack_require__(22);
module.exports = __webpack_require__(23);


/***/ })
/******/ ]);