<?php

namespace Tests\Traits;

use App;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\User;

trait UserTestTrait
{

    /**
     * Create an instance user repository interface
     *
     * @author vulh
     * @return App\Repositories\Contracts\CompanyRepositoryInterface
     */
    public function instanceUserRepo()
    {
        return App::make(UserRepositoryInterface::class);
    }

    /**
     * Create a user
     *
     * @author vulh
     * @return Object model user
     */
    public function createUser()
    {
        return factory(User::class, 1)->create([
            'status'    => 'publish'
        ])->first();
    }

}