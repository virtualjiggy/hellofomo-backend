<?php

namespace App\Providers;

use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Eli\ApiLog\Providers\ApiLogServiceProvider;
use Elidev\Repository\Providers\RepositoryServiceProvider;
use Illuminate\Support\ServiceProvider;
use Laravel\Passport\PassportServiceProvider;
use Spatie\Permission\PermissionServiceProvider;
use Mpociot\ApiDoc\ApiDocGeneratorServiceProvider;
use Efriandika\LaravelSettings\SettingsServiceProvider;
use Spatie\Activitylog\ActivitylogServiceProvider;
use Barryvdh\Debugbar\ServiceProvider as DebuggerServiceProvider;
use Botble\Media\Providers\MediaServiceProvider;
use Intervention\Image\ImageServiceProvider;
use Laracasts\Utilities\JavaScript\JavaScriptServiceProvider;
use Elidev\ACL\Providers\ACLServiceProvider;
use LaravelFCM\FCMServiceProvider;

class PackageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment('local')) {
            $this->app->register(DebuggerServiceProvider::class);
            $this->app->register(IdeHelperServiceProvider::class);
        }

        $this->app->register(RepositoryServiceProvider::class);
        $this->app->register(PermissionServiceProvider::class);
        $this->app->register(PassportServiceProvider::class);
        $this->app->register(SettingsServiceProvider::class);
        $this->app->register(ActivitylogServiceProvider::class);

        $this->app->register(ApiDocGeneratorServiceProvider::class);

        // Files management
        $this->app->register(ImageServiceProvider::class);
        $this->app->register(MediaServiceProvider::class);

        // Laracast Utilities
        $this->app->register(JavaScriptServiceProvider::class);

        // ACL
        $this->app->register(ACLServiceProvider::class);

        $this->app->register(FCMServiceProvider::class);

        // API logging service
        $this->app->register(ApiLogServiceProvider::class);
    }
}
