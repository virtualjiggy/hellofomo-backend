<?php

namespace App\Providers;

use App;
use App\Http\Requests\CustomValidations\ValidationCountryCode;
use App\Http\Requests\CustomValidations\ValidationDimension;
use App\Http\Requests\CustomValidations\ValidationPerKeyword;
use App\Http\Requests\CustomValidations\ValidationUrl;
use App\Http\Requests\CustomValidations\ValidationVideo;
use Illuminate\Support\ServiceProvider;
use Schema;
use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Set default string length for the generation to avoild the Segmentation fault: 11 error
        Schema::defaultStringLength(100);

        // Custom validation
        Validator::extend('max_length_per_word', function ($attribute, $value, $parameters, $validator) {
            $isExistedCompSize = App::make(ValidationPerKeyword::class);
            return $isExistedCompSize->validate($value);
        });

        // Custom validation
        Validator::extend('valid_video', function ($attribute, $value, $parameters, $validator) {
            $validator = App::make(ValidationVideo::class);
            return $validator->validate($value);
        });

        // Custom validation
        Validator::extend('valid_country_code', function ($attribute, $value, $parameters, $validator) {
            $validator = App::make(ValidationCountryCode::class);
            return $validator->validate($value);
        });

        Validator::extend('valid_base64_image',function($attribute, $value, $params, $validator) {
            $image = base64_decode($value);
            $f = finfo_open();
            $result = finfo_buffer($f, $image, FILEINFO_MIME_TYPE);
            return $result == 'image/png';
        });

        Validator::extend('image_dimensions',function($attribute, $value, $params, $validator) {
            $validator = App::make(ValidationDimension::class);
            return $validator->validate($value);
        });

        // Custom url validation (ticket #12811)
        Validator::extend('custom_url', function ($attribute, $value, $parameters, $validator) {
            $validator = App::make(ValidationUrl::class);
            return $validator->validate($value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->register(PackageServiceProvider::class);
        $this->app->register(ResponseMacroServiceProvider::class);
        $this->app->register(RepositoryServiceProvider::class);
        $this->app->registerDeferredProvider(DeferredRepositoryServiceProvider::class);
        $this->app->register(ComposerServiceProvider::class);
    }
}
