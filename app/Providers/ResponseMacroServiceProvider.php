<?php

namespace App\Providers;

use ApiLog;
use Illuminate\Support\ServiceProvider;
use Response;

class ResponseMacroServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Use the same response format for any APIs
        // How to use ? In API controller response()->error($message) or response->success($data)

        Response::macro('success', function ($data) {

            // log api/ajax calls
            ApiLog::log(request()->input(), [
                'errors' => false,
                'data' => $data,
            ]);

            return Response::json([
                'errors' => false,
                'data' => $data,
            ]);
        });

        Response::macro('error', function ($message, $code = '', $status = 400) {
            // log api/ajax calls
            ApiLog::log(request()->input(), [
                'errors'  => true,
                'code'    => $code,
                'message' => $message,
            ]);

            return Response::json([
                'errors'  => true,
                'code'    => $code,
                'message' => $message,
            ], $status);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
