<?php

namespace App\Providers;

use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\DeviceRepositoryInterface;
use App\Repositories\Contracts\GeneralTokenRepositoryInterface;
use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Repositories\Contracts\NewsTagsRepositoryInterface;
use App\Repositories\Contracts\PermissionRepositoryInterface;
use App\Repositories\Contracts\RoleRepositoryInterface;
use App\Repositories\Contracts\TagsRepositoryInterface;
use App\Repositories\Contracts\UserActivationRepositoryInterface;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Repositories\Eloquents\AuthorRepository;
use App\Repositories\Eloquents\CategoryRepository;
use App\Repositories\Eloquents\DeviceRepository;
use App\Repositories\Eloquents\GeneralTokenRepository;
use App\Repositories\Eloquents\NewsRepository;
use App\Repositories\Eloquents\NewsTagsRepository;
use App\Repositories\Eloquents\PermissionRepository;
use App\Repositories\Eloquents\RoleRepository;
use App\Repositories\Eloquents\TagsRepository;
use App\Repositories\Eloquents\UserActivationRepository;
use App\Repositories\Eloquents\UserRepository;
use Illuminate\Support\ServiceProvider;
use App\Repositories\Contracts\NewsAuthorsRepositoryInterface;
use App\Repositories\Eloquents\NewsAuthorsRepository;

class DeferredRepositoryServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = true;

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(UserActivationRepositoryInterface::class, UserActivationRepository::class);
        $this->app->bind(GeneralTokenRepositoryInterface::class, GeneralTokenRepository::class);
        $this->app->bind(AuthorRepositoryInterface::class, AuthorRepository::class);
        $this->app->bind(CategoryRepositoryInterface::class, CategoryRepository::class);

        /**
         * System user
         */
        $this->app->bind(RoleRepositoryInterface::class, RoleRepository::class);
        $this->app->bind(PermissionRepositoryInterface::class, PermissionRepository::class);

        $this->app->bind(TagsRepositoryInterface::class, TagsRepository::class);
        $this->app->bind(NewsRepositoryInterface::class, NewsRepository::class);
        $this->app->bind(NewsTagsRepositoryInterface::class, NewsTagsRepository::class);
        $this->app->bind(DeviceRepositoryInterface::class, DeviceRepository::class);

        $this->app->bind(NewsAuthorsRepositoryInterface::class, NewsAuthorsRepository::class);
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            UserRepositoryInterface::class,
            UserActivationRepositoryInterface::class,
            GeneralTokenRepositoryInterface::class,
            AuthorRepositoryInterface::class,
            CategoryRepositoryInterface::class,

            /**
             * System user
             */
            RoleRepositoryInterface::class,
            PermissionRepositoryInterface::class,

            /**
             * News modules
             */
            NewsRepositoryInterface::class,
            TagsRepositoryInterface::class,
            NewsTagsRepositoryInterface::class,

            DeviceRepositoryInterface::class,

            NewsAuthorsRepository::class,
        ];
    }

}
