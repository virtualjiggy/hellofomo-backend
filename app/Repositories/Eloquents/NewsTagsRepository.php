<?php

namespace App\Repositories\Eloquents;

use Elidev\Repository\Eloquent\BaseRepository;
use App\Repositories\Contracts\NewsTagsRepositoryInterface;
use App\Models\NewsTags;

/**
 * Class NewsTagsRepository
 * @package namespace App\Repositories\Eloquents;
 */
class NewsTagsRepository extends BaseRepository implements NewsTagsRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return NewsTags::class;
    }
}
