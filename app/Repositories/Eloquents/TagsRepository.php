<?php

namespace App\Repositories\Eloquents;

use App\Models\Tags;
use App\Repositories\Contracts\TagsRepositoryInterface;
use App\Repositories\Criterias\FilterByName;
use App\Repositories\Criterias\Tag\FilterByType;
use Elidev\Repository\Eloquent\BaseRepository;

/**
 * Class TagsRepository
 * @package namespace App\Repositories\Eloquents;
 */
class TagsRepository extends BaseRepository implements TagsRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Tags::class;
    }

    /**
     * List tags (also filter: name, type)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC')
    {
        $this->resetCriteria();

        return $this->pushCriteria(new FilterByName(array_get($terms, 'name', '')))
                    ->pushCriteria(new FilterByType(array_get($terms, 'type', '')))
                    ->orderBy($sortColumn, $sortOrder)
                    ->paginate($limit, $columns);
    }

    /**
     * Get news tags
     *
     * @param array $columns
     *
     * @return array
     */
    public function getNewsTags($columns = [])
    {
        if ( count($columns) === 0 ) {
            $columns = ['id', 'name'];
        }

        return $this->model
            ->where('type', '=', config('fomo.category.types.news'))
            ->select($columns)
            ->get();
    }
}
