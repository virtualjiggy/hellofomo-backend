<?php

namespace App\Repositories\Eloquents;

use App\Models\Category;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Criterias\Category\FilterByCategoryType;
use App\Repositories\Criterias\FilterByName;
use Elidev\Repository\Eloquent\BaseRepository;

/**
 * Class CategoryRepository
 * @package namespace App\Repositories\Eloquents;
 */
class CategoryRepository extends BaseRepository implements CategoryRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Category::class;
    }

    /**
     * List categories (also filter: name, type)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC')
    {
        $this->resetCriteria();

        return $this->pushCriteria(new FilterByName(array_get($terms, 'name', '')))
                    ->pushCriteria(new FilterByCategoryType(array_get($terms, 'type', '')))
                    ->orderBy($sortColumn, $sortOrder)
                    ->paginate($limit, $columns);
    }

    /**
     * Get category types for filter in datatable listing
     *
     * @return array
     */
    public function getCategoryTypesForFilter()
    {
        $categoryTypes = array_unique(Category::all(['id', 'name', 'type'])->pluck('type')->all());
        foreach ($categoryTypes as $key => $categoryType) {
            $categoryTypes[$categoryType] = $categoryType;
            unset($categoryTypes[$key]);
        }

        return $categoryTypes;
    }

    /**
     * Get salutations and titles
     *
     * @param array $columns
     *
     * @return array
     */
    public function getSalutationsAndTitles($columns = [])
    {
        if ( count($columns) === 0 ) {
            $columns = ['id', 'name', 'type'];
        }

        $result = [ 'salutations' => [], 'titles' => [] ];
        $data = Category::whereIn('type', [
            config('fomo.category.types.salutation'),
            config('fomo.category.types.title')
        ])->select($columns)->get();

        if ( !$data ) {
            return $result;
        }

        $result['salutations'] = $data->where('type', '=', config('fomo.category.types.salutation'))->pluck('name', 'id')->all();
        $result['titles'] = $data->where('type', '=', config('fomo.category.types.title'))->pluck('name', 'id')->all();

        return $result;
    }

    /**
     * Get news's category detail
     *
     * @param int $id
     *
     * @return mixed
     */
    public function getNewsCategoryDetail($id = -1)
    {
        if ( $id == -1 ) {
            return false;
        }

        return $this->model
            ->where('id', '=', $id)
            ->where('type', '=', config('fomo.category.types.news'))
            ->first();
    }

    /**
     * Get news categories
     *
     * @param array $columns
     *
     * @return array
     */
    public function getNewsCategories($columns = [])
    {
        if ( count($columns) === 0 ) {
            $columns = ['id', 'name'];
        }

        return $this->model
            ->where('type', '=', config('fomo.category.types.news'))
            ->select($columns)
            ->get();
    }
}
