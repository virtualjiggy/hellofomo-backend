<?php

namespace App\Repositories\Eloquents;

use App\Models\Author;
use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Repositories\Criterias\Author\FilterByNameCriteria;
use App\Repositories\Criterias\Author\FilterBySalutation;
use App\Repositories\Criterias\Author\FilterByTitle;
use App\Repositories\Criterias\FilterTrashCriteria;
use App\Repositories\Criterias\WithRelationsCriteria;
use Elidev\Repository\Eloquent\BaseRepository;

/**
 * Class AuthorRepository
 * @package namespace App\Repositories\Eloquents;
 */
class AuthorRepository extends BaseRepository implements AuthorRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Author::class;
    }

    /**
     * List authors (also filter: first_name, last_name)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     * @param bool   $onlyTrashed
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC', $onlyTrashed = false)
    {
        $this->resetCriteria();

        return $this->pushCriteria(new WithRelationsCriteria(['salutation', 'title']))
                    ->pushCriteria(new FilterTrashCriteria($onlyTrashed))
                    ->pushCriteria(new FilterBySalutation(array_get($terms, 'salutation_name', -1)))
                    ->pushCriteria(new FilterByTitle(array_get($terms, 'title_name', -1)))
                    ->pushCriteria(new FilterByNameCriteria('first_name', array_get($terms, 'first_name', '')))
                    ->pushCriteria(new FilterByNameCriteria('last_name', array_get($terms, 'last_name', '')))
                    ->orderBy($sortColumn, $sortOrder)
                    ->paginate($limit, $columns);
    }

    /**
     * Search authors based on keyword
     *
     * @param array  $columns
     * @param string $keyword
     *
     * @return mixed
     */
    public function search($columns = ['*'], $keyword = '')
    {
        $keyword = '%' . $keyword . '%';

        return $this->model
            ->with(['salutation', 'title'])
            ->where('first_name', 'LIKE', $keyword)
            ->orWhere('last_name', 'LIKE', $keyword)
            ->orWhere('display_name', 'LIKE', $keyword)
            ->paginate(config('elidev.default_limit', 10), $columns);
    }
}
