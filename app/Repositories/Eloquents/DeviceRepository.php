<?php

namespace App\Repositories\Eloquents;

use Elidev\Repository\Eloquent\BaseRepository;
use App\Repositories\Contracts\DeviceRepositoryInterface;
use App\Models\Device;

/**
 * Class DeviceRepository
 * @package namespace App\Repositories\Eloquents;
 */
class DeviceRepository extends BaseRepository implements DeviceRepositoryInterface
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Device::class;
    }
}
