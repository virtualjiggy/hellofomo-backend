<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserActivationRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface UserActivationRepositoryInterface extends RepositoryInterface
{
    /**
     * Create user activation
     *
     * @param App/User $user
     * @return string $token
     */
    public function createActivation($user);

    /**
     * Delete by token
     * @param string token
     * @return boolean
     */
    public function deleteByToken($token);
}
