<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface CategoryRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface CategoryRepositoryInterface extends RepositoryInterface
{
    /**
     * List categories (also filter: name, type)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC');

    /**
     * Get category types for filter in datatable listing
     *
     * @return array
     */
    public function getCategoryTypesForFilter();

    /**
     * Get salutations and titles
     *
     * @param array $columns
     *
     * @return array
     */
    public function getSalutationsAndTitles($columns = []);

    /**
     * Get news categories
     *
     * @param array $columns
     *
     * @return array
     */
    public function getNewsCategories($columns = []);

    /**
     * Get news's category detail
     *
     * @param int $id
     *
     * @return mixed
     */
    public function getNewsCategoryDetail($id = -1);
}
