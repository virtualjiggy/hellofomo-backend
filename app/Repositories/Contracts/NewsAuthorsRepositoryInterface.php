<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsAuthorsRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface NewsAuthorsRepositoryInterface extends RepositoryInterface
{
    //
}
