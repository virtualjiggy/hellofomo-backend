<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface NewsCategorysRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface NewsCategorysRepositoryInterface extends RepositoryInterface
{
    //
}
