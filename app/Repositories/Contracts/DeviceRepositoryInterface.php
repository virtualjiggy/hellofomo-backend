<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface DeviceRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface DeviceRepositoryInterface extends RepositoryInterface
{
    //
}
