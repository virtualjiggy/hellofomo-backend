<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface AuthorRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface AuthorRepositoryInterface extends RepositoryInterface
{
    /**
     * List authors (also filter: first_name, last_name)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     * @param bool   $onlyTrashed
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC', $onlyTrashed = false);

    /**
     * Search authors based on keyword
     *
     * @param array  $columns
     * @param string $keyword
     *
     * @return mixed
     */
    public function search($columns = ['*'], $keyword = '');
}
