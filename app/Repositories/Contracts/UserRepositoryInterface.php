<?php

namespace App\Repositories\Contracts;

use Elidev\Repository\Contracts\RepositoryInterface;

/**
 * Interface UserRepositoryInterface
 * @package namespace App\Repositories\Contracts;
 */
interface UserRepositoryInterface extends RepositoryInterface
{
    /**
     * List users (also filter: name, email or phone)
     *
     * @param array  $columns
     * @param array  $terms
     * @param int    $limit
     * @param string $sortColumn
     * @param string $sortOrder
     * @param bool   $onlyTrashed
     *
     * @return mixed
     */
    public function listing($columns = ['*'], $terms = [], $limit = 10, $sortColumn = 'id', $sortOrder = 'ASC', $onlyTrashed = false);

    /**
     * Get by email
     * @param $email
     * @return Collection
     */
    public function getByEmail($email);

    /**
     * Get all publish users
     *
     * @return Collection
     */
    public function getPublishUsers();

    /**
     * @param $email
     * @param $userID
     * @return mixed
     */
    public function checkEmailExist($email, $userID);

}
