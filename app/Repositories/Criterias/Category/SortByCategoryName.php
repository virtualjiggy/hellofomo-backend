<?php

namespace App\Repositories\Criterias\Category;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class SortByCategoryName implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $sort;

    /**
     * FilterByType constructor.
     *
     * @param string $sort
     */
    public function __construct($sort = 'ASC')
    {
        $this->sort = $sort;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->sort) ) {
            $this->sort = 'ASC';
        }

        return $model->orderBy('name', $this->sort);
    }
}
