<?php

namespace App\Repositories\Criterias\Category;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByCategoryType implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $types;

    /**
     * FilterByCategoryType constructor.
     *
     * @param array $types
     */
    public function __construct($types = [])
    {
        if ( $types == '' ) {
            $types = [];
        }

        if ( !is_array($types) ) {
            $types = [ $types ];
        }

        $this->types = $types;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( count($this->types) === 0 ) {
            return $model;
        }

        return $model->whereIn('type', $this->types);
    }
}
