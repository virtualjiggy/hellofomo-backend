<?php

namespace App\Repositories\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByTitle implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $title;

    /**
     * FilterByTitle constructor.
     *
     * @param string $title
     */
    public function __construct($title = '')
    {
        $this->title = $title;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->title) ) {
            return $model;
        }

        return $model->where('title', 'LIKE', '%' . $this->title . '%');
    }
}
