<?php

namespace App\Repositories\Criterias\News;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByTagIds implements CriteriaInterface
{
    /**
     * @var array
     */
    protected $tagIds;

    /**
     * FilterByTagIds constructor.
     *
     * @param array $tagIds
     */
    public function __construct($tagIds = [])
    {
        $this->tagIds = $tagIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->tagIds) || !is_array($this->tagIds) || count($this->tagIds) < 1) {
            return $model;
        }

        return $model
            ->join('news_tags', 'news_tags.news_id', '=', 'news.id')
            ->whereIn('news_tags.tag_id', $this->tagIds)
            ->distinct("news.id");
    }
}