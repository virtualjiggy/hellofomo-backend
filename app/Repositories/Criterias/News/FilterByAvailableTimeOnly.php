<?php

namespace App\Repositories\Criterias\News;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByAvailableTimeOnly implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $availableTimeOnly;

    /**
     * FilterByAvailableTimeOnly constructor.
     *
     * @param string $availableTimeOnly
     */
    public function __construct($availableTimeOnly = '')
    {
        $this->availableTimeOnly = $availableTimeOnly;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->availableTimeOnly) ) {
            return $model;
        }

        return $model->whereRaw("(DATE_FORMAT(available_date, '%H:%i') LIKE '%$this->availableTimeOnly%')");
    }
}
