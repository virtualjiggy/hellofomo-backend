<?php

namespace App\Repositories\Criterias\News;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByAvailableDate implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $availableDate;

    /**
     * FilterByAvailableDate constructor.
     *
     * @param string $availableDate
     */
    public function __construct($availableDate = '')
    {
        $this->availableDate = $availableDate;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->availableDate) ) {
            return $model;
        }

        return $model->whereDate('available_date', '=', date_format(date_create($this->availableDate), 'Y-m-d'));
    }
}
