<?php

namespace App\Repositories\Criterias\News;

use App\Utils\Utils;
use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByAvailableDateOnly implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $availableDateOnly;

    /**
     * FilterByAvailableDateOnly constructor.
     *
     * @param string $availableDateOnly
     */
    public function __construct($availableDateOnly = '')
    {
        $this->availableDateOnly = $availableDateOnly;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->availableDateOnly) ) {
            return $model;
        }

        // only accept format: Y-m-d, not: Y.m.d or Y/m/d
        $this->availableDateOnly = str_replace(['.', '/'], '-', $this->availableDateOnly);

        $format = Utils::dateTimeFormatBasedOnLanguage()['date_format'];
        $format = str_replace(['.', '/'], '-', $format);
        $format = str_replace('i:H', 'H:i', $format);
        $format = str_replace('d', '%d', $format);
        $format = str_replace('m', '%m', $format);
        $format = str_replace('Y', '%Y', $format);

        return $model->whereRaw("DATE_FORMAT(available_date, '$format') LIKE '%$this->availableDateOnly%'");
    }
}
