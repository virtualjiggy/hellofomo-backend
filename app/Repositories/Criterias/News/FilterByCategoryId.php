<?php

namespace App\Repositories\Criterias\News;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByCategoryId implements CriteriaInterface
{
    /**
     * @var int
     */
    protected $categoryId;

    /**
     * FilterByCategoryId constructor.
     *
     * @param int $categoryId
     */
    public function __construct($categoryId = -1)
    {
        $this->categoryId = $categoryId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->categoryId) || $this->categoryId == -1 ) {
            return $model;
        }

        return $model
            ->join('news_categories', 'news_categories.news_id', '=', 'news.id')
            ->where('news_categories.category_id', '=', $this->categoryId);
    }
}
