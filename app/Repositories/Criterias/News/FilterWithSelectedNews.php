<?php

namespace App\Repositories\Criterias\News;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterWithSelectedNews implements CriteriaInterface
{

    /**
     * @var array
     */
    protected $selectedNews;

    /**
     * FilterWithSelectedNews constructor.
     *
     * @param array $selectedNews
     */
    public function __construct($selectedNews = [])
    {
        $this->selectedNews = $selectedNews;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( count($this->selectedNews) == 0 || !is_array($this->selectedNews) ) {
            return $model;
        }

        return $model->whereIn('id', $this->selectedNews);
    }
}
