<?php

namespace App\Repositories\Criterias\News;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByCategoryIds implements CriteriaInterface
{
    /**
     * @var array
     */
    protected $categoryIds;

    /**
     * FilterByCategoryIds constructor.
     *
     * @param array $categoryIds
     */
    public function __construct($categoryIds = [])
    {
        $this->categoryIds = $categoryIds;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->categoryIds) || !is_array($this->categoryIds) || count($this->categoryIds) < 1) {
            return $model;
        }

        return $model
            ->join('news_categories', 'news_categories.news_id', '=', 'news.id')
            ->whereIn('news_categories.category_id', $this->categoryIds)
            ->distinct("news.id");
    }
}
