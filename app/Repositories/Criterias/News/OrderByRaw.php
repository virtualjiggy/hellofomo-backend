<?php

namespace App\Repositories\Criterias\News;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class OrderByRaw implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $sortColumn;
    protected $sortOrder;

    /**
     * FilterByAvailableTimeOnly constructor.
     *
     * @param string $availableTimeOnly
     */
    public function __construct($sortColumn = '', $sortOrder = 'ASC')
    {
        $this->sortColumn = $sortColumn;
        $this->sortOrder = $sortOrder;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->sortColumn) ) {
            return $model;
        }

        if($this->sortColumn == 'available_time_only') {
            return $model->orderByRaw("HOUR(available_date) $this->sortOrder, MINUTE(available_date) $this->sortOrder");
        } else {
            return $model->orderBy($this->sortColumn, $this->sortOrder);
        }
    }
}
