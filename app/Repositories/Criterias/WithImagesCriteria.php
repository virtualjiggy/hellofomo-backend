<?php

namespace App\Repositories\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class WithImagesCriteria implements CriteriaInterface
{
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        return $model->with('images');
    }
}
