<?php

namespace App\Repositories\Criterias\User;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByEmailCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $keyword;

    /**
     * FilterByEmailCriteria constructor.
     *
     * @param string $keyword
     */
    public function __construct($keyword = '')
    {
        $this->keyword = $keyword;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( !empty($this->keyword) ) {
            $model = $model->where('email', 'LIKE', '%' . $this->keyword . '%');
        }

        return $model;
    }
}
