<?php

namespace App\Repositories\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class WithRelationsCriteria implements CriteriaInterface
{
    /**
     * @var array
     */
    protected $relationNames;

    public function __construct($relationNames = [])
    {
        $this->relationNames = $relationNames;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( count($this->relationNames) > 0 ) {
            foreach ($this->relationNames as $relationName) {
                $model->with($relationName);
            }
        }

        return $model;
    }
}
