<?php

namespace App\Repositories\Criterias\Author;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByTitle implements CriteriaInterface
{
    /**
     * @var int
     */
    protected $titleId;

    /**
     * FilterByTitle constructor.
     *
     * @param int $titleId
     */
    public function __construct($titleId = -1)
    {
        $this->titleId = empty($titleId) ? -1 : $titleId;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( $this->titleId == -1 ) {
            return $model;
        }

        return $model->where('title_id', '=', $this->titleId);
    }
}
