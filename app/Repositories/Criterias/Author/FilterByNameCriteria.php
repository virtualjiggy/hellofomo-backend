<?php

namespace App\Repositories\Criterias\Author;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByNameCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $keyword;

    /**
     * @var string
     */
    protected $column;

    /**
     * FilterByNameCriteria constructor.
     *
     * @param string $column
     * @param string $keyword
     */
    public function __construct($column = '', $keyword = '')
    {
        $this->column = $column;
        $this->keyword = $keyword;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->column) || !in_array($this->column, ['first_name', 'last_name']) || empty($this->keyword) ) {
            return $model;
        }

        return $model->where($this->column, 'LIKE', '%' . $this->keyword . '%');
    }
}
