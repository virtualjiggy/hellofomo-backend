<?php

namespace App\Repositories\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterStatusCriteria implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $status;

    /**
     * @var string
     */
    protected $alias;

    /**
     * FilterStatusCriteria constructor.
     *
     * @param string $status (pending | publish)
     * @param string $alias
     *
     */
    public function __construct($status, $alias = '')
    {
        $this->status = $status;
        $this->alias = $alias;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $statusField = $this->alias ? $this->alias.'.status' : 'status';
        if ( !empty($this->status) ) {
            $model = $model->where([$statusField => $this->status]);
        }

        return $model;
    }
}
