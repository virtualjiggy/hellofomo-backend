<?php

namespace App\Repositories\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByName implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $name;

    /**
     * FilterByCategoryName constructor.
     *
     * @param string $name
     */
    public function __construct($name = '')
    {
        $this->name = $name;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->name) ) {
            return $model;
        }

        return $model->where('name', 'LIKE', '%' . $this->name . '%');
    }
}
