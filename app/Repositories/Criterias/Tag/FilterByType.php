<?php

namespace App\Repositories\Criterias\Tag;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterByType implements CriteriaInterface
{
    /**
     * @var string
     */
    protected $type;

    /**
     * FilterByType constructor.
     *
     * @param string $type
     */
    public function __construct($type = '')
    {
        $this->type = $type;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( empty($this->type) ) {
            return $model;
        }

        return $model->where('type', 'LIKE', '%' . $this->type . '%');
    }
}
