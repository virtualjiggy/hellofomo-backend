<?php

namespace App\Repositories\Criterias;

use Elidev\Repository\Contracts\CriteriaInterface;
use Elidev\Repository\Contracts\RepositoryInterface;

class FilterWithTrashedCriteria implements CriteriaInterface
{
    /**
     * @var bool
     */
    protected $withTrash;

    /**
     * FilterWithTrashedCriteria constructor.
     *
     * @param integer $withTrash
     *
     * Note:
     *  - trash = 1 : get all items (also contains trashed items)
     *  - trash = 0 : get all active items which have not trashed
     */
    public function __construct($withTrash = 0)
    {
        $this->withTrash = $withTrash;
    }

    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if ( $this->withTrash ) {
            return $model->withTrashed();
        }

        return $model;
    }
}
