<?php namespace App\Factories\User;

use App\Repositories\Contracts\UserActivationRepositoryInterface;
use App\Notifications\UserActivation;
use App\Repositories\Contracts\UserRepositoryInterface;
use Carbon\Carbon;

class ActivationFactory
{

    /**
     * Resend after 24 hours
     * @var $resendAfter
     */
    protected $resendAfter = 24;

    /**
     * Resend after 4 seconds
     * @var $sendNotificationAfter
     */
    protected $sendNotificationAfter = 4;

    /**
     * @var $userActivationRepo
    */
    protected $userActivationRepo;

    /**
     * @var $userRepo
     */
    protected $userRepo;

    /**
     * @param UserActivationRepositoryInterface $userActivationRepo
     * @param UserRepositoryInterface $userRepo
     */
    public function __construct(UserActivationRepositoryInterface $userActivationRepo, UserRepositoryInterface $userRepo)
    {
        $this->userActivationRepo = $userActivationRepo;
        $this->userRepo = $userRepo;
    }

    /**
     * Send activation mail
     * @param $user
     *
     * @return boolean
     */
    public function sendActivationMail($user)
    {
        if ($user->status == config('elidev.list_default_status.publish') || !$this->shouldSend($user)) {
            return false;
        }
        $token = $this->userActivationRepo->createActivation($user);

        // Not delay to send email
        $user->notify((new UserActivation($token)));

        return true;
    }

    /**
     * Whether send activation link or not
     * @param $user
     *
     * @return boolean
     */
    private function shouldSend($user)
    {
        $activation = $this->userActivationRepo->find($user->id);
        return $activation === null || strtotime($activation->created_at) + 60 * 60 * $this->resendAfter < time();
    }

    /**
     * Activate user
     * @param $token
     *
     * @return App/User $user
     */
    public function activateUser($token)
    {
        $activation = $this->userActivationRepo->findWhere(['token' => $token]);

        if ($activation->first() === null) {
            return null;
        }

        $user = $this->userRepo->find($activation->first()->id);
        $user->status = config('elidev.list_default_status.publish');
        $user->save();

        $this->userActivationRepo->deleteByToken($token);

        return $user;
    }
}
