<?php

namespace App\Models;

use Botble\Media\Models\MediaFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Author extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'display_name',
        'description', 'salutation_id', 'title_id', 'image_id',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'salutation_name', 'title_name'
    ];

    /**
     * The attributes that should be visible in arrays.
     *
     * @var array
     */
    protected $visible = [
        'id', 'first_name', 'last_name', 'display_name',
        'salutation_id', 'salutation_name',
        'title_id', 'title_name', 'checkbox'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'deleted_at' ];

    /**
     * Author's image
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this
            ->hasOne(MediaFile::class, 'id', 'image_id')
            ->select(['id', 'user_id', 'name', 'folder_id', 'mime_type', 'url', 'is_public', 'deleted_at']);
    }

    /**
     * Author's salutation
     *
     * @return mixed
     */
    public function salutation()
    {
        return $this
            ->hasOne(Category::class, 'id', 'salutation_id')
            ->where('type', config('fomo.category.types.salutation'))
            ->select(['id', 'name', 'type']);
    }

    /**
     * Author's title
     *
     * @return mixed
     */
    public function title()
    {
        return $this
            ->hasOne(Category::class, 'id', 'title_id')
            ->where('type', config('fomo.category.types.title'))
            ->select(['id', 'name', 'type']);
    }

    /**
     * The news that belong to the author
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function news()
    {
        return $this->belongsToMany(News::class, 'news_authors')->withTimestamps();
    }

    /**
     * Get author's image
     */
    public function getImageSrcAttribute()
    {
        return $this->image ? url('/') . '/' . $this->image->url : '';
    }

    /**
     * Get salutation name
     *
     * @return string
     */
    public function getSalutationNameAttribute()
    {
        return $this->salutation ? $this->salutation->name : '';
    }

    /**
     * Get title name
     *
     * @return string
     */
    public function getTitleNameAttribute()
    {
        return $this->title ? $this->title->name : '';
    }

    /**
     * Get display name
     *
     * @param string $value
     *
     * @return string
     * @ticket #13385 - HFBACKEND-91 News / Edit - Show Authors Display name in Selectbox
     */
    public function getDisplayNameAttribute($value)
    {
        // handle for old data which display name has value is null
        $name = empty($value) ? sprintf('%s %s', $this->first_name, $this->last_name) : $value;

        return $name;
    }
}
