<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivation extends Model
{
    protected $fillable = ['id', 'token', 'created_at'];
    protected $table = 'user_activations';
}
