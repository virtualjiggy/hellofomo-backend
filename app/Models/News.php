<?php

namespace App\Models;

use App\Utils\Utils;
use Botble\Media\Models\MediaFile;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title', 'introduction', 'description',
        'keywords', 'status', 'available_date',
        'tags', 'image_id', 'gallery_ids', 'attachment_ids',
        'meta_title', 'meta_permalink', 'meta_description'
    ];

    protected $appends = ['formatted_available_date'];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'deleted_at', 'available_date' ];

    /**
     * The authors that belong to the news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function authors()
    {
        return $this->belongsToMany(Author::class, 'news_authors')->withTimestamps();
    }

    /**
     * The categories that belong to the news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories()
    {
        return $this
            ->belongsToMany(Category::class, 'news_categories', 'news_id', 'category_id')
            ->where('categories.type', '=', config('fomo.category.types.news'));
    }

    /**
     * The tags that belong to the news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this
            ->belongsToMany(Tags::class, 'news_tags', 'news_id', 'tag_id')
            ->where('tags.type', '=', config('fomo.category.types.news'));
    }

    /**
     * The related news that belong to the news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function relatedNews()
    {
        return $this->belongsToMany(News::class, 'news_news', 'news_id', 'related_news_id')->withTimestamps();
    }

    /**
     * The related news that belong to the news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function relatedNewsOf()
    {
        return $this->belongsToMany(News::class, 'news_news', 'related_news_id', 'news_id')->withTimestamps();
    }

    /**
     * get all related news
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function getRelatedNewsFullAttribute()
    {
        return $this->relatedNews->merge($this->relatedNewsOf);
    }

    /**
     * News's cover picture relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this
            ->hasOne(MediaFile::class, 'id', 'image_id')
            ->select(['id', 'user_id', 'name', 'folder_id', 'mime_type', 'url', 'is_public', 'deleted_at', 'focus']);
    }

    /**
     * Get tag names with link
     *
     * @return string
     */
    public function getTagNamesWithLinkAttribute()
    {
        // build anchor link contains tag name
        $names = $this->tags->map(function($tag) {
            return view('admin.news.includes.tag_names_with_link', [
                'tagId'   => $tag->id,
                'tagName' => $tag->name,
            ])->render();
        })->toArray();

        return implode(', ', $names);
    }

    /**
     * Get category names
     *
     * @return string
     */
    public function getCategoryNamesAttribute()
    {
        return implode(', ', $this->categories->pluck('name')->all());
    }

    /**
     * Get category names with link
     *
     * @return string
     */
    public function getCategoryNamesWithLinkAttribute()
    {
        // build anchor link contains category name
        $names = $this->categories->map(function($category) {
            return view('admin.news.includes.category_names_with_link', [
                'categoryId'   => $category->id,
                'categoryName' => $category->name,
            ])->render();
        })->toArray();

        return implode(', ', $names);
    }

    /**
     * Get author names
     *
     * @return string
     */
    public function getAuthorNamesAttribute()
    {
        // format each name
        $names = $this->authors->map(function($author) {
            return sprintf('%s %s', $author->first_name, $author->last_name);
        })->toArray();

        return implode(', ', $names);
    }

    /**
     * Get author names with link
     *
     * @return string
     */
    public function getAuthorNamesWithLinkAttribute()
    {
        // build anchor link contains author name
        $names = $this->authors->map(function($author) {
            return view('admin.news.includes.author_names_with_link', [
                'author_id'   => $author->id,
                'author_name' => $author->display_name,
            ])->render();
        })->toArray();

        return implode(', ', $names);
    }

    /**
     * Get news's cover picture
     */
    public function getImageSrcAttribute()
    {
        return $this->image ? url('/') . '/' . $this->image->url : '';
    }

    /**
     * Get available date only and format based on current setting language
     *
     * @return string
     */
    public function getAvailableDateOnlyAttribute()
    {
        if (empty($this->available_date)) {
            return '';
        }

        return $this->available_date->format(Utils::dateTimeFormatBasedOnLanguage()['date_format']);
    }

    /**
     * Get available time only and format based on current setting language
     *
     * @return string
     */
    public function getAvailableTimeOnlyAttribute()
    {
        if (empty($this->available_date)) {
            return '';
        }

        return $this->available_date->format(Utils::dateTimeFormatBasedOnLanguage()['time_format']);
    }

    /**
     * Get available time and format based on current setting language
     *
     * @return string
     */
    public function getFormattedAvailableDateAttribute()
    {
        if (empty($this->available_date)) {
            return '';
        }

        $lang = \Request::get('lang');
        if ( !$lang ) {
            $lang = \App::getLocale();
        }
        if ( $this->available_date != null) {
            return $this->available_date->format(Utils::dateTimeFormatBasedOnLanguage($lang)['date_time_format']);
        }
        return $this->available_date;
    }
}
