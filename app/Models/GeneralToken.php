<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GeneralToken extends Model
{
    protected $fillable = ['token', 'email', 'type', 'expired_date'];

}
