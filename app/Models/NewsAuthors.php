<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsAuthors extends Model
{
    protected $fillable = ['news_id', 'author_id'];
}
