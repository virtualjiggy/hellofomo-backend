<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewsTags extends Model
{
    protected $fillable = ['news_id', 'tag_id'];

}
