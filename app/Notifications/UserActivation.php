<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class UserActivation extends Notification
{
    use Queueable;

    /**
     * @var $token
     */
    protected $token;

    /**
     * Create a new notification instance.
     *
     * @param $token
     * @return mixed
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $link = route('user::activateUser', $this->token);
        return (new MailMessage)
                    ->subject('Activation account')
                    ->greeting(sprintf('Hello %s!', $notifiable->name))
                    ->line('Please click the Activate account button to verify your registration:')
                    ->action('Activate account', $link)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
