<?php

namespace App\Services\PushNotification\Abstracts;

use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

abstract class PushServiceAbstract
{
    /**
     * @var string
     */
    protected $title;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var int
     */
    protected $newsId;

    /**
     * @var string
     */
    protected $error;

    /**
     * @var \App\Repositories\Contracts\DeviceRepositoryInterface
     */
    protected $deviceRepo;

    /**
     * Set data before push
     *
     * @param \Illuminate\Http\Request $request
     */
    public function parse($request)
    {
        $this->title   = $request->get('push_title', '');
        $this->content = $request->get('push_content', '');
        $this->newsId  = $request->get('news_id', '');
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Push the notification to user devices
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public abstract function push($request);

    /**
     * Build notification payload
     *
     * @return \LaravelFCM\Message\PayloadNotification
     */
    public function buildNotification()
    {
        $notificationBuilder = new PayloadNotificationBuilder();

        $notificationBuilder
            ->setTitle($this->title)
            ->setBody($this->content)
            ->setSound('default');

        $notification = $notificationBuilder->build();

        return $notification;
    }

    /**
     * Build optional data for sending news
     *
     * @return \LaravelFCM\Message\PayloadData|null
     */
    public function buildData()
    {
        $data = null;

        if ( !empty($this->newsId) ) {
            $dataBuilder = new PayloadDataBuilder();

            $dataBuilder->addData([
                'data' => [
                    'type'    => 'new_post',
                    'post_id' => $this->newsId,
                ],
            ]);

            $data = $dataBuilder->build();
        }

        return $data;
    }
}
