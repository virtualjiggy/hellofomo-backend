<?php

namespace App\Services\PushNotification\Frontend;

use App\Repositories\Contracts\DeviceRepositoryInterface;
use App\Services\PushNotification\Abstracts\PushServiceAbstract;
use FCM;
use LaravelFCM\Message\OptionsBuilder;

class PushByDevicesService extends PushServiceAbstract
{
    /**
     * PushByDevicesService constructor.
     *
     * @param \App\Repositories\Contracts\DeviceRepositoryInterface $deviceRepo
     */
    public function __construct(DeviceRepositoryInterface $deviceRepo)
    {
        $this->deviceRepo = $deviceRepo;
    }

    /**
     * Push the notification to user devices
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function push($request)
    {
        $this->parse($request);

        if ( empty(trim($this->content)) ) {
            $this->error = __('Invalid content. Please check again');
            return false;
        }

        try {
            $optionBuilder = new OptionsBuilder();
            $optionBuilder->setTimeToLive(60*20);
            $option = $optionBuilder->build();

            $notification = $this->buildNotification();
            $data = $this->buildData();

            // TODO: You must change it to get your tokens
            $devices = $this->deviceRepo->all();
            $tokens = $devices->pluck('fcm_token')->toArray();

            $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

            $downstreamResponse->numberSuccess();
            $downstreamResponse->numberFailure();
            $downstreamResponse->numberModification();

            // return Array - you must remove all this tokens in your database
            $downstreamResponse->tokensToDelete();

            // return Array (key : oldToken, value : new token - you must change the token in your database )
            $downstreamResponse->tokensToModify();

            // return Array - you should try to resend the message to the tokens in the array
            $downstreamResponse->tokensToRetry();

            // return Array (key:token, value:error) - in production you should remove from your database the tokens present in this array
            $downstreamResponse->tokensWithError();

        } catch(\Exception $ex) {
            $this->error = $ex->getMessage();
            Log::error('Something happens when push notification. Error: ' . $this->error);
            return false;
        }

        return true;
    }
}
