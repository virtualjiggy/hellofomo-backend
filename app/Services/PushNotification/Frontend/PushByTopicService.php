<?php

namespace App\Services\PushNotification\Frontend;

use App\Services\PushNotification\Abstracts\PushServiceAbstract;
use FCM;
use LaravelFCM\Message\Topics;
use Log;

class PushByTopicService extends PushServiceAbstract
{
    /**
     * Push the notification to user devices
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function push($request)
    {
        $this->parse($request);

        if ( empty(trim($this->content)) ) {
            $this->error = __('Invalid content. Please check again');
            return false;
        }

        try {
            // build notification
            $notification = $this->buildNotification();
            $data = $this->buildData();

            // set topic name
            $topic = new Topics();
            $topic->topic(config('fomo.push_notification.default_topic'));

            // push notification to topic
            $topicResponse = FCM::sendToTopic($topic, null, $notification, $data);

            // push failed
            if ( !$topicResponse->isSuccess() ) {
                $this->error = $topicResponse->error();
                Log::error('Push notification failed. Error: ' . $this->error);
                return false;
            }
        } catch(\Exception $ex) {
            $this->error = $ex->getMessage();
            Log::error('Something happens when push notification. Error: ' . $this->error);
            return false;
        }

        return true;
    }
}
