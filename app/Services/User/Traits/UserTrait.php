<?php

namespace App\Services\User\Traits;

use App\Utils\Utils;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

trait UserTrait
{
    /**
     * Format an user (get avatar URL...)
     * @param $collection
     *
     * @return $collection
     */
    public function formatUsers($collection)
    {
        $countries = collect(json_decode(File::get(public_path() . '/countries.json'), true));
        $collection->each(function($user) use($countries) {
            // assign role names to the output
            $roles = $user->roles->map(function($role) {
                return $role->name;
            })->toArray();
            $user['roleName'] = isset($roles[0]) ? $roles[0] : '';
            unset($user->roles);

            $avatarCollect        = config('elidev.image_collections.user.avatar');
            $path                 = $user->getMediaUrl($avatarCollect);
            $user[$avatarCollect] = empty($path) ? '' : url('/') . '/' . $path;

            // convert country code -> country name
            $user['country_name'] = Utils::getCountryName($countries, $user->country);

            return $user;
        });

        return $collection;
    }

    /**
     * Format a deed (get image URL, get deed's company logo URL...)
     *
     * @param \App\User $user
     *
     * @return null|\App\User
     */
    public function formatUser($user)
    {
        if ( $user ) {

            // assign role names to the output
            $roles = $user->roles->map(function($role) {
                return $role->name;
            })->toArray();
            $user['roleName'] = isset($roles[0]) ? $roles[0] : '';
            unset($user->roles);

        }

        return $user;
    }
}
