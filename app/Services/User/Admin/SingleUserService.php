<?php

namespace App\Services\User\Admin;

use App\Services\User\Abstracts\SingleUserService as SingleAbstract;

class SingleUserService extends SingleAbstract
{
    /**
     * After get single user, use this function to get more data you want
     *
     * @return mixed
     */
    function afterSingled()
    {
        //
    }
}
