<?php

namespace App\Services\User\Admin;

use App\Services\User\Abstracts\StoreUserService as StoreUserServiceAbstract;

class StoreUserService extends StoreUserServiceAbstract
{
    /**
     * The hook method is executed after user is added to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function afterStored($request)
    {
        // Upload avatar
        if ( $request->get('avatar') ) {
            $this->user->storeMediaBase64($request->get('avatar'), config('elidev.image_collections.user.avatar'));
        }
    }
}
