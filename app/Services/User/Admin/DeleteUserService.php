<?php

namespace App\Services\User\Admin;

use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Abstracts\DeleteEntityServiceAbstract;

class DeleteUserService extends DeleteEntityServiceAbstract
{
    /**
     * DeleteUserService constructor.
     *
     * @param \App\Repositories\Contracts\UserRepositoryInterface $repo
     */
    public function __construct(UserRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Process after destroy
     *
     * @return mixed
     */
    public function afterDestroy()
    {
    }

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    function afterSoftDelete()
    {
    }

    /**
     * Process after restore
     *
     * @return mixed
     */
    function afterRestore()
    {
    }
}
