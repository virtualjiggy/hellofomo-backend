<?php

namespace App\Services\User\Admin;

use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Abstracts\ListingAbstract;
use App\Services\User\Traits\UserTrait;
use App\Utils\Utils;

class ListingUserService extends ListingAbstract
{
    use UserTrait;

    /**
     * @var \App\Repositories\Contracts\UserRepositoryInterface
     */
    protected $repo;

    /**
     * ListingUserService constructor.
     *
     * @param \App\Repositories\Contracts\UserRepositoryInterface $repo
     */
    public function __construct(UserRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Handle ajax URL
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function ajaxURL($request)
    {
    }

    /**
     * Returned columns
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id', 'name', 'email', 'phone', 'status',
            'created_at', 'updated_at', 'deleted_at'
        ];
    }

    /**
     * Get listing data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function execute($request)
    {
        // set filter params (name, email or phone)

        $pagination = Utils::getDataTablePagination($request, ['name_with_link', 'email', 'phone', 'status']);
        if ( array_has($pagination['terms'], 'name_with_link') ) {
            $pagination['terms']['name'] = $pagination['terms']['name_with_link'];
            unset($pagination['terms']['name_with_link']);
        }
        if ( in_array($pagination['sortColumn'], ['checkbox']) ) {
            $pagination['sortColumn'] = 'id';
        }
        if ( $pagination['sortColumn'] == 'name_with_link' ) {
            $pagination['sortColumn'] = 'name';
        }

        return $this->repo->listing(
            $this->fields(),
            $pagination['terms'],
            $pagination['limit'],
            $pagination['sortColumn'],
            $pagination['sortOrder'],
            $pagination['onlyTrashed']
        );
    }
}
