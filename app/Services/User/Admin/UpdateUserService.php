<?php

namespace App\Services\User\Admin;

use App\Services\User\Abstracts\UpdateUserService as UpdateUserServiceAbstract;

class UpdateUserService extends UpdateUserServiceAbstract
{
    /**
     * The hook method is executed after user is added to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function afterUpdate($request)
    {

    }
}
