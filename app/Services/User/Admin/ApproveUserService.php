<?php namespace App\Services\User\Admin;


use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Abstracts\ApproveEntityServiceAbstract;

class ApproveUserService extends ApproveEntityServiceAbstract
{
    /**
     * @param UserRepositoryInterface $repo
     */
    public function __construct(UserRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * A hook method after entity has been approved
     *
     * @return mixed
     */
    function afterApproved()
    {
        //
    }

    /**
     * A hook method after entity has been disapproved
     *
     * @return mixed
     */
    function afterDisapproved()
    {
        //
    }
}