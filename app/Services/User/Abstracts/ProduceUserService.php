<?php

namespace App\Services\User\Abstracts;

use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\Contracts\ProduceServiceInterface;

abstract class ProduceUserService implements ProduceServiceInterface
{
    /**
     * @var App\Repositories\Contracts\UserRepositoryInterface
     */
    protected $repo;

    /**
     * ProduceUserService constructor.
     *
     * @param \App\Repositories\Contracts\UserRepositoryInterface $repo
     */
    public function __construct(UserRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Prepare attributes before insert/update
     *
     * @param \App\Http\Request $request
     *
     * @return array
     */
    abstract public function prepareAttributes($request);

    /**
     * Execute produce a user
     *
     * @param \App\Http\Request $request
     * @return object
     */
    abstract public function execute($request);
}
