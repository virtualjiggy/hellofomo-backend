<?php

namespace App\Services\Device;

use App\Services\Contracts\ProduceServiceInterface;
use GuzzleHttp\Client;
use Log;

class DeviceRefreshTokenService implements ProduceServiceInterface
{
    /**
     * @var int
     */
    protected $errorCode;

    /**
     * @var string
     */
    protected $errorMessage;

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * Execute produce an entity
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        try {
            $http = new Client();
            $url = config('app.url') . '/oauth/token';
            $response = $http->post($url, [
                'form_params' => [
                    'grant_type'    => 'refresh_token',
                    'client_id'     => $request->get('client_id', ''),
                    'client_secret' => $request->get('client_secret', ''),
                    'refresh_token' => $request->get('refresh_token', ''),
                ],
            ]);

            return json_decode((string) $response->getBody(), true);
        } catch(\Exception $ex) {
            $this->setErrorCodeAndErrorMessage($ex);
            Log::error('Failed to refresh token. Request url: ' . $url . '. Error: ' . $ex->getMessage());
        }

        return false;
    }

    /**
     * Set error code & error message
     *
     * @param null|Exception $ex
     */
    private function setErrorCodeAndErrorMessage($ex = null)
    {
        if ( $ex == null || !($ex instanceof \Exception) ) {
            return;
        }

        switch( $ex->getCode() )
        {
            case 400:
                // parse json response which is containing in the error message
                $jsonResponse = json_decode(substr($ex->getMessage(), strpos($ex->getMessage(), '{"error"')));

                $this->errorCode = isset($jsonResponse->message) && $jsonResponse->message == __('The refresh token is invalid.') ? 1011 : 1012;
                $this->errorMessage = isset($jsonResponse->message) ? $jsonResponse->message : __('Bad request');
                break;
            case 401:
                $this->errorCode = 1007;
                $this->errorMessage = __('Invalid client');
                break;
            default:
                $this->errorCode = 1012;
                $this->errorMessage = __('Failed to refresh token');
                break;
        }
    }
}
