<?php namespace App\Services\Abstracts;

use App\Services\Contracts\ApproveEntityServiceInterface;

abstract class ApproveEntityServiceAbstract implements ApproveEntityServiceInterface
{
    /**
     * @var Repository $repo
     */
    protected $repo;

    /**
     * @var \App\Repositories\Contracts\CompanyRepositoryInterface
     */
    protected $compRepo;

    /**
     * @var array
     */
    protected $ids;

    /**
     * @var collection
     */
    protected $entities;

    /**
     * A hook method after entity has been approved
     *
     * @return mixed
     */
    abstract function afterApproved();

    /**
     * A hook method after entity has been disapproved
     *
     * @return mixed
     */
    abstract function afterDisapproved();

    /**
     * @param \Illuminate\Http\Request $request
     * @param string $status
     *
     * @return bool
     */
    private function _getIDs($request, $status)
    {
        $invalidIds = [];
        $this->ids  = explode('-', $request->id);
        foreach ($this->ids as $id) {
            if ( !ctype_digit($id) || !($entity = $this->repo->find($id)) || $entity->status != $status ) {
                $invalidIds[] = $id;
            } else {
                // ID is OK, store the entity to perform query later, no need to find again
                $this->entities[] = $entity;
            }
        }

        return (count($invalidIds) > 0) ? false : true;
    }

    /**
     * Approve an entity
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public function approve($request)
    {
        if ( !$this->_getIDs($request, config('elidev.list_default_status.pending')) ) {
            return false;
        }

        try {
            foreach ($this->entities as $entity) {
                $entity->update([ 'status' => config('elidev.list_default_status.publish') ]);
            }

            $this->afterApproved();
        } catch(\Exception $e) {
            return false;
        }

        return true;
    }

    /**
     * Dis-approve an entity
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public function disapprove($request)
    {
        if ( !$this->_getIDs($request, config('elidev.list_default_status.publish')) ) {
            return false;
        }

        try {
            foreach ($this->entities as $entity) {
                $entity->update([ 'status' => config('elidev.list_default_status.pending') ]);
            }

            $this->afterDisapproved();
        } catch(\Exception $e) {
            return false;
        }

        return true;
    }
}
