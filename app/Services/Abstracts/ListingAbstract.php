<?php
namespace App\Services\Abstracts;

use App\Services\Contracts\ListingServiceInterface;
use Illuminate\Pagination\LengthAwarePaginator;

abstract class ListingAbstract implements ListingServiceInterface
{
    /**
     * @var $repo
     */
    protected $repo;

    /**
     * @var $collection
     */
    protected $collection;

    /**
     * @var $columns
     */
    protected $columns;

    /**
     * Handle ajax URL
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    abstract public function ajaxURL($request);

    /**
     * Get listing data
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    abstract public function execute($request);

    /**
     * Format the result for listing
     *
     * @param Request $request
     * @param array $data
     * @param boolean | string $hasMore
     * @author vulh
     * @return array
     */
    public function formatResult($request, $data = [], $hasMore = '')
    {
        $total = 0;
        if ($this->collection && ($this->collection instanceof LengthAwarePaginator))
        {
            $collectionData = $this->collection->toArray();
            $data = sizeof($data) ? $data : $collectionData['data'];

            if ($hasMore === '') {
                $hasMore = $this->collection->currentPage() < $this->collection->lastPage();
            }
            $total = !empty($collectionData['total']) ? $collectionData['total'] : 0;
        }

        return [
            'total' => $total,
            'items' => $data,
            'hasMore'   => $hasMore,
            'url'   => $this->ajaxURL($request)
        ];
    }
}
