<?php

namespace App\Services\Abstracts;

use App\Repositories\Criterias\FilterWithTrashedCriteria;
use App\Services\Contracts\DeleteEntityServiceInterface;
use Log;

abstract class DeleteEntityServiceAbstract implements DeleteEntityServiceInterface
{
    /**
     * @var Object Model $entity
     */
    protected $entity;

    /**
     * @var Repository $repo
     */
    protected $repo;

    /**
     * Process after destroy
     *
     * @return mixed
     */
    abstract function afterDestroy();

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    abstract function afterSoftDelete();

    /**
     * Process after restore
     *
     * @return mixed
     */
    abstract function afterRestore();

    /**
     * Soft delete entity
     *
     * @param integer $id
     * @return boolean
     */
    public function softDelete($id)
    {
        if ($this->entity = $this->repo->find($id)) {
            if ($softDeleted = $this->entity->delete()) {
                $this->afterSoftDelete();
            }
            return $softDeleted;
        }
        return false;
    }

    /**
     * Restore an entity
     *
     * @param integer $id
     * @return boolean
     */
    public function restore($id)
    {
        if ($this->entity = $this->repo->findSoftDelete($id)) {
            if ($restored = $this->entity->restore()) {
                $this->afterRestore();
            }
            return $restored;
        }
        return false;
    }

    /**
     * Destroy an entity
     *
     * @param integer $id
     * @return boolean
     */
    public function destroy($id)
    {
        if ($this->entity = $this->repo->findSoftDelete($id) ) {
            if ($deleted = $this->entity->forceDelete()) {
                // Execute after destroy
                $this->afterDestroy();
            }
            return $deleted;
        }

        return false;
    }

    /**
     * @param $ids
     * @return bool
     */
    public function multiSoftDelete($ids) {
        $result = true;
        try {
            foreach ($ids as $id) {
                if(!$this->softDelete($id)) {
                    $result = false;
                }
            }
        } catch(\Exception $ex) {
            $result = false;
        }
        return $result;
    }

    /**
     * @param $ids
     * @return bool
     */
    public function multiRestore($ids) {
        $result = true;
        try {
            foreach ($ids as $id) {
                if(!$this->restore($id)) {
                    $result = false;
                }
            }
        } catch(\Exception $ex) {
            $result = false;
        }
        return $result;
    }


    /**
     * @param $ids
     * @return bool
     */
    public function multiDestroy($ids) {
        $result = true;
        try {
            foreach ($ids as $id) {
                if(!$this->destroy($id)) {
                    $result = false;
                }
            }
        } catch(\Exception $ex) {
            $result = false;
        }
        return $result;
    }


}
