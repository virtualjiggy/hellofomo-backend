<?php

namespace App\Services\News\Admin;

use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Services\Abstracts\DeleteEntityServiceAbstract;

class DeleteNewsService extends DeleteEntityServiceAbstract
{
    /**
     * DeleteNewsService constructor.
     *
     * @param \App\Repositories\Contracts\NewsRepositoryInterface $newsRepo
     */
    public function __construct(NewsRepositoryInterface $newsRepo)
    {
        $this->repo = $newsRepo;
    }

    /**
     * Process after destroy
     *
     * @return mixed
     */
    function afterDestroy()
    {
    }

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    function afterSoftDelete()
    {
    }

    /**
     * Process after restore
     *
     * @return mixed
     */
    function afterRestore()
    {
    }
}
