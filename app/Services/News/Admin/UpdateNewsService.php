<?php

namespace App\Services\News\Admin;

use App\Services\News\Abstracts\UpdateNewsService as UpdateNewsServiceAbstract;

class UpdateNewsService extends UpdateNewsServiceAbstract
{
    /**
     * The hook method is executed after news is update to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function afterUpdate($request)
    {
    }
}
