<?php

namespace App\Services\News\Admin;

use App\Services\News\Abstracts\ApplyCategoriesTagsService as ApplyCategoriesTagsServiceAbstract;

class ApplyCategoriesTagsService extends ApplyCategoriesTagsServiceAbstract
{
    /**
     * The hook method is executed after applied categories or tags for news
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function afterApplied($request)
    {
    }
}
