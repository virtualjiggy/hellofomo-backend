<?php

namespace App\Services\News\Admin;

use App\Services\News\Abstracts\StoreNewsService as StoreNewsServiceAbstract;

class StoreNewsService extends StoreNewsServiceAbstract
{
    /**
     * The hook method is executed after news is added to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function afterStored($request)
    {
    }
}
