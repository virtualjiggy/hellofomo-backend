<?php

namespace App\Services\News\API;

use App\Services\News\Abstracts\SingleNewsService as SingleAbstract;
use App\Utils\Utils;

class SingleNewsService extends SingleAbstract
{
    /**
     * After get single news, use this function to get more data you want
     *
     * @return mixed
     */
    function afterSingled()
    {
        $news = null;
        if ( $this->news ) {
            $lang = \Request::get('lang');
            if (!$lang) {
                $lang = \App::getLocale();
            }
            $news['id'] = $this->news->id;
            $news['title'] = $this->news->title;
            $news['introduction'] = $this->news->introduction;
            $news['description'] = $this->news->description;
            $news['available_date'] = empty($this->news->available_date) ? '' : $this->news->available_date->format(Utils::dateTimeFormatBasedOnLanguage($lang)['date_time_format']);
            $news['primary_image'] = $this->news->image_src;
            $news['meta_title'] = $this->news->meta_title;
            $news['meta_permalink'] = $this->news->meta_permalink;
            $news['meta_description'] = $this->news->meta_description;

            //tags
            $tags = $this->news->tags;
            $news['tags'] = array_map(function ($value) {
                return ['id' => $value['id'], 'name' => $value['name']];
            }, $tags ? $tags->toArray() : []);

            //categories
            $categories = $this->news->categories;
            $news['categories'] = array_map(function ($value) {
                return ['id' => $value['id'], 'name' => $value['name']];
            }, $categories ? $categories->toArray() : []);

            // authors
            $authors = $this->news->authors;
            $news['authors'] = array_map(function ($value) {
                $name = empty($value['display_name']) ? $value['first_name'] . ' ' . $value['last_name'] : $value['display_name'];

                return ['id' => $value['id'], 'name' => $name];
            }, $authors ? $authors->toArray() : []);

            //galleries
            $galleries = $this->news->galleries;
            $news['galleries'] = array_map(function ($value) {
                return url('/') . $value['url'];
            }, $galleries ? $galleries->toArray() : []);

            //attachments
            $attachments = $this->news->attachments;
            $news['attachments'] = array_map(function ($value) {
                return url('/') . $value['url'];
            }, $attachments ? $attachments->toArray() : []);

            //relatedNews
            $related_news = $this->news->relatedNewsFull;
            $news['related_news'] = [];

            foreach ($related_news as $item) {
                $news['related_news'][] = ['id' => $item->id, 'title' => $item->title];
            }
        }

        $this->news = $news;
    }
}
