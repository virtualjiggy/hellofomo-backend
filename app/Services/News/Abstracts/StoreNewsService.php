<?php

namespace App\Services\News\Abstracts;

use App\Utils\Utils;
use Carbon\Carbon;
use Illuminate\Http\Request;

abstract class StoreNewsService extends ProduceNewsService
{
    /**
     * @var \App\Models\News
     */
    protected $news;

    /**
     * The hook method is executed after news is added to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    abstract public function afterStored($request);

    /**
     * Prepare attributes before insert/update
     *
     * @param Request $request
     *
     * @return array
     */
    public function prepareAttributes($request)
    {
        $data = $request->only($this->fields());

        if ( isset($data['available_date']) ) {
            $format = Utils::dateTimeFormatBasedOnLanguage()['date_time_format'];
            $format = str_replace('i:H', 'H:i', $format);
            $available_date = $data['available_date'] . ' ' . $request->input('available_time', '00:00');
            $data['available_date'] = Carbon::createFromFormat($format, $available_date)->format('Y-m-d H:i:s');
        }

        if ( !isset($data['status']) || $data['status'] == '' ) {
            $data['status'] = config('elidev.list_default_status.pending');
        }

        if ( isset($data['galleries']) ) {
            $data['gallery_ids'] = serialize($data['galleries']);
            unset($data['galleries']);
        }

        if ( isset($data['attachments']) ) {
            $data['attachment_ids'] = serialize($data['attachments']);
            unset($data['attachments']);
        }

        return $data;
    }

    /**
     * Specify fields need to store
     *
     * @return array
     */
    public function fields()
    {
        return [
            'title', 'available_date', 'keywords',
            'status', 'introduction', 'description',
            'image_id', 'galleries', 'attachments',
            'meta_title', 'meta_permalink', 'meta_description'
        ];
    }

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $data = $this->prepareAttributes($request);
        $this->news = $this->newsRepo->create($data);

        // attach categories
        $this->news->categories()->attach($request->get('categories', []));

        // attach tags
        $this->news->tags()->attach($request->get('tags', []));

        // attach authors
        $this->news->authors()->attach($request->get('authors', []));

        // attach related news
        $this->news->relatedNews()->attach($request->get('related_news', []));

        // execute hook method
        $this->afterStored($request);

        return $this->news;
    }
}
