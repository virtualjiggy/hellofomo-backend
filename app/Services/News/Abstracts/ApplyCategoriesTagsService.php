<?php

namespace App\Services\News\Abstracts;

use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Services\Contracts\ProduceServiceInterface;

abstract class ApplyCategoriesTagsService implements ProduceServiceInterface
{
    /**
     * @var \App\Repositories\Contracts\NewsRepositoryInterface
     */
    protected $newsRepo;

    /**
     * @var array
     */
    protected $newsIds;

    /**
     * @var
     */
    protected $newsCollection;

    /**
     * @var string
     */
    protected $type;

    /**
     * @var array
     */
    protected $indeterminateIds;

    /**
     * ApplyCategoriesTagsService constructor.
     *
     * @param \App\Repositories\Contracts\NewsRepositoryInterface $newsRepo
     */
    public function __construct(NewsRepositoryInterface $newsRepo)
    {
        $this->newsRepo = $newsRepo;
    }

    /**
     * The hook method is executed after applied categories or tags for news
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    abstract public function afterApplied($request);

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        // verify before execute
        $this->type = $request->get('type', '');
        $categories = $request->get('categories', []);
        $tags       = $request->get('tags', []);
        $this->indeterminateIds = empty($request->get('indeterminate_checkboxes', '')) ? [] : array_unique(explode(',', $request->get('indeterminate_checkboxes')));
        if ( $request->get('news', '') == ''
            || ($this->type == 'category' && count($categories) <= 0 && count($this->indeterminateIds) <= 0)
            || ($this->type == 'tag' && count($tags) <= 0 && count($this->indeterminateIds) <= 0) ) {
            return false;
        }

        // init variables
        $this->newsIds = array_unique(explode(',', $request->get('news')));
        $this->newsCollection = $this->newsRepo->findSelectedNews(['id', 'available_date'], $this->newsIds);
        if ( $this->newsCollection->isEmpty() ) {
            return false;
        }

        // do apply IDs
        $result = false;
        if ( $this->type == 'category' ) {
            $result = $this->syncCategories($categories);
        } else if ( $this->type == 'tag' ) {
            $result = $this->syncTags($tags);
        }

        return $result ? $this->newsCollection : false;
    }

    /**
     * Sync categories
     *
     * @param array $categories
     *
     * @return bool
     */
    private function syncCategories($categories = [])
    {
        $this->newsCollection->transform(function($newsItem) use ($categories) {
            // get old IDs
            $oldIds = $newsItem->categories->pluck('id')->all();

            // find all IDs which are still be kept
            $stillKeptIDs = array_intersect($this->indeterminateIds, $oldIds);

            // merge kept IDs with newest IDs to get final IDs want to sync
            $finalIds = array_merge($stillKeptIDs, $categories);

            // sync & fresh category data
            $newsItem->categories()->sync($finalIds);
            $newsItem->load('categories');

            // update UI (categories column) with newest data when call AJAX
            $newsItem->category_ids = implode(',', $newsItem->categories->pluck('id')->all());
            $newsItem->category_names_with_link = $newsItem->category_names_with_link;

            return $newsItem;
        });

        return true;
    }

    /**
     * Sync tags
     *
     * @param array $tags
     *
     * @return bool
     */
    private function syncTags($tags = [])
    {
        $this->newsCollection->transform(function($newsItem) use ($tags) {
            // get old IDs
            $oldIds = $newsItem->tags->pluck('id')->all();

            // find all IDs which are still be kept
            $stillKeptIDs = array_intersect($this->indeterminateIds, $oldIds);

            // merge kept IDs with newest IDs to get final IDs want to sync
            $finalIds = array_merge($stillKeptIDs, $tags);

            // sync & fresh tag data
            $newsItem->tags()->sync($finalIds);
            $newsItem->load('tags');

            // update UI (tags column) with newest data when call AJAX
            $newsItem->tag_ids = implode(',', $newsItem->tags->pluck('id')->all());
            $newsItem->tag_names_with_link = $newsItem->tag_names_with_link;

            return $newsItem;
        });

        return true;
    }
}
