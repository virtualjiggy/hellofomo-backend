<?php

namespace App\Services\News\Abstracts;

use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Repositories\Criterias\WithRelationsCriteria;
use App\Services\Contracts\ProduceServiceInterface;
use Botble\Media\Repositories\Interfaces\MediaFileInterface;

abstract class SingleNewsService implements ProduceServiceInterface
{
    /**
     * @var \App\Models\News
     */
    protected $news;

    /**
     * @var \App\Repositories\Contracts\NewsRepositoryInterface
     */
    protected $newsRepo;

    /**
     * @var array
     */
    protected $mediaFilesColumns;

    /**
     * SingleNewsService constructor.
     *
     * @param \App\Repositories\Contracts\NewsRepositoryInterface $newsRepo
     */
    public function __construct(NewsRepositoryInterface $newsRepo)
    {
        $this->newsRepo = $newsRepo;

        // only get these columns when query media_files table data
        $this->mediaFilesColumns = ['id', 'name', 'mime_type', 'url', 'is_public'];
    }

    /**
     * After get single news, use this function to get more data you want
     *
     * @return mixed
     */
    abstract function afterSingled();

    /**
     * Specify fields need to return
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id', 'title', 'available_date', 'keywords',
            'status', 'introduction', 'description',
            'image_id', 'gallery_ids', 'attachment_ids',
            'meta_title', 'meta_permalink', 'meta_description'
        ];
    }

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $this->newsRepo->resetCriteria();
        $this->news = $this->newsRepo
            ->pushCriteria(new WithRelationsCriteria(['categories', 'tags', 'authors', 'image', 'relatedNews']))
            ->find($request->id, $this->fields());

        // get media gallery files
        if ( !empty($this->news->gallery_ids) ) {
            $file = app(MediaFileInterface::class)->getModel();
            $ids = str_contains($this->news->gallery_ids, '{') ? unserialize($this->news->gallery_ids): json_decode($this->news->gallery_ids);
            $this->news->galleries = $file->whereIn('id', $ids)->get($this->mediaFilesColumns);
        }

        // get media attachment files
        if ( !empty($this->news->attachment_ids) ) {
            $file = app(MediaFileInterface::class)->getModel();
            $ids = str_contains($this->news->attachment_ids, '{') ? unserialize($this->news->attachment_ids): json_decode($this->news->attachment_ids);
            $this->news->attachments = $file->whereIn('id', $ids)->get($this->mediaFilesColumns);
        }

        $this->afterSingled();

        return $this->news;
    }
}
