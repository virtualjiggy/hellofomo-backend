<?php

namespace App\Services\Contracts;


interface ApproveEntityServiceInterface
{
    /**
     * Approve an entity
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public function approve($request);

    /**
     * Dis-approve an entity
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public function disapprove($request);
}
