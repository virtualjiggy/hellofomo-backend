<?php
namespace App\Services\Contracts;

interface ListingServiceInterface
{
    /**
     * Handle ajax URl
     *
     * @param Illuminate\Http\Request $request
     * @return array
     */
    public function ajaxURL($request);

    /**
     * Get Listing data
     *
     * @param Illuminate\Http\Request $request
     * @return array
     */
    public function execute($request);

    /**
     * Format the result for listing
     *
     * @param Request $request
     * @param array $data
     * @param boolean | string $hasMore
     * @author vulh
     * @return array
     */
    public function formatResult($request, $data = [], $hasMore = '');
}