<?php

namespace App\Services\Contracts;

interface ProduceServiceInterface
{
    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     * @author vulh
     * @return mixed
     */
    public function execute($request);
}
