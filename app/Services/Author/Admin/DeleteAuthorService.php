<?php

namespace App\Services\Author\Admin;

use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Services\Abstracts\DeleteEntityServiceAbstract;

class DeleteAuthorService extends DeleteEntityServiceAbstract
{
    /**
     * DeleteAuthorService constructor.
     *
     * @param \App\Repositories\Contracts\AuthorRepositoryInterface $authorRepo
     */
    public function __construct(AuthorRepositoryInterface $authorRepo)
    {
        $this->repo = $authorRepo;
    }

    /**
     * Process after destroy
     *
     * @return mixed
     */
    function afterDestroy()
    {
    }

    /**
     * Process after soft delete
     *
     * @return mixed
     */
    function afterSoftDelete()
    {
    }

    /**
     * Process after restore
     *
     * @return mixed
     */
    function afterRestore()
    {
    }
}
