<?php

namespace App\Services\Author\Admin;

use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Services\Abstracts\ListingAbstract;
use App\Services\Author\Traits\AuthorTrait;
use App\Utils\Utils;

class ListingAuthorService extends ListingAbstract
{
    use AuthorTrait;

    /**
     * @var \App\Repositories\Contracts\AuthorRepositoryInterface
     */
    protected $authorRepo;

    /**
     * ListingAuthorService constructor.
     *
     * @param \App\Repositories\Contracts\AuthorRepositoryInterface $authorRepo
     */
    public function __construct(AuthorRepositoryInterface $authorRepo)
    {
        $this->authorRepo = $authorRepo;
    }

    /**
     * Returned columns
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id', 'first_name', 'last_name',
            'salutation_id', 'title_id',
        ];
    }

    /**
     * Handle ajax URL
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function ajaxURL($request)
    {
    }

    /**
     * Get listing data
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function execute($request)
    {
        // set filter params (first_name, last_name)
        $pagination = Utils::getDataTablePagination(
            $request,
            [ 'first_name', 'last_name', 'salutation_name', 'title_name' ]
        );

        if ( in_array($pagination['sortColumn'], ['checkbox']) ) {
            $pagination['sortColumn'] = 'id';
        }

        $collection = $this->authorRepo->listing(
            $this->fields(),
            $pagination['terms'],
            $pagination['limit'],
            $pagination['sortColumn'],
            $pagination['sortOrder'],
            $pagination['onlyTrashed']
        );

        return $this->formatAuthors($collection);
    }
}
