<?php

namespace App\Services\Author\Admin;

use App\Services\Author\Abstracts\StoreAuthorService as StoreAuthorServiceAbstract;

class StoreAuthorService extends StoreAuthorServiceAbstract
{
    /**
     * The hook method is executed after author is added to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function afterStored($request)
    {
    }
}
