<?php

namespace App\Services\Author\Admin;

use App\Services\Author\Abstracts\UpdateAuthorService as UpdateAuthorServiceAbstract;

class UpdateAuthorService extends UpdateAuthorServiceAbstract
{
    /**
     * The hook method is executed after author is update to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    public function afterUpdate($request)
    {
    }
}
