<?php

namespace App\Services\Author\Abstracts;

use App\Services\Author\Traits\AuthorTrait;

abstract class UpdateAuthorService extends ProduceAuthorService
{
    use AuthorTrait;

    /**
     * @var \App\Models\Author
     */
    protected $author;

    /**
     * Prepare attributes before insert/update
     *
     * @param \App\Http\Request $request
     *
     * @return array
     */
    public function prepareAttributes($request)
    {
    }

    /**
     * Specify fields need to update
     *
     * @return array
     */
    public function fields()
    {
        return [ 'first_name', 'last_name', 'display_name', 'description', 'salutation_id', 'title_id', 'image_id' ];
    }

    /**
     * The hook method is executed after author is update to DB
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return mixed
     */
    abstract public function afterUpdate($request);

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $this->author = $this->authorRepo->update($request->only($this->fields()), $request->id);
        if ( !$this->author ) {
            return false;
        }

        $this->afterUpdate($request);

        return $this->formatAuthor($this->author);
    }
}
