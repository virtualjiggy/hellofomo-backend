<?php

namespace App\Services\Author\Abstracts;

use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Repositories\Criterias\WithRelationsCriteria;
use App\Services\Author\Traits\AuthorTrait;
use App\Services\Contracts\ProduceServiceInterface;

abstract class SingleAuthorService implements ProduceServiceInterface
{
    use AuthorTrait;

    /**
     * @var \App\Models\Author
     */
    protected $author;

    /**
     * @var \App\Repositories\Contracts\AuthorRepositoryInterface
     */
    protected $authorRepo;

    /**
     * SingleAuthorService constructor.
     *
     * @param \App\Repositories\Contracts\AuthorRepositoryInterface $authorRepo
     */
    public function __construct(AuthorRepositoryInterface $authorRepo)
    {
        $this->authorRepo = $authorRepo;
    }

    /**
     * After get single author, use this function to get more data you want
     *
     * @return mixed
     */
    abstract function afterSingled();

    /**
     * Specify fields need to return
     *
     * @return array
     */
    public function fields()
    {
        return [
            'id', 'first_name', 'last_name', 'display_name',
            'description', 'salutation_id', 'title_id',
            'image_id', 'created_at', 'updated_at',
        ];
    }

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    public function execute($request)
    {
        $this->authorRepo->resetCriteria();
        $this->author = $this->authorRepo
            ->pushCriteria(new WithRelationsCriteria(['salutation', 'title']))
            ->find($request->id, $this->fields());

        $this->afterSingled();

        return $this->formatAuthor($this->author);
    }
}
