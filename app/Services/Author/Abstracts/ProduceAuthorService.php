<?php

namespace App\Services\Author\Abstracts;

use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Services\Contracts\ProduceServiceInterface;

abstract class ProduceAuthorService implements ProduceServiceInterface
{
    /**
     * @var \App\Repositories\Contracts\AuthorRepositoryInterface
     */
    protected $authorRepo;

    /**
     * ProduceAuthorService constructor.
     *
     * @param \App\Repositories\Contracts\AuthorRepositoryInterface $authorRepo
     */
    public function __construct(AuthorRepositoryInterface $authorRepo)
    {
        $this->authorRepo = $authorRepo;
    }

    /**
     * Prepare attributes before insert/update
     *
     * @param \App\Http\Request $request
     *
     * @return array
     */
    abstract public function prepareAttributes($request);

    /**
     * Execute produce an entity
     *
     * @param \App\Http\Request $request
     *
     * @return mixed
     */
    abstract public function execute($request);
}
