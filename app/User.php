<?php

namespace App;

use App\Models\Company;
use App\Models\Deed;
use App\Notifications\AdminResetPasswordNotification;
use App\Utils\Utils;
use Botble\Media\Models\MediaFile;
use Carbon\Carbon;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'status',
        'image_id', 'language'
    ];

    protected $appends = [
        'name_with_link'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Check user is admin
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->hasRole('Administrator');
    }

    /**
     * User's image
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this
            ->hasOne(MediaFile::class, 'id', 'image_id')
            ->select(['id', 'user_id', 'name', 'folder_id', 'mime_type', 'url', 'is_public', 'deleted_at']);
    }

    /**
     * get name with link
     * @return string
     */
    public function getNameWithLinkAttribute()
    {
        return '<a href="' . route('admin.users.show', ['user' => $this->id]) . '">' . $this->name . '</a>';
    }

    /**
     * Get & format created at
     *
     * @param string $value
     *
     * @return string
     */
    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(Utils::dateTimeFormatBasedOnLanguage()['date_time_format']);
    }

    /**
     * Get & format updated at
     *
     * @param string $value
     *
     * @return string
     */
    public function getUpdatedAtAttribute($value)
    {
        return Carbon::parse($value)->format(Utils::dateTimeFormatBasedOnLanguage()['date_time_format']);
    }

    /**
     * Get user's image
     */
    public function getImageSrcAttribute()
    {
        return $this->image ? url('/') . '/' . $this->image->url : '';
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     * @ticket #13381 - HFBACKEND-100 System / Log-in - Implement forgot Password
     */
    public function sendPasswordResetNotification($token)
    {
        // override this method to implement send reset password email for admin user and normal user
        if ( request()->routeIs('admin.password.email') ) {
            $this->notify(new AdminResetPasswordNotification($token));
        } else {
            $this->notify(new ResetPasswordNotification($token));
        }
    }
}
