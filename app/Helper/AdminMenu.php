<?php

namespace App\Helper;

use Route;
use URL;

class AdminMenu
{
    /**
     * Display admin menu
     *
     * @ticket #12850 - integrating Admin LTE
     * @return string
     * @author Sang Nguyen
     */
    public static function render()
    {
        $menus = config('fomo.admin.menu', []);

        if ( count($menus) < 0 ) {
            return '';
        }

        foreach ($menus as $key => $item) {
            // check is active
            $item['url'] = Route::has($item['route']) ? route($item['route']) : '#';
            $item['active'] = false;
            if (URL::full() == $item['url'] || (isset($item['prefix']) && strpos(URL::full(), $item['prefix']) > 0)) {
                $item['active'] = true;
            }

            foreach (array_get($item, 'children', []) as $child_key => $child) {
                // check is active
                $child['url'] = Route::has($child['route']) ? route($child['route']) : '#';
                $child['active'] = false;
                if (URL::full() == $child['url'] || (isset($child['prefix']) && strpos(URL::full(), $child['prefix']) > 0)) {
                    $item['active'] = true;
                    $child['active'] = true;
                }
                $item['children'][$child_key] = $child;
            }

            $menus[$key] = $item;
        }

        return view('admin.partials.left-sidebar.menu', compact('menus'))->render();
    }
}
