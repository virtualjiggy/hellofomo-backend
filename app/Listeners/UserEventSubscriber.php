<?php

namespace App\Listeners;

class UserEventSubscriber
{
    /**
     * Handle user login events.
     * @param $event
     */
    public function onUserLogin($event)
    {
        // todo
    }

    /**
     * Handle user logout events.
     * @param $event
     */
    public function onUserLogout($event)
    {
        // todo
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );

        $events->listen(
            'Illuminate\Auth\Events\Logout',
            'App\Listeners\UserEventSubscriber@onUserLogout'
        );
    }

}