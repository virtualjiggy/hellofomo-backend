<?php

namespace App\Http\Requests\CustomValidations\Contracts;


interface CustomValidation
{
    /**
     * Check the request company sizes are existed in database
     *
     * @param $input
     * @return boolean
     */
    public function validate($input);
}