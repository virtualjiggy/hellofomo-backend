<?php

namespace App\Http\Requests\CustomValidations;

use App\Http\Requests\CustomValidations\Contracts\CustomValidation;

class ValidationVideo implements CustomValidation
{
    /**
     * Check valid youtube or vimeo
     *
     * @param string $input
     * @return boolean
     */
    public function validate($input)
    {
        return (preg_match('/youtu\.be/i', $input) || preg_match('/youtube\.com\/watch/i', $input)) || (preg_match('/vimeo\.com/i', $input));
    }

}




