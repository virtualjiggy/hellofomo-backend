<?php

namespace App\Http\Requests\CustomValidations;

use App\Http\Requests\CustomValidations\Contracts\CustomValidation;

class ValidationDimension implements CustomValidation
{
    /**
     * Check valid youtube or vimeo
     *
     * @param string $input
     * @return boolean
     */
    public function validate($input)
    {
        $maxWidth = config('elidev.default_max_image_width');
        $maxHeight = config('elidev.default_max_image_height');

        list($width, $height) = getimagesize($input);

        if ($input->getClientMimeType() == 'image/svg') {
            return true;
        }

        return $maxWidth >= $width && $maxHeight >= $height;
    }

}




