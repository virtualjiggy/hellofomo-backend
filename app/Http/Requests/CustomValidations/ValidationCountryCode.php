<?php

namespace App\Http\Requests\CustomValidations;

use App\Http\Requests\CustomValidations\Contracts\CustomValidation;
use Storage;
use Illuminate\Support\Facades\File;

class ValidationCountryCode implements CustomValidation
{
    /**
     * Check the request company sizes are existed in database
     *
     * @param $input
     *
     * @return boolean
     */
    public function validate($input)
    {
        if (!$input) {
            return true;
        }

        $countries = collect(json_decode(File::get(public_path() . '/countries.json'), true));
        if (count($countries) < 0) {
            return true; // no check if empty countries list
        }

        $country = $countries->filter(function ($value, $key) use ($input) {
            return strtolower($value['code']) == strtolower($input);
        })->first();
        if (is_null($country)) {
            return false;
        }

        return true;
    }
}
