<?php

namespace App\Http\Requests\CustomValidations;

use App\Http\Requests\CustomValidations\Contracts\CustomValidation;

class ValidationUrl implements CustomValidation
{
    /**
     * Check the request company sizes are existed in database
     *
     * @param $input
     *
     * @ticket #12811
     * @return boolean
     */
    public function validate($input)
    {
        // if user has not entered http:// https:// or ftp:// assume they mean http://
        if ( !preg_match("~^(?:f|ht)tps?://~i", $input) ) {
            $input = "http://" . $input;
        }

        return filter_var($input, FILTER_VALIDATE_URL) !== false;
    }
}
