<?php

namespace App\Http\Requests\CustomValidations;

use App\Http\Requests\CustomValidations\Contracts\CustomValidation;

class ValidationPerKeyword implements CustomValidation
{
    /**
     * Check the request company sizes are existed in database
     *
     * @param string $input - separate by commas
     * @return boolean
     */
    public function validate($input)
    {
        if (!$input) {
            return true;
        }

        $keywords = explode(',', $input);
        $n = count($keywords);
        foreach ($keywords as $i => $keyword) {

            $keyword = trim($keyword);
            $length = strlen($keyword);

            // Not check the last commas "Test, abc,"
            if (!$length && $n && $i == $n - 1) {
                continue;
            }

            if (!$length || $length > config('elidev.validation.per_keyword_length')) {
                return false;
            }
        }
        return true;
    }

}




