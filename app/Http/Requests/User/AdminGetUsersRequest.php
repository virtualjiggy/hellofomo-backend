<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class AdminGetUsersRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $sort = ['-name', 'name', '-id', 'id'];

        return [
            'page' => 'sometimes|required|integer|min:1',
            'limit' => 'sometimes|required|integer|min:1',
            'sort' => 'sometimes|required|in:' . implode(',', $sort),
            'keyword' => 'sometimes|required|string',
            'status' => 'sometimes|required|string|in:' . implode(',', config('elidev.list_default_status')),
            'trash' => 'sometimes|required|integer|in:1,0',
        ];
    }

    /**
     * Set allow params
     * @return array parameters
     */
    public function allowParams()
    {
        return ['page', 'limit', 'sort', 'keyword', 'status', 'trash'];
    }
}
