<?php

namespace App\Http\Requests\User;

use App\Http\Requests\Request;

class AdminStoreUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Set allow params
     *
     * @return array parameters
     */
    function allowParams()
    {
        return [
            'name', 'email', 'role',
            'password', 'password_confirmation', 'status',
            'country', 'city', 'phone', 'language', 'avatar'
        ];
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:255',
            'country' => 'nullable|string',
            'city' => 'nullable|string',
            'phone' => 'nullable|string|max:45',
            'language' => 'nullable|string|max:5',
            'status' => 'nullable|sometimes|required|string|in:' . implode(',', config('elidev.list_default_status')),
        ];

        // if update user request (PUT)
        if ($this->user) {
            $additionalRules = [
                'email' => 'required|string|max:255|email',
                'role' => 'sometimes|required|string|in:' . implode(',', config('elidev.role.allow')),
                'password' => 'sometimes|required|string|confirmed|min:6',
                'password_confirmation' => 'sometimes|required|string',
            ];
        } else {
            // if add new user request (POST)
            $additionalRules = [
                'email' => 'required|string|max:255|email|unique:users',
                'role' => 'required|string|in:' . implode(',', config('elidev.role.allow')),
                'password' => 'required|string|confirmed|min:6',
                'password_confirmation' => 'required|string',
            ];
        }

        return array_merge($rules, $additionalRules);
    }
}
