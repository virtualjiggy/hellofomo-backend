<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Utils\Utils;

abstract class Request extends FormRequest
{
    /**
     * @var array $allowParams
     */
    protected $allowParams = [];

    /**
     * Set allow params
     * @return array parameters
     */
    abstract function allowParams();

    /**
     * Request constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->allowParams = $this->allowParams();
    }

    /**
     * Validate parameters
     *
     * @return bool|array
     */
    public function validateParams()
    {
        $inputParams = array_keys($this->all());

        if (!Utils::allElmsArrayExistedInArray($inputParams, $this->allowParams)) {

            $invalid = array_diff($inputParams, $this->allowParams);

            return [
                'message' => sprintf(__('Bad request. API only allows %d parameters: %s. Invalid params: %s'), count($this->allowParams), implode(', ', $this->allowParams), implode(', ', $invalid)),
                'code' => 1000
            ];
        }
        return true;
    }

    /**
     * Override response method for API request
     *
     * @param  array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        // From the api request send header is application/json
        if ($errors && $this->expectsJson()) {
            return response()->error($errors);
        }
    }
}
