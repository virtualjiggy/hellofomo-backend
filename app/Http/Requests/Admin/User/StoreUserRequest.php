<?php

namespace App\Http\Requests\Admin\User;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->get('_method') == 'PUT' || \Request::isMethod('PUT')) {
            return [
                'name' => 'required|string|max:50',
                'email' => 'required|max:100|email|unique:users,email,' . (($this->user) ? $this->user : Auth::user()->id),
                'password' => 'max:100|confirmed',
                'password_confirmation' => 'max:100',
                'phone' => 'max:100',
                'language' => 'max:3',
            ];
        }

        return [
            'name' => 'required|string|max:50',
            'email' => 'required|max:100|email|unique:users',
            'password' => 'required|min:6|max:100|confirmed',
            'password_confirmation' => 'required|min:6|max:100',
            'phone' => 'max:100',
            'language' => 'max:3',
        ];

    }


}
