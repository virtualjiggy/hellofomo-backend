<?php

namespace App\Http\Requests\Admin\News;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'          => 'required|string|max:255',
            'description'    => 'nullable|string',
            'keywords'       => 'nullable|string|max:255',
            'meta_title'     => 'nullable|string|max:255',
            'meta_permalink' => 'nullable|string|max:255',
        ];
    }
}
