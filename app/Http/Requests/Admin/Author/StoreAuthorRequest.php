<?php

namespace App\Http\Requests\Admin\Author;

use Illuminate\Foundation\Http\FormRequest;

class StoreAuthorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'salutation_id' => 'required|integer',
            'first_name'    => 'required|string|max:255',
            'last_name'     => 'required|string|max:255',
            'display_name'  => 'required|string|max:255'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'salutation_id.required'   => __('Please choose an salutation'),
            'salutation_id.integer'    => __('Please choose an salutation'),
            'salutation_id.first_name' => __('Please input first name'),
            'salutation_id.last_name'  => __('Please input last name'),
        ];
    }
}
