<?php
namespace App\Http\Requests\APIs\V1;
use App\Http\Requests\Request;

class ListingNewsRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $lang = implode(',', config("fomo.support_languages"));
        return [
            'categories'        => 'sometimes',
            'tags'              => 'sometimes',
            'limit'             => 'sometimes|numeric|min:1',
            'page'              => 'sometimes|numeric|min:0',
            'lang'              => 'sometimes|in:' . $lang,
        ];
    }

    /**
     * Set allow params
     * @return array parameters
     */
    function allowParams()
    {
        // TODO: Implement allowParams() method.
    }
}