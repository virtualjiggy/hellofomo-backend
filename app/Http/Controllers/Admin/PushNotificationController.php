<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\NewsRepositoryInterface;
use App\Services\PushNotification\Frontend\PushByTopicService;
use Illuminate\Http\Request;
use Log;
use Settings;

class PushNotificationController extends Controller
{
    /**
     * Display push notification page
     *
     * @return \Illuminate\Http\Response
     * @ticket #13198 - HFBACKEND-35 - App / Push Notification - Layout
     */
    public function getPushNotification()
    {
        // get max length of custom text
        $maxLengthOfCustomText = Settings::get(config('fomo.app_settings.push_notification_max_length'), 1024);

        return view('admin.app.push-notification.index', [
            'maxLengthOfCustomText' => $maxLengthOfCustomText,
            'breadcrumbs' => [
                ['text' => __('App')],
                ['text' => __('Push notification')],
            ],
        ]);
    }

    /**
     * Handle push notification
     *
     * @param Request                                                    $request
     * @param NewsRepositoryInterface                                    $newsRepo
     * @param \App\Services\PushNotification\Frontend\PushByTopicService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #13198 - HFBACKEND-35 - App / Push Notification - Layout
     */
    public function postPushNotification(Request $request, NewsRepositoryInterface $newsRepo, PushByTopicService $service)
    {
        try {
            $selectedNews = $request->get('related_news');
            if (!empty($selectedNews[0])) {
                $news = $newsRepo->find($selectedNews[0]);

                if (!$news) {
                    return response()->error(__('Not found the selected news'));
                }
            }

            $isNews      = ($request->get('active_tab', 'news') == 'news' && !empty($news));
            $pushTitle   = $isNews ? $news->title : ''; // no need title when push custom text
            $pushContent = $isNews ? $news->introduction : $request->get('custom_text');
            $newsId      = $isNews ? $news->id : '';
            $request->merge([
                'push_title'   => $pushTitle,
                'push_content' => $pushContent,
                'news_id'      => $newsId,
            ]);

            if ( $service->push($request) ) {
                return response()->success(['message' => __('Pushed notification')]);
            }
            return response()->error('Can not push notification. Error: ' . $service->getError());
        } catch(\Exception $ex) {
            Log::error('Can not push notification. Error: ' . $ex->getMessage());
            return response()->error(__('Error! Can not push notification'));
        }
    }
}
