<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreCategoryRequest;
use App\Models\Category;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Utils\Utils;
use Illuminate\Http\Request;
use Log;

class SalutationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12950 - HFBACKEND-12 - Author - Salutation & Titles menu
     */
    public function index(CategoryRepositoryInterface $categoryRepo)
    {
        // dynamic set category types for filter
        $filterValues = collect($categoryRepo->getCategoryTypesForFilter())->except(config('fomo.category.types.news'));
        $table = [
            'filter_navigation' => false,
            'title' => 'Salutations & Titles Overview',
            'id' => 'categories-datatable',
            'ajax' => [
                'src' => 'admin.salutations.more', // route name
            ],
            'columns' => [
                'id' => [
                    'text' => '#',
                    'width' => '5',
                ],
                'name' => [
                    'text' => 'Name',
                    'filter' => [
                        'type' => 'input',
                    ],
                ],
                'type' => [
                    'text' => 'Type',
                    'width' => '10%',
                    'filter' => [
                        'type' => 'select',
                        'data' => $filterValues,
                    ],
                ],
            ],
            'actions' => [
                'width'  => '70',
                'add'    => [
                    'name'  => 'Add new',
                    'route' => 'admin.salutations.create', // route name
                    'role' => 'salutations.create',
                ],
                'edit'   => [
                    'route' => 'admin.salutations.edit', // route name
                    'role' => 'salutations.edit',
                ],
                'delete' => [
                    'route' => 'admin.salutations.destroy', // route name
                    'icon' => 'fa fa-times',
                    'color' => 'red-intense',
                    'role' => 'salutations.destroy',
                ],
            ],
        ];

        $breadcrumbs = [
            [
                'url' => route('admin.authors.index'),
                'text' => __('Authors')
            ],
            [
                'text' => __('Salutations & Titles')
            ],
        ];

        return view('admin.salutation.list', [
            'table'       => $table,
            'breadcrumbs' => $breadcrumbs,
            'pageTitle'   => __('Salutations & Titles'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $breadcrumbs = [
            [
                'url' => route('admin.authors.index'),
                'text' => __('Authors')
            ],
            [
                'url' => route('admin.salutations.index'),
                'text' => __('Salutations & Titles')
            ],
            [
                'text' => __('Create'),
            ],
        ];

        return view('admin.salutation.create', [
            'categoryTypes' => collect(config('fomo.category.types'))->except('news'),
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\Category\StoreCategoryRequest  $request
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request, CategoryRepositoryInterface $categoryRepo)
    {
        $category = $categoryRepo->create($request->only([ 'name', 'type' ]));

        $key = $category ? 'success' : 'error';
        $message = $category ? __('Create category successfully') : __('Create failed. Please try again');

        return redirect(route('admin.salutations.index'))->with($key, $message)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                                                    $id
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id, CategoryRepositoryInterface $categoryRepo)
    {
        $category = $categoryRepo->find($id, ['id', 'name', 'type']);
        if ( !$category ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.authors.index'),
                'text' => __('Authors')
            ],
            [
                'url' => route('admin.categories.index'),
                'text' => __('Salutations & Titles')
            ],
            [
                'text' => $category->name
            ],
            [
                'text' => __('Edit')
            ],
        ];

        return view('admin.salutation.edit', [
            'category'      => $category,
            'categoryTypes' => collect(config('fomo.category.types'))->except('news'),
            'breadcrumbs'   => $breadcrumbs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\Category\StoreCategoryRequest  $request
     * @param  int                                                    $id
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StoreCategoryRequest $request, $id, CategoryRepositoryInterface $categoryRepo)
    {
        $result = $categoryRepo->update($request->only(['name', 'type']), $id);

        $key     = $result ? 'success' : 'error';
        $message = $result ? __('Updated successfully') : __('Update failed. Please try again');

        return redirect(route('admin.salutations.index'))->with($key, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Category::find($id)->delete();

            return redirect(route('admin.salutations.index'))->with('success', __('Permanently deleted successfully'));
        } catch(\Exception $ex) {
            Log::error(sprintf('Failed to delete the category (ID = %s). Message: %s', $id, $ex->getMessage()));
        }

        return back()->with('error', __('Failed to delete. Please try again.'));
    }

    /**
     * Get more categories
     *
     * @param \Illuminate\Http\Request                                $request
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12950 - HFBACKEND-12 - Author - Salutation & Titles menu
     */
    public function more(Request $request, CategoryRepositoryInterface $categoryRepo)
    {
        // set filter params (name, type)
        $pagination = Utils::getDataTablePagination($request, ['name', 'type']);
        if ( $pagination['terms']['type'] == null ) {
            $pagination['terms']['type'] = [
                config('fomo.category.types.salutation'),
                config('fomo.category.types.title')
            ];
        }

        // get data
        $categories = $categoryRepo->listing(
            [ 'id', 'name', 'type', 'created_at', 'updated_at' ],
            $pagination['terms'],
            $pagination['limit'],
            $pagination['sortColumn'],
            $pagination['sortOrder']
        );
        $total = $categories->total();

        return json_encode([
            'data'            => $categories->getCollection()->toArray(),
            'recordsTotal'    => $total,
            'recordsFiltered' => $total,
        ]);
    }
}
