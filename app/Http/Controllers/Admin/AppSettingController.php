<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Artisan;
use Illuminate\Http\Request;
use Log;
use Settings;

class AppSettingController extends Controller
{
    /**
     * @var array only get app's settings
     */
    protected $appSettingKeys;

    /**
     * AppSettingController constructor.
     */
    public function __construct()
    {
        $this->appSettingKeys = [
            config('fomo.app_settings.font'),
            config('fomo.app_settings.push_notification_max_length'),
            config('fomo.app_settings.fcm_sender_id'),
            config('fomo.app_settings.fcm_server_key'),
        ];
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     * @ticket #13238 - HFBACKEND-79 - APP / Settings - Move App Setting
     */
    public function edit()
    {
        $appSettings = [];
        foreach ($this->appSettingKeys as $appSettingKey) {
            $appSettings[$appSettingKey] = Settings::get($appSettingKey, '');
        }

        return view('admin.app.settings.edit', [
            'settings' => $appSettings,
            'breadcrumbs' => [
                [ 'text' => __('App') ],
                [ 'text' => __('Settings') ],
                [ 'text' => __('Edit') ],
            ],
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     *
     * @return \Illuminate\Http\Response
     * @ticket #13238 - HFBACKEND-79 - APP / Settings - Move App Setting
     */
    public function update(Request $request)
    {
        // only get settings belong to the App
        $items = $request->only($this->appSettingKeys);
        try {
            foreach($items as $key => $val) {
                Settings::set($key, $val);
            }

            // create cache config
            Artisan::call('config:cache');

            return back()->with('success', __('Edit the app settings successfully'))->withInput();
        } catch (\Exception $ex) {
            Log::error('Failed to edit the app settings with values = ' . json_encode($items) . '. Error message: ' . $ex->getMessage());
        }
        return back()->with('error', __('Edit failed. Please try again'))->withInput();
    }
}
