<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\User\Admin\UpdateUserService;
use App\Services\User\Admin\SingleUserService;
use App\Services\User\Admin\DeleteUserService;
use App\Services\User\Admin\ListingUserService;
use App\Http\Requests\Admin\User\StoreUserRequest;
use App\Services\User\Admin\StoreUserService;
use Illuminate\Http\Request;
use Log;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $breadcrumbs = [
            [ 'text' => __('Users') ],
        ];

        $table = [
            'title' => 'Users Overview',
            'id' => 'users-datatable',
            'ajax' => [
                'src' => 'admin.users.more', // route name
            ],
            'order_default' => [
                'column' => 'created_at',
                'order' => 'desc'
            ],
            'actions_ajax' => [
                'bulk_trash_multi' => [
                    'route' => 'admin.users.trash_multi', // route name
                    'method' => 'post'
                ],
                'delete_multi' => [
                    'route' => 'admin.users.delete_multi', // route name
                    'method' => 'post',
                ],
                'restore_multi'   => [
                    'route' => 'admin.users.restore_multi', // route name
                    'method' => 'post',
                ]
            ],
            'checkbox_column' => true,
            'columns' => [
                'id' => ['text' => '#'],
                'name_with_link' => [
                    'text' => 'Name',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'orderable' => true,
                ],
                'email' => [
                    'text' => 'Email',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'orderable' => true,
                ],
                'phone' => [
                    'text' => 'Phone',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'orderable' => true,
                ],
                'status' => [
                    'text' => 'Status',
                    'html' => '<i class="fa fa-check font-green-jungle tooltips" data-placement="top" data-original-title="Publish"></i>',
                    'filter' => [
                        'type' => 'select',
                        'data' => [
                            config('elidev.list_default_status.pending') => config('elidev.list_default_status.pending'),
                            config('elidev.list_default_status.publish') => config('elidev.list_default_status.publish'),
                        ],
                    ],
                    'orderable' => true,
                ],
                'created_at' => [ 'text' => 'Created at' ],
            ],
            'actions' => [
                'width'  => '100',
                'show' => [
                    'route' => 'admin.users.show', // route name
                    'role' => 'users.read',
                ],
                'add'    => [
                    'name'  => 'Add new',
                    'route' => 'admin.users.create', // route name
                    'icon'  => 'fa fa-fw fa-user-plus',
                    'role' => 'users.create',
                ],
                'edit'   => [
                    'route' => 'admin.users.edit', // route name
                    'role' => 'users.edit',
                ],
                'trash' => [
                    'route' => 'admin.users.trash', // route name
                    'icon' => 'fa fa-trash',
                    'color' => 'red-intense',
                    'role' => 'users.trash',
                ],
                'restore'   => [
                    'route' => 'admin.users.restore', // route name
                    'color' => 'green-jungle',
                    'role' => 'users.trash',
                ],
                'delete' => [
                    'route' => 'admin.users.destroy', // route name
                    'icon' => 'fa fa-times',
                    'color' => 'red-intense',
                    'role' => 'users.destroy',
                ],
            ],
        ];

        return view('admin.users.index', [
            'table' => $table,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $breadcrumbs = [
            [
                'url' => route('admin.users.index'),
                'text' => __('Users')
            ],
            [
                'text' => __('Create'),
            ],
        ];

        return view('admin.users.create', [
            'breadcrumbs' => $breadcrumbs,
            'list_default_status' => config('elidev.list_default_status'),
            'overrideClass' => true,
            'dataAjaxUrl' => route('admin.users.checkEmailExist')
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\User\StoreUserRequest $request
     * @param  \App\Services\User\Admin\StoreUserService $service
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUserRequest $request, StoreUserService $service)
    {
        $user = $service->execute($request);

        if ( $user ) {
            return redirect(route('admin.users.show', [ 'user' => $user->id ]))
                ->with('success', __('Create user successfully'))
                ->withInput();
        }

        return back()->with('error', __('Create failed. Please try again'))->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @param $request
     * @param $service
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, SingleUserService $service)
    {
        $request->merge(['id' => $id]);
        $user = $service->execute($request);

        if ( !$user ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.users.index'),
                'text' => __('Users')
            ],
            [
                'text' => sprintf('%s', $user->name),
            ],
        ];

        return view('admin.users.show', [
            'user'      => $user,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int $id
     * @param $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id, SingleUserService $service)
    {
        $request->merge(['id' => $id]);
        $user = $service->execute($request);

        if ( !$user ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.users.index'),
                'text' => __('Users')
            ],
            [
                'url' => route('admin.users.show', ['user' => $user->id]),
                'text' => sprintf('%s', $user->name),
            ],
            [
                'text' => __('Edit'),
            ],
        ];

        return view('admin.users.edit', [
            'user'      => $user,
            'list_default_status' => config('elidev.list_default_status'),
            'breadcrumbs' => $breadcrumbs,
            'overrideClass' => true,
            'dataAjaxUrl' => route('admin.users.checkEmailExist'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param   $request
     * @param  int $id
     * @param $service
     * @return \Illuminate\Http\Response
     */
    public function update(StoreUserRequest $request, $id, UpdateUserService $service)
    {
        $request->merge(['id' => $id]);
        $result = $service->execute($request);

        $key     = $result ? 'success' : 'error';
        $message = $result ? __('Updated successfully') : __('Update failed. Please try again');

        return back()->with($key, $message);
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int                                       $id
     * @param \App\Services\User\Admin\DeleteUserService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12893 - HFBACKEND-12 - Author - Trash/Destroy action
     */
    public function trash($id, DeleteUserService $service)
    {
        if ( $service->softDelete($id) ) {
            return redirect(route('admin.users.index'))->with('success', __('Trashed user successfully'));
        }

        Log::error(sprintf('Failed to trash the user (ID = %s).', $id));

        return back()->with('error', __('Failed to trash the user. Please try again.'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int                                       $id
     * @param \App\Services\User\Admin\DeleteUserService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12893 - HFBACKEND-12 - Author - Trash/Destroy action
     */
    public function restore($id, DeleteUserService $service)
    {
        if ( $service->restore($id) ) {
            return back()->with('success', __('Restored user successfully'));
        }

        Log::error(sprintf('Failed to restore the user (ID = %s).', $id));

        return back()->with('error', __('Failed to restore the user. Please try again.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                                       $id
     * @param \App\Services\User\Admin\DeleteUserService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12893 - HFBACKEND-12 - Author - Trash/Destroy action
     */
    public function destroy($id, DeleteUserService $service)
    {
        if ( $service->destroy($id) ) {
            return redirect(route('admin.users.index'))->with('success', __('Permanently deleted user successfully'));
        }

        Log::error(sprintf('Failed to delete the user (ID = %s) permanently.', $id));

        return back()->with('error', __('Failed to delete the user permanently. Please try again.'));
    }

    /**
     * Get authors
     *
     * @param \Illuminate\Http\Request                    $request
     * @param \App\Services\User\Admin\ListingUserService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12875 - HFBACKEND-12 - Author - Listing page
     */
    public function more(Request $request, ListingUserService $service)
    {
        $users = $service->execute($request);
        $users->getCollection()->each(function($user) {
            $data = array(
                'value' => $user->id,
                'attr' => array('data-user-id' => $user->id)
            );
            $user->checkbox = view('admin.components.checkbox', $data )->render();
        });
        $total = $users->total();

        return json_encode([
            'data'            => $users->getCollection()->toArray(),
            'recordsTotal'    => $total,
            'recordsFiltered' => $total,
        ]);
    }

    /**
     * validate email exist
     * @param Request $request
     * @param UserRepositoryInterface $repository
     * @return string
     */
    public function checkEmailExist(Request $request, UserRepositoryInterface $repository) {

        $email = $repository->checkEmailExist($request->get('email'), $request->get('user_id'));
        return json_encode([
            'data' => [
                'email' => $email,
                'message' => ($email) ? __( 'The email has already been taken') : ''
            ]
        ]);

    }


    /**
     * get profile user
     * @param Request $request
     * @param UpdateUserService $service
     * @return String
     */
    public function getUpdateProfile(Request $request, UpdateUserService $service)
    {
        $user = \Auth::user();
        return view('admin.users.update-profile', compact('user'));
    }

    /**
     * update profile current user
     * @param Request $request
     * @param UpdateUserService $service
     * @return string
     */
    public function postUpdateProfile(StoreUserRequest $request, UpdateUserService $service)
    {
        $request->merge(['id' => \Auth::user()->id, 'status' => \Auth::user()->status]);
        $result = $service->execute($request);
        if ($result) {
            return response()->success([ 'message' => 'Updated profile successfully']);
        }
        return response()->error(__('Update failed. Please try again'));
    }

    /**
     * Move multi users to trash
     * @param Request $request
     * @param DeleteUserService $service
     * @return mixed
     */
    public function trashMultiple(Request $request, DeleteUserService $service) {
        if ( $request->has('ids') ) {
            $ids = $request->get('ids');
            $result = $service->multiSoftDelete($ids);

            if ( $result ) {
                return response()->success(['message' => __(  'Trashed users successfully.' )]);
            }
        }
        return response()->error(__('Failed to trash the user. Please try again.'));
    }

    /**
     * Restore multi users
     * @param Request $request
     * @param DeleteUserService $service
     * @return mixed
     */
    public function restoreMultiple(Request $request, DeleteUserService $service) {
        if ( $request->has('ids') ) {
            $ids = $request->get('ids');
            $result = $service->multiRestore($ids);

            if ( $result ) {
                return response()->success(['message' => __(  'Restored users successfully.' )]);
            }
        }
        return response()->error(__('Failed to restore the user. Please try again.'));
    }

    /**
     * Delete permanently multi users
     * @param Request $request
     * @param DeleteUserService $service
     * @return mixed
     */
    public function deleteMultiple(Request $request, DeleteUserService $service) {
        if ( $request->has('ids') ) {
            $ids = $request->get('ids');
            $result = $service->multiDestroy($ids);

            if ( $result ) {
                return response()->success(['message' => __(  'Deleted users successfully.' )]);
            }
        }
        return response()->error(__('Failed to delete the user. Please try again.'));
    }
}
