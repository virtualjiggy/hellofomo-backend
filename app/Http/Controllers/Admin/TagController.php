<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tag\StoreTagRequest;
use App\Models\Tags;
use App\Repositories\Contracts\TagsRepositoryInterface;
use App\Utils\Utils;
use Illuminate\Http\Request;
use Log;

class TagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @ticket #12966 - HFBACKEND-13 - News - Tag menu
     */
    public function index()
    {
        $table = [
            'filter_navigation' => false,
            'title' => 'Tags Overview',
            'id' => 'tags-datatable',
            'ajax' => [
                'src' => 'admin.tags.more', // route name
            ],
            'order_default' => [
                'column' => 'name',
                'order' => 'asc'
            ],
            'columns' => [
                'name' => [
                    'text' => 'Name',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'orderable' => true,
                    'class' => 'td-text-left'
                ],
            ],
            'actions' => [
                'width'  => '70',
                'add'    => [
                    'name'  => 'Add new',
                    'route' => 'admin.tags.create', // route name
                    'role' => 'tags.create',
                ],
                'edit'   => [
                    'route' => 'admin.tags.edit', // route name
                    'role' => 'tags.edit',
                ],
                'delete' => [
                    'route' => 'admin.tags.destroy', // route name
                    'icon' => 'fa fa-times',
                    'color' => 'red-intense',
                    'role' => 'tags.destroy',
                ],
            ],
        ];

        return view('admin.tag.list', [
            'table'       => $table,
            'breadcrumbs' => [
                [
                    'url' => route('admin.news.index'),
                    'text' => __('News')
                ],
                [
                    'text' => __('Tags')
                ]
            ],
            'pageTitle'   => __('Tags'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @ticket #12966 - HFBACKEND-13 - News - Tag menu
     */
    public function create()
    {
        $breadcrumbs = [
            [
                'url' => route('admin.news.index'),
                'text' => __('News')
            ],
            [
                'url' => route('admin.tags.index'),
                'text' => __('Tags')
            ],
            [
                'text' => __('Create'),
            ],
        ];

        return view('admin.tag.create', [
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\Tag\StoreTagRequest        $request
     * @param \App\Repositories\Contracts\TagsRepositoryInterface $tagRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12966 - HFBACKEND-13 - News - Tag menu
     */
    public function store(StoreTagRequest $request, TagsRepositoryInterface $tagRepo)
    {
        $data = $request->only(['name']);
        $data['type'] = config('fomo.category.types.news');
        $tag = $tagRepo->create($data);

        $key = $tag ? 'success' : 'error';
        $message = $tag ? __('Created tag successfully') : __('Create failed. Please try again');

        return redirect(route('admin.tags.index'))->with($key, $message)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     * @ticket #12966 - HFBACKEND-13 - News - Tag menu
     */
    public function show($id)
    {
        // no implement view detail tag, use Edit page instead
        return redirect(route('admin.tags.index'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                                                $id
     * @param \App\Repositories\Contracts\TagsRepositoryInterface $tagRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12966 - HFBACKEND-13 - News - Tag menu
     */
    public function edit($id, TagsRepositoryInterface $tagRepo)
    {
        $tag = $tagRepo->find($id, ['id', 'name', 'type']);
        if ( !$tag ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.news.index'),
                'text' => __('News')
            ],
            [
                'url' => route('admin.tags.index'),
                'text' => __('Tags')
            ],
            [
                'url' => route('admin.tags.edit', ['tag' => $id]),
                'text' => $tag->name
            ],
            [
                'text' => __('Edit')
            ],
        ];

        return view('admin.tag.edit', [
            'tag'      => $tag,
            'breadcrumbs'   => $breadcrumbs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\Tag\StoreTagRequest        $request
     * @param  int                                                $id
     * @param \App\Repositories\Contracts\TagsRepositoryInterface $tagRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12966 - HFBACKEND-13 - News - Tag menu
     */
    public function update(StoreTagRequest $request, $id, TagsRepositoryInterface $tagRepo)
    {
        $data = $request->only(['name']);
        $data['type'] = config('fomo.category.types.news');
        $result = $tagRepo->update($data, $id);

        $key     = $result ? 'success' : 'error';
        $message = $result ? __('Updated successfully') : __('Update failed. Please try again');

        return redirect(route('admin.tags.index'))->with($key, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @ticket #12966 - HFBACKEND-13 - News - Tag menu
     */
    public function destroy($id)
    {
        try {
            Tags::find($id)->delete();

            return redirect(route('admin.tags.index'))->with('success', __('Permanently deleted successfully'));
        } catch(\Exception $ex) {
            Log::error(sprintf('Failed to delete the tag (ID = %s). Error message: ', $id, $ex->getMessage()));
        }

        return back()->with('error', __('Failed to delete the tag. Please try again.'));
    }

    /**
     * Get more news tags
     *
     * @param \Illuminate\Http\Request                            $request
     * @param \App\Repositories\Contracts\TagsRepositoryInterface $tagRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12966 - HFBACKEND-13 - News - Tag menu
     */
    public function more(Request $request, TagsRepositoryInterface $tagRepo)
    {
        // set filter params (name, type)
        $pagination = Utils::getDataTablePagination($request, ['name', 'type']);

        // get data
        $tags = $tagRepo->listing(
            [ 'id', 'name', 'type', 'created_at', 'updated_at' ],
            $pagination['terms'],
            $pagination['limit'],
            $pagination['sortColumn'],
            $pagination['sortOrder']
        );
        $total = $tags->total();

        return json_encode([
            'data'            => $tags->getCollection()->toArray(),
            'recordsTotal'    => $total,
            'recordsFiltered' => $total,
        ]);
    }
}
