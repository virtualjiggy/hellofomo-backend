<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Author\StoreAuthorRequest;
use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Services\Author\Admin\DeleteAuthorService;
use App\Services\Author\Admin\ListingAuthorService;
use App\Services\Author\Admin\SingleAuthorService;
use App\Services\Author\Admin\StoreAuthorService;
use App\Services\Author\Admin\UpdateAuthorService;
use Illuminate\Http\Request;
use Log;

class AuthorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     */
    public function index(CategoryRepositoryInterface $categoryRepo)
    {
        $categories = $categoryRepo->getSalutationsAndTitles();

        $table = [
            'title' => 'Authors Overview',
            'id' => 'authors-datatable',
            'ajax' => [
                'src' => 'admin.authors.more', // route name
            ],
            'order_default' => [
                'column' => 'created_at',
                'order' => 'desc'
            ],
            'checkbox_column' => true,
            'service' => 'author',
            'columns' => [
                'salutation_name' => [
                    'text' => 'Salutation',
                    'width' => '10%',
                    'filter' => [
                        'type' => 'select',
                        'data' => $categories['salutations'],
                    ],
                ],
                'title_name' => [
                    'text' => 'Title',
                    'width' => '10%',
                    'filter' => [
                        'type' => 'select',
                        'data' => $categories['titles'],
                    ],
                ],
                'first_name' => [
                    'text' => 'First name',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'orderable' => true,
                ],
                'last_name' => [
                    'text' => 'Last name',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'orderable' => true,
                ],
            ],
            'actions' => [
                'width'  => '100',
                'show'    => [
                    'route' => 'admin.authors.show', // route name
                    'role' => 'authors.read',
                ],
                'add'    => [
                    'name'  => 'Add new',
                    'route' => 'admin.authors.create', // route name
                    'role' => 'authors.add',
                ],
                'edit'   => [
                    'route' => 'admin.authors.edit', // route name
                    'role' => 'authors.edit',
                ],
                'trash' => [
                    'route' => 'admin.authors.trash', // route name
                    'icon' => 'fa fa-trash',
                    'color' => 'red-intense',
                    'role' => 'authors.trash',
                ],
                'restore'   => [
                    'route' => 'admin.authors.restore', // route name
                    'color' => 'green-jungle',
                    'role' => 'authors.trash',
                ],
                'delete' => [
                    'route' => 'admin.authors.destroy', // route name
                    'icon' => 'fa fa-times',
                    'color' => 'red-intense',
                    'role' => 'authors.destroy',
                ],
            ],
            'actions_ajax' => [
                'bulk_trash_multi' => [
                    'route' => 'admin.authors.bulk_trash_multi', // route name
                    'method' => 'post'
                ],
                'restore_multi'   => [
                    'route' => 'admin.authors.restore_multi', // route name
                    'method' => 'post',
                ],
                'delete_multi'   => [
                    'route' => 'admin.authors.delete_multi', // route name
                    'method' => 'post',
                ],
            ],
        ];

        return view('admin.author.list', [
            'table' => $table,
            'breadcrumbs' => [
                [ 'text' => __('Authors') ],
            ],
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     */
    public function create(CategoryRepositoryInterface $categoryRepo)
    {
        $categories = $categoryRepo->getSalutationsAndTitles();

        $breadcrumbs = [
            [
                'url' => route('admin.authors.index'),
                'text' => __('Authors')
            ],
            [
                'text' => __('Create'),
            ],
        ];

        return view('admin.author.create', [
            'salutations' => $categories['salutations'],
            'titles'      => $categories['titles'],
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\Author\StoreAuthorRequest $request
     * @param \App\Services\Author\Admin\StoreAuthorService      $service
     *
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAuthorRequest $request, StoreAuthorService $service)
    {
        $author = $service->execute($request);

        if ( $author ) {
            return redirect(route('admin.authors.show', [ 'author' => $author->id ]))
                ->with('success', __('Create author successfully'))
                ->withInput();
        }

        return back()->with('error', __('Create failed. Please try again'))->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param \Illuminate\Http\Request                       $request
     * @param  int                                           $id
     * @param \App\Services\Author\Admin\SingleAuthorService $service
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id, SingleAuthorService $service)
    {
        $request->merge(['id' => $id]);
        $author = $service->execute($request);

        if ( !$author ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.authors.index'),
                'text' => __('Authors')
            ],
            [
                'url' => route('admin.authors.show', ['author' => $author->id]),
                'text' => sprintf('%s %s %s %s', $author->salutation_name, $author->title_name, $author->first_name, $author->last_name),
            ],
        ];

        return view('admin.author.show', [
            'author'      => $author,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                                                    $id
     * @param \Illuminate\Http\Request                                $request
     * @param \App\Services\Author\Admin\SingleAuthorService          $service
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request, SingleAuthorService $service, CategoryRepositoryInterface $categoryRepo)
    {
        $request->merge(['id' => $id]);
        $author = $service->execute($request);

        if ( !$author ) {
            abort(404);
        }

        $categories = $categoryRepo->getSalutationsAndTitles();
        $breadcrumbs = [
            [
                'url' => route('admin.authors.index'),
                'text' => __('Authors')
            ],
            [
                'url' => route('admin.authors.show', ['author' => $author->id]),
                'text' => sprintf('%s %s %s %s', $author->salutation_name, $author->title_name, $author->first_name, $author->last_name),
            ],
            [
                'url' => route('admin.authors.edit', ['author' => $author->id]),
                'text' => __('Edit')
            ],
        ];

        return view('admin.author.edit', [
            'author'      => $author,
            'salutations' => $categories['salutations'],
            'titles'      => $categories['titles'],
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\Author\StoreAuthorRequest $request
     * @param  int                                               $id
     * @param \App\Services\Author\Admin\UpdateAuthorService     $service
     *
     * @return \Illuminate\Http\Response
     */
    public function update(StoreAuthorRequest $request, $id, UpdateAuthorService $service)
    {
        $request->merge(['id' => $id]);
        $result = $service->execute($request);

        $key     = $result ? 'success' : 'error';
        $message = $result ? __('Updated successfully') : __('Update failed. Please try again');

        return back()->with($key, $message);
    }

    /**
     * Trash the specified resource from storage.
     *
     * @param  int                                           $id
     * @param \App\Services\Author\Admin\DeleteAuthorService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12893 - HFBACKEND-12 - Author - Trash/Destroy action
     */
    public function trash($id, DeleteAuthorService $service)
    {
        if ( $service->softDelete($id) ) {
            return redirect(route('admin.authors.index'))->with('success', __('Trashed author successfully'));
        }

        Log::error(sprintf('Failed to trash the author (ID = %s)', $id));

        return back()->with('error', __('Failed to trash the author. Please try again.'));
    }

    /**
     * Restore the specified resource from storage.
     *
     * @param  int                                           $id
     * @param \App\Services\Author\Admin\DeleteAuthorService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12893 - HFBACKEND-12 - Author - Trash/Destroy action
     */
    public function restore($id, DeleteAuthorService $service)
    {
        if ( $service->restore($id) ) {
            return back()->with('success', __('Restored author successfully'));
        }

        Log::error(sprintf('Failed to restore the author (ID = %s)', $id));

        return back()->with('error', __('Failed to restore the author. Please try again.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int                                           $id
     * @param \App\Services\Author\Admin\DeleteAuthorService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12893 - HFBACKEND-12 - Author - Trash/Destroy action
     */
    public function destroy($id, DeleteAuthorService $service)
    {
        if ( $service->destroy($id) ) {
            return redirect(route('admin.authors.index'))->with('success', __('Permanently deleted author successfully'));
        }

        Log::error(sprintf('Failed to delete the author (ID = %s) permanently.', $id));

        return back()->with('error', __('Failed to delete the author permanently. Please try again.'));
    }

    /**
     * Get authors
     *
     * @param \Illuminate\Http\Request                                $request
     * @param \App\Services\Author\Admin\ListingAuthorService         $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #12875 - HFBACKEND-12 - Author - Listing page
     */
    public function more(Request $request, ListingAuthorService $service)
    {
        $authors = $service->execute($request);
        $authors->getCollection()->each(function($item) {
            $data = array(
                'value' => $item->id,
                'attr' => array('data-author-id' => $item->id)
            );
            $item->checkbox = view('admin.components.checkbox', $data )->render();
        });

        $total = $authors->total();

        return json_encode([
            'data'            => $authors->getCollection()->toArray(),
            'recordsTotal'    => $total,
            'recordsFiltered' => $total,
        ]);
    }

    /**
     * Search authors
     *
     * @param \Illuminate\Http\Request                              $request
     * @param \App\Repositories\Contracts\AuthorRepositoryInterface $authorRepo
     *
     * @return json
     */
    public function search(Request $request, AuthorRepositoryInterface $authorRepo)
    {
        $authors = $authorRepo->search(['id', 'first_name', 'last_name', 'display_name'], $request->get('q', ''));
        $totalAuthors = $authors->total();
        $authors = $authors->map(function($author) {
            return [
                'id' => $author->id,
                'text' => $author->display_name,
            ];
        })->toArray();

        return json_encode([
            'items'       => $authors,
            'total_count' => $totalAuthors,
        ]);
    }

    /**
     * trash multi author
     *
     * @param \Illuminate\Http\Request                                $request
     * @param \App\Services\Author\Admin\DeleteAuthorService         $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #13143 - HFBACKEND-12 - Author - Multiple deleting
     */
    public function trashMulti(Request $request, DeleteAuthorService $service)
    {
        if($request->has('ids')) {
            $ids = $request->input('ids');
            $result = $service->multiSoftDelete($ids);
            if ( $result ) {
                return response()->success([ 'message' => __('Trashed authors successfully') ]);
            }
        }
        return response()->error(__('Failed to trash the author. Please try again.'));
    }

    /**
     * restore multi author
     *
     * @param \Illuminate\Http\Request                                $request
     * @param \App\Services\Author\Admin\DeleteAuthorService         $service
     *
     * @return \Illuminate\Http\Response
     */
    public function restoreMulti(Request $request, DeleteAuthorService $service)
    {
        if($request->has('ids')) {
            $ids = $request->input('ids');
            $result = $service->multiRestore($ids);
            if ( $result ) {
                return response()->success([ 'message' => __('Restored authors successfully') ]);
            }
        }
        return response()->error(__('Failed to restore the author. Please try again.'));
    }

    /**
     * delete multi author
     *
     * @param \Illuminate\Http\Request                                $request
     * @param \App\Services\Author\Admin\DeleteAuthorService         $service
     *
     * @return \Illuminate\Http\Response
     */
    public function deleteMulti(Request $request, DeleteAuthorService $service)
    {
        if($request->has('ids')) {
            $ids = $request->input('ids');
            $result = $service->multiDestroy($ids);
            if ( $result ) {
                return response()->success([ 'message' => __('Delete authors successfully') ]);
            }
        }
        return response()->error(__('Failed to delete the author. Please try again.'));
    }
}
