<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OAuthClientController extends Controller
{
    public function index()
    {
        if ( in_array(config('app.env'), ['local', 'dev', 'test', 'testing']) ) {
            $client = \DB::table('oauth_clients')
                ->where('personal_access_client', '=', 0)
                ->where('password_client', '=', 0)
                ->where('revoked', '=', 0)
                ->first();
        }

        return view('admin.oauth-client.index', compact('client'));
    }
}
