<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\TagsRepositoryInterface;
use Illuminate\Http\Request;
use Log;
use Settings;

class AppLayoutController extends Controller
{
    /**
     * Display layout dashboard settings page. This page is used for settings 2 types of dashboard
     * 1. App dashboard
     * 2. App News dashboard
     *
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     * @param \App\Repositories\Contracts\TagsRepositoryInterface     $tagRepo
     * @param string                                                  $settingName
     *
     * @return \Illuminate\Http\Response
     * @ticket #13213 - HFBACKEND-14 - App / Layout Dashboard - Setting layout
     * @ticket #13291 - HFBACKEND-80 App / Layout News Dashboard - UI concept
     */
    public function getLayoutSettings(CategoryRepositoryInterface $categoryRepo, TagsRepositoryInterface $tagRepo, $settingName = '')
    {
        // invalid setting page name
        if ( empty($settingName) || !in_array($settingName, ['dashboard', 'news']) ) {
            abort(404);
        }

        // get data
        $categories = $categoryRepo->getNewsCategories()->pluck('name', 'id')->all();
        $tags       = $tagRepo->getNewsTags()->pluck('name', 'id')->all();

        // determine app dashboard layout or app news layout to get proper layout settings
        $layoutSettings = [
            'dashboard' => ['key' => config('fomo.app_settings.app_layout_dashboard'), 'title' => __('Layout Dashboard Settings')],
            'news'      => ['key' => config('fomo.app_settings.app_layout_news'), 'title' => __('Layout News Settings')],
        ];
        $settingValue = Settings::get($layoutSettings[$settingName]['key'], []);

        return view('admin.app.layout.dashboard', [
            'categories'     => $categories,
            'tags'           => $tags,
            'layoutSettings' => $settingValue,
            'title'          => $layoutSettings[$settingName]['title'],
            'breadcrumbs' => [
                [ 'text' => __('App') ],
                [ 'text' => __('Layout') ],
                [ 'text' => __(ucfirst($settingName)) ],
            ],
        ]);
    }

    /**
     * Save app dashboard layout settings to DB. This page is used for settings 2 types of dashboard
     * 1. App dashboard
     * 2. App News dashboard
     *
     * @param \Illuminate\Http\Request $request
     * @param string                   $settingName
     *
     * @return \Illuminate\Http\Response
     * @ticket #13227 - HFBACKEND-14 - App / Layout Dashboard - Handle saving setting
     * @ticket #13291 - HFBACKEND-80 App / Layout News Dashboard - UI concept
     */
    public function saveLayoutSettings(Request $request, $settingName = '')
    {
        // invalid setting page name
        if ( empty($settingName) || !in_array($settingName, ['dashboard', 'news']) ) {
            response()->error(__('Invalid settings name. Please try again'));
        }

        $settingKey = ($settingName == 'dashboard') ? config('fomo.app_settings.app_layout_dashboard') : config('fomo.app_settings.app_layout_news');
        $settingValue = [];

        // also store if user has deleted all items
        $sectionType = $request->get('section_type', []);
        if ( ($length = count($sectionType)) == 0 ) {
            Settings::set($settingKey, $settingValue);
            return response()->success(['message' => __('Layout settings were saved successfully')]);
        }

        try {
            $sectionDesc = $request->get('section_description');
            $layout      = $request->get('layout');
            $showPicture = $request->get('show_picture');
            $type        = $request->get('type');
            $typeValue   = $request->get('type_value');
            $dataCount   = $request->get('data_count');
            $dataOffset  = $request->get('data_offset');
            $ordering    = $request->get('ordering');

            for($i = 0; $i < $length; $i++) {
                $settingValue[$i] = [
                    'section_name'        => ucfirst($sectionType[$i]) . ' ' . ucfirst($layout[$i]),
                    'section_description' => $sectionDesc[$i],
                    'section_type'        => $sectionType[$i],
                    'layout'              => $layout[$i],
                    'show_picture'        => $showPicture[$i],
                    'type'                => $type[$i],
                    'type_value'          => $type[$i] != 'all' && $request->has("type_value_$i") ? $request->get("type_value_$i"): '', // when choose Data Type is "All" -> store empty filter value
                    'data_count'          => $dataCount[$i],
                    'data_offset'         => $dataOffset[$i],
                    'ordering'            => $ordering[$i],
                ];
            }

            Settings::set($settingKey, $settingValue);

            return response()->success(['message' => __('Layout settings were saved successfully')]);
        } catch(\Exception $ex) {
            Log::error('Failed to save settings. Error message: ' . $ex->getMessage());
        }
        return response()->error(__('Failed to save layout settings. Please try again'));
    }

    /**
     * Display/Save app layout settings page
     *
     * @param Request $request
     * @param $settingName
     * @return \Illuminate\Http\Response
     * @ticket #13243 - HFBACKEND-76 APP / Layout - Contact
     */
    public function settingContent(Request $request, $settingName)
    {
        $arrLayoutSetting = [
            'contact'   => ['setting_key' => config('fomo.app_settings.app_layout_contact')  , 'label' => __('Contact')],
            'privacy'   => ['setting_key' => config('fomo.app_settings.app_layout_privacy')  , 'label' => __('Privacy')],
            'impressum' => ['setting_key' => config('fomo.app_settings.app_layout_impressum'), 'label' => __('Impressum')],
            'agb'       => ['setting_key' => config('fomo.app_settings.app_layout_agb')      , 'label' => __('AGB')],
        ];

        if ( array_key_exists($settingName, $arrLayoutSetting) ) {
            $settingKey = $arrLayoutSetting[$settingName]['setting_key'];
            $settingLabel = $arrLayoutSetting[$settingName]['label'];

            $breadcrumbs = [
                [
                    'url' => '#',
                    'text' => __('App')
                ],
                [
                    'url' => '#',
                    'text' => __('Layout')
                ],
                [
                    'text' => __($settingLabel),
                ],
            ];

            if ($request->isMethod('post')) {
                $settingValue = $request->input('value');
                try {
                    Settings::set($settingKey, $settingValue);
                    $request->session()->flash('success', __('Updated successfully'));
                } catch(\Exception $ex) {
                    $request->session()->flash('error', __('Update failed. Please try again'));
                    Log::error('Failed to save settings. Error message: ' . $ex->getMessage());
                }
            }

            $settingValue = Settings::get($settingKey);
            return view('admin.app.layout.setting_content', compact('breadcrumbs', 'settingValue', 'settingLabel'));
        } else {
            return redirect()->route('admin.dashboard');
        }
    }
}
