<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ComponentController extends Controller
{

    /**
     * Role management
     *
     * @auth vulh
     *
     * @return view
     */
    public function role()
    {
        return view('admin.components.role');
    }

    /**
     * Media management
     *
     * @auth vulh
     *
     * @return view
     */
    public function media()
    {
        return view('admin.components.media', [
            'breadcrumbs'=> [
                [
                    'text' => __('File Management')
                ],
            ]
        ]);
    }
}
