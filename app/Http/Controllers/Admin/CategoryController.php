<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreCategoryRequest;
use App\Models\Category;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Utils\Utils;
use Illuminate\Http\Request;
use Log;
use Illuminate\Support\Facades\File;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     * @ticket #12965 - HFBACKEND-13 - News - Category menu
     */
    public function index()
    {
        $table = [
            'filter_navigation' => false,
            'title' => 'News Categories Overview',
            'id' => 'news-categories-datatable',
            'ajax' => [
                'src' => 'admin.categories.more', // route name
            ],
            'order_default' => [
                'column' => 'name',
                'order' => 'asc'
            ],
            'columns' => [
                'name' => [
                    'text' => 'Name',
                    'filter' => [
                        'type' => 'input',
                    ],
                    'orderable' => true,
                    'class' => 'td-text-left'
                ],
            ],
            'actions' => [
                'width'  => '70',
                'add'    => [
                    'name'  => 'Add new',
                    'route' => 'admin.categories.create', // route name
                    'role' => 'categories.create',
                ],
                'edit'   => [
                    'route' => 'admin.categories.edit', // route name
                    'role' => 'categories.edit',
                ],
                'delete' => [
                    'route' => 'admin.categories.destroy', // route name
                    'icon' => 'fa fa-times',
                    'color' => 'red-intense',
                    'role' => 'categories.destroy',
                ],
            ],
        ];

        return view('admin.category.list', [
            'table'       => $table,
            'breadcrumbs' => [
                [
                    'url' => route('admin.news.index'),
                    'text' => __('News')
                ],
                [
                    'text' => __('Categories')
                ]
            ],
            'pageTitle'   => __('News categories'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     * @ticket #12965 - HFBACKEND-13 - News - Category menu
     */
    public function create()
    {
        $breadcrumbs = [
            [
                'url' => route('admin.news.index'),
                'text' => __('News')
            ],
            [
                'url' => route('admin.categories.index'),
                'text' => __('Categories')
            ],
            [
                'text' => __('Create'),
            ],
        ];

        return view('admin.category.create', [
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\Admin\Category\StoreCategoryRequest  $request
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12965 - HFBACKEND-13 - News - Category menu
     */
    public function store(StoreCategoryRequest $request, CategoryRepositoryInterface $categoryRepo)
    {
        File::delete(get_categories_caching_file());
        $category = $categoryRepo->create($request->only([ 'name', 'type' ]));

        $key = $category ? 'success' : 'error';
        $message = $category ? __('Created category successfully') : __('Create failed. Please try again');

        return redirect(route('admin.categories.index'))->with($key, $message)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int                                                    $id
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12965 - HFBACKEND-13 - News - Category menu
     */
    public function show($id, CategoryRepositoryInterface $categoryRepo)
    {
        $category = $categoryRepo->getNewsCategoryDetail($id);
        if ( !$category ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.news.index'),
                'text' => __('News')
            ],
            [
                'url' => route('admin.categories.index'),
                'text' => __('Categories')
            ],
            [
                'text' => $category->name,
            ],
        ];

        return view('admin.category.show', [
            'category' => $category,
            'breadcrumbs' => $breadcrumbs,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int                                                    $id
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12965 - HFBACKEND-13 - News - Category menu
     */
    public function edit($id, CategoryRepositoryInterface $categoryRepo)
    {
        $category = $categoryRepo->find($id, ['id', 'name', 'type']);
        if ( !$category ) {
            abort(404);
        }

        $breadcrumbs = [
            [
                'url' => route('admin.news.index'),
                'text' => __('News')
            ],
            [
                'url' => route('admin.categories.index'),
                'text' => __('Categories')
            ],
            [
                'url' => route('admin.categories.show', ['category' => $id]),
                'text' => $category->name
            ],
            [
                'text' => __('Edit')
            ],
        ];

        return view('admin.category.edit', [
            'category'      => $category,
            'breadcrumbs'   => $breadcrumbs,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\Admin\Category\StoreCategoryRequest  $request
     * @param                                                         $id
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12965 - HFBACKEND-13 - News - Category menu
     */
    public function update(StoreCategoryRequest $request, $id, CategoryRepositoryInterface $categoryRepo)
    {
        $result = $categoryRepo->update($request->only(['name', 'type']), $id);

        $key     = $result ? 'success' : 'error';
        $message = $result ? __('Updated successfully') : __('Update failed. Please try again');

        File::delete(get_categories_caching_file());

        return redirect(route('admin.categories.index'))->with($key, $message);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     * @ticket #12965 - HFBACKEND-13 - News - Category menu
     */
    public function destroy($id)
    {
        try {
            Category::find($id)->delete();
            File::delete(get_categories_caching_file());
            return redirect(route('admin.categories.index'))->with('success', __('Permanently deleted successfully'));
        } catch(\Exception $ex) {
            Log::error(sprintf('Failed to delete the category (ID = %s). Error message: %s', $id, $ex->getMessage()));
        }

        return back()->with('error', __('Failed to delete the category. Please try again.'));
    }

    /**
     * Get more news categories
     *
     * @param \Illuminate\Http\Request                                $request
     * @param \App\Repositories\Contracts\CategoryRepositoryInterface $categoryRepo
     *
     * @return \Illuminate\Http\Response
     * @ticket #12965 - HFBACKEND-13 - News - Category menu
     */
    public function more(Request $request, CategoryRepositoryInterface $categoryRepo)
    {
        // set filter params (name, type)
        $pagination = Utils::getDataTablePagination($request, ['name']);
        $pagination['terms']['type'] = config('fomo.category.types.news');

        // get data
        $categories = $categoryRepo->listing(
            [ 'id', 'name', 'type', 'created_at', 'updated_at' ],
            $pagination['terms'],
            $pagination['limit'],
            $pagination['sortColumn'],
            $pagination['sortOrder']
        );
        $total = $categories->total();

        return json_encode([
            'data'            => $categories->getCollection()->toArray(),
            'recordsTotal'    => $total,
            'recordsFiltered' => $total,
        ]);
    }
}
