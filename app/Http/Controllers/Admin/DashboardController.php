<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Repositories\Contracts\AuthorRepositoryInterface;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\NewsAuthorsRepositoryInterface;
use App\Repositories\Contracts\NewsRepositoryInterface;
use Botble\Media\Models\MediaFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     *
     * @param NewsRepositoryInterface $nr
     * @param NewsAuthorsRepositoryInterface $naR
     * @param AuthorRepositoryInterface $ar
     * @return mixed
     * @ticket #12850 - integrating Admin LTE
     */
    public function index(Request $request, NewsRepositoryInterface $nr, NewsAuthorsRepositoryInterface $naR, AuthorRepositoryInterface $ar, CategoryRepositoryInterface $catRepo)
    {

        if (isset($_GET['fomo_checking'])) {
            $news = DB::table('news')->get();
            foreach ($news as $n) {
                $date = $n->available_date ? date('Y-m-d', strtotime($n->available_date)) : '0000-00-00';
                $cn = DB::table('tbl_news')->where('row_id', $n->id)->get();

                if (count($cn)) {
                    $cn = $cn[0];
                }

                if ($date != $cn->date) {
                    var_dump("Old Version = ". $cn->date);
                    var_dump("New Version = ". $date);
                    dd($cn);
                }
            }

            foreach ($news as $n) {
                $newsAuthors1 = DB::table('news_authors')->where('news_id', $n->id)->get(['author_id'])->pluck('author_id')->toArray();
                $newsAuthors2 = DB::table('tbl_news_author')->where('news_id', $n->id)->get(['author_id'])->pluck('author_id')->toArray();
                if (array_diff($newsAuthors1, $newsAuthors2)) {
                    dd($newsAuthors1, $newsAuthors2);
                }
            }
        }


        // ==================================================================================================
        if (isset($_GET['clonedb']) && $_GET['clonedb'] = '!@#Whatever123dd') {
            // Insert news

            if (isset($_GET['segment1'])) {

                $news = DB::table('tbl_news')->get();
                foreach($news as $i => $n) {

                    $data = [
                        'id'    => $n->row_id,
                        'title' => $n->headline,
                        'description' => $n->content_html,
                        'description' => $n->content_html,
                        'introduction' => $n->content_text,
                        'available_date'    => $n->date == '0000-00-00' ? '' : $n->date. ' 00:00:00',
                        'status'    => 'publish',
                    ];

                    if (!$n->date || $n->date == '0000-00-00') {
                        unset($data['available_date']);
                    }


                    $nr->create($data);

                    $newsFiles = DB::table('tbl_news_files')->where('news_id', $n->row_id)->get();
                    $fileIDs = [];
                    foreach($newsFiles as $f) {
                        $media = MediaFile::create([
                            'user_id'   => 1,
                            'name'   => $f->file_name,
                            'folder_id'   => 1,
                            'mime_type'   => $f->file_type,
                            'size'   => $f->file_size,
                            'url'   => str_replace('../../uploads/customers_obligations_files/client_customer_id_/obligations_id_/', '/uploads/1/nachrichten/', $f->file_path),
                            'is_public' => 1
                        ]);
                        $fileIDs[] = $media->id;
                    }

                    if (count($fileIDs)) {
                        $nr->update([
                            'attachment_ids'    => serialize($fileIDs)
                        ], $n->row_id);
                    }

                }
            }


            // ===============================================================

            if (isset($_GET['segment2'])) {
                $authors = DB::table('tbl_author')->get();
                foreach($authors as $n) {
                     $ar->create([
                        'id'    => $n->row_id,
                        'first_name' => $n->first_name,
                        'last_name' => $n->last_name,
                        'salutation_id' => 1
                    ]);
                }

                $newsAuthors = DB::table('tbl_news_author')->get();
                foreach($newsAuthors as $n) {

                    if ($nr->find($n->news_id)) {
                        $naR->create([
                            'news_id'   => $n->news_id,
                            'author_id' => $n->author_id
                        ]);
                    }
                }
            }

        }

        return view('admin.dashboard');
    }
}
