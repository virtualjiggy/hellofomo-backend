<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\FrontendStoreUserRequest;
use App\Repositories\Contracts\UserRepositoryInterface;
use App\Services\User\Frontend\UpdateUserService;
use App\Utils\Utils;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Auth;
use Storage;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    /**
     * @var \App\Repositories\Contracts\UserRepositoryInterface
     */
    protected $repo;

    /**
     * UserController constructor.
     *
     * @param \App\Repositories\Contracts\UserRepositoryInterface $repo
     */
    public function __construct(UserRepositoryInterface $repo)
    {
        $this->repo = $repo;
    }

    /**
     * Search users based on name or email
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $request->page = 1;
        $users = $this->repo->searchByNameOrEmail(['id', 'name'], $request->get('term'), 'name');

        $selectedUsers = [];
        if ($request->has('selectedUsers') && !empty($request->get('selectedUsers')) && $request->get('type') == 'junior') {
            $selectedUsers = explode(',', $request->get('selectedUsers'));
        }

        $html = view('user.company-search.list', [
            'users' => $users->getCollection(),
            'selectedUsers' => $selectedUsers,
        ])->render();

        return response()->success($html);
    }

    public function checkEmailExisted(Request $request)
    {
        $email = $request->get('email');
        $users = $this->repo
            ->getByEmail($email);
        $check = count($users) > 0 ? true : false;

        if (Auth::user() && $email == Auth::user()->email) {
            return response()->success([
                'existed' => false
            ]);
        }
        return response()->success([
            'existed' => $check
        ]);
    }

    /**
     * Finish profile page
     * @param FrontendStoreUserRequest $request
     * @param UpdateUserService $service
     * @return JsonResponse
     */
    public function finishProfile(FrontendStoreUserRequest $request, UpdateUserService $service)
    {
        $user = $service->execute($request);
        if (!$user) {
            return response()->error(__('Bad Request - Update user.'), 1002);
        }

        // convert country code -> country name
        $countries = collect(json_decode(File::get(public_path() . '/countries.json'), true));
        $user->country_name = Utils::getCountryName($countries, $user->country);

        $data = Arr::only(
            $user->toArray(),
            ['id', 'name', 'email', 'phone', 'country', 'country_name', 'city', 'language', 'status', 'deleted_at', 'roleName', 'avatar']
        );

        return response()->success(array_merge($data, ['redirect' => $request->input('_redirect')]));
    }
}
