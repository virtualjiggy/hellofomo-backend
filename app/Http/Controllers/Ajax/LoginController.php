<?php

namespace App\Http\Controllers\Ajax;

use App\Factories\User\ActivationFactory;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    /**
     * User Activation
     *
     * @param string $token
     * @param ActivationFactory $activationFactory
     * @return mixed
     */
    public function activateUser($token, ActivationFactory $activationFactory)
    {
        if ($user = $activationFactory->activateUser($token)) {
            auth()->login($user);
            return redirect($this->redirectPath());
        }
        abort(404);
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return bool
     */
    public function attemptLogin(Request $request)
    {
        $input = $request->input();
        $redirectUrl = isset($input['redirected_url']) && !empty($input['redirected_url']) ? $input['redirected_url'] : $this->redirectPath();

        if (Auth::attempt([
            'email' => $input['email'],
            'password' => $input['password'],
            'status' => 'publish',
        ], $request->input('remember', 0))
        ) {
            return response()->success([
                'message' => __('Login success'),
                'redirectUrl' => $redirectUrl,
            ]);
        } else {
            return response([
                'message' => __('Login failed, please check your login information or account is not activated'),
            ], 422);
        }
    }

    public function redirectTo()
    {
        return route('/');
    }

}
