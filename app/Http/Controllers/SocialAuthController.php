<?php

namespace App\Http\Controllers;

use App\Services\User\Frontend\SocialAccountService;
use App\Utils\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Auth;


class SocialAuthController extends Controller
{
    /**
     * Redirect to facebook
     *
     * @author vulh
     * @return Socialite
     */
    public function redirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Handle facebook callback
     *
     * @param \Illuminate\Http\Request $request
     * @param SocialAccountService     $service
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function callback(Request $request, SocialAccountService $service)
    {
        if ( $request->has('error') ) {
            // log error
            Log::error(
                'Failed to login by Facebook.',
                Utils::formatLog([
                    'error'             => $request->get('error'),
                    'error_code'        => $request->get('error_code'),
                    'error_description' => $request->get('error_description'),
                    'error_reason'      => $request->get('error_reason'),
                    'state'             => $request->get('state'),
                ], __LINE__, __FUNCTION__, __CLASS__)
            );
        } else {
            $user = $service->createOrGetUser(Socialite::driver('facebook')->user());
            Auth::login($user);
        }

        return redirect()->to(action('DeedController@getUserDeeds'));
    }
}
