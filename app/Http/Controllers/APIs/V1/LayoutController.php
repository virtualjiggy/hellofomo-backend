<?php
/**
 * Created by PhpStorm.
 * User: hieu
 * Date: 07/08/2017
 * Time: 16:00
 */

namespace App\Http\Controllers\APIs\V1;

use Settings;

class LayoutController
{
    /**
     * Get content layout contact
     *
     * @return mixed
     */
    public function getContentContact()
    {
        return response()->success(Settings::get(config('fomo.app_settings.app_layout_contact')));
    }

    /**
     * Get content layout privacy
     *
     * @return mixed
     */
    public function getContentPrivacy()
    {
        return response()->success(Settings::get(config('fomo.app_settings.app_layout_privacy')));
    }

    /**
     * Get content layout impressum
     *
     * @return mixed
     */
    public function getContentImpressum()
    {
        return response()->success(Settings::get(config('fomo.app_settings.app_layout_impressum')));
    }

    /**
     * Get content layout AGB
     *
     * @return \Illuminate\Http\Response
     * @ticket #13290 - HFBACKEND-81 APP / Layout - AGB
     */
    public function getContentAGB()
    {
        return response()->success(Settings::get(config('fomo.app_settings.app_layout_agb')));
    }
}
