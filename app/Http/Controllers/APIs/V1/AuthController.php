<?php

namespace App\Http\Controllers\APIs\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\APIs\V1\MobileAuthRequest;
use App\Http\Requests\APIs\V1\RefreshTokenRequest;
use App\Services\Device\DeviceRefreshTokenService;
use App\Services\Device\DeviceRegisterService;
use Log;

class AuthController extends Controller
{
    /**
     * Register new mobile device and return access token
     *
     * @param \App\Http\Requests\APIs\V1\MobileAuthRequest $request
     * @param \App\Services\Device\DeviceRegisterService   $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #13199 - HFBACKEND-53 - Auth by token
     */
    public function login(MobileAuthRequest $request, DeviceRegisterService $service)
    {
        try {
            $result = $service->execute($request);
            if ( !$result ) {
                return response()->error($service->getErrorMessage(), $service->getErrorCode());
            }

            return $result;
        } catch(\Exception $ex) {
            Log::error('Failed to register mobile device. Error: ' . $ex->getMessage());
        }

        return response()->error(__('Failed to authenticate the mobile device'), 1010);
    }

    /**
     * Refresh the access token
     *
     * @param \App\Http\Requests\APIs\V1\RefreshTokenRequest $request
     * @param \App\Services\Device\DeviceRefreshTokenService $service
     *
     * @return \Illuminate\Http\Response
     * @ticket #13217 - HFBACKEND-53 - Refresh token
     */
    public function refreshToken(RefreshTokenRequest $request, DeviceRefreshTokenService $service)
    {
        $result = $service->execute($request);
        if ( !$result ) {
            return response()->error($service->getErrorMessage(), $service->getErrorCode());
        }

        return $result;
    }
}
