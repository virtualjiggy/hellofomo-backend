<?php

namespace App\Http\Controllers\APIs\V1;

use App\Http\Requests\APIs\V1\ListingNewsRequest;
use App\Http\Requests\APIs\V1\SingleNewsRequest;
use App\Services\News\API\ListingNewsService;
use App\Services\News\API\SingleNewsService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ListingNewsRequest $request
     * @param ListingNewsService $service
     * @return \Illuminate\Http\Response
     */
    public function index(ListingNewsRequest $request, ListingNewsService $service)
    {
        $news = $service->execute($request);
        return \Response::json([
            'errors' => false,
            'data' => isset($news['data']) ? $news['data'] : null,
            'has_more' => isset($news['has_more']) ? $news['has_more'] : false,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Response detail news
     *
     * @param SingleNewsRequest $request
     * @param int $id
     * @param SingleNewsService $service
     * @return \Illuminate\Http\Response
     */
    public function show(SingleNewsRequest $request, $id, SingleNewsService $service)
    {
        $request->merge(['id' => $id]);
        $news = $service->execute($request);
        return response()->success($news, __('The news not exist'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}