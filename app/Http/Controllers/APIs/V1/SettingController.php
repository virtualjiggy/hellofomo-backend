<?php

namespace App\Http\Controllers\APIs\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\CategoryRepositoryInterface;
use App\Repositories\Contracts\TagsRepositoryInterface;
use Settings;

class SettingController extends Controller
{
    /**
     * Get current font in Admin Panel
     *
     * @return mixed
     * @ticket #13182 - HFBACKEND-39 - App / possibility to select name of font family. This list is hardcoded
     */
    public function getCurrentFont()
    {
        return response()->success(Settings::get(config('fomo.app_settings.font'), __('The font is not set')));
    }

    /**
     * Get current layout dashboard in Admin Panel
     *
     * @return mixed
     */
    public function getCurrentLayoutDashboard()
    {
        $data = Settings::get(config('fomo.app_settings.app_layout_dashboard'));
        return response()->success($data, __('The layout dashboard is not set'));
    }

    /**
     * Get current layout news dashboard in Admin Panel
     *
     * @return \Illuminate\Http\Response
     * @ticket #13291 - HFBACKEND-80 App / Layout News Dashboard - UI concept
     */
    public function getCurrentLayoutNewsDashboard()
    {
        $data = Settings::get(config('fomo.app_settings.app_layout_news'));
        return response()->success($data, __('The layout news is not set'));
    }
}
