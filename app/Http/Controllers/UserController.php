<?php

namespace App\Http\Controllers;

use App\Http\Requests\User\FrontendStoreUserRequest;
use App\Repositories\Contracts\GeneralTokenRepositoryInterface;
use App\Services\User\Frontend\SingleUserService;
use App\Services\User\Frontend\UpdateUserService;
use App\Utils\Utils;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Auth;
use Settings;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param  integer $id
     * @param \App\Services\User\Frontend\SingleUserService $service
     *
     * @ticket #11770
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id, SingleUserService $service)
    {
        // only allow admin or current user edit profile, not allow other's user edit current user's profile
        if (Auth::user()->id != $id && !$request->user()->hasRole('Administrator')) {
            abort(404);
        }

        $request->id = $id;
        $user = $service->execute($request);
        if (is_null($user)) {
            abort(404);
        }

        return view('user.edit', [
            'user' => $user,
            'languages' => json_decode(File::get(public_path() . '/languages.json'), true),
            'countries' => json_decode(File::get(public_path() . '/countries.json'), true),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\User\FrontendStoreUserRequest $request
     * @param  integer $id
     * @param \App\Services\User\Frontend\UpdateUserService $service
     *
     * @ticket #11770
     * @return \Illuminate\Http\Response
     */
    public function update(FrontendStoreUserRequest $request, $id, UpdateUserService $service)
    {

        if (!ctype_digit($id)) {
            abort(404);
        }

        // only allow admin or current user edit profile, not allow other's user edit current user's profile
        if (Auth::user()->id != $id && !$request->user()->hasRole('Administrator')) {
            abort(404);
        }

        $request->id = $id;
        $user = $service->execute($request);
        if (!$user) {
            return back()->withErrors(['msg', $service->errorMessage])->withInput();;
        }

        return back()->with('success', __('Updated user profile successfully!'))->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     *
     * Load user profile form
     *
     * @param Request $request
     * @param SingleUserService $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * @author Trilm
     *
     */
    public function userProfile(Request $request, SingleUserService $service)
    {
        // only allow admin or current user edit profile, not allow other's user edit current user's profile

        $request->id = Auth::user()->id;
        $user = $service->execute($request);
        if (is_null($user)) {
            abort(404);
        }

        if (request()->ajax()) {
            return response()->success(['message' => __('Updated user profile successfully!')]);
        }

        return view('user.edit', [
            'user' => $user,
            'languages' => json_decode(File::get(public_path() . '/languages.json'), true),
            'countries' => json_decode(File::get(public_path() . '/countries.json'), true),
        ]);
    }


    /**
     *
     * Thank you for signing up page
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     *
     * @author TriLm
     *
     */
    public function thankYou()
    {
        if (Auth::user() && Auth::user()->id) {
            return redirect('/');
        }
        return view('user.thankyou', [
            'thankYou' => ''
        ]);
    }


    /**
     * Form update personal profile ( after click link active account from activation email )
     *
     * @param Request $request
     * @param SingleUserService $service
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     *
     * @author TriLm
     *
     */
    public function signupPersonal(
        Request $request,
        SingleUserService $service
    )
    {

        if (!Auth::user()->id) {
            abort(404);
        }
        $request->id = Auth::user()->id;
        $user = $service->execute($request);
        return view('user.signupPersonal', [
                'user' => $user,
                'languages' => json_decode(File::get(public_path() . '/languages.json'), true),
                'countries' => json_decode(File::get(public_path() . '/countries.json'), true),
            ]
        );
    }

    /**
     *
     * Update personal profile action
     *
     * @param FrontendStoreUserRequest $request
     * @param UpdateUserService $service
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     *
     * @author TriLm
     *
     */
    public function updatePersonalProfile(FrontendStoreUserRequest $request, UpdateUserService $service)
    {
        $user = $service->execute($request);
        if (!$user) {
            return response()->error(__('Bad Request - Update user.'), 1002);
        }

        // convert country code -> country name
        $countries = collect(json_decode(File::get(public_path() . '/countries.json'), true));
        $user->country_name = Utils::getCountryName($countries, $user->country);

        $data = Arr::only(
            $user->toArray(),
            ['id', 'name', 'email', 'phone', 'country', 'country_name', 'city', 'language', 'status', 'deleted_at', 'roleName', 'avatar']
        );

        return response()->success(array_merge($data, ['message' => __('Updated user profile successfully!')]));
    }
}
