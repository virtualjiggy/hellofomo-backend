<?php
namespace App\Http\ViewComposers\Admin;

use Illuminate\Support\Facades\Session;
use Illuminate\View\View;
use Auth;
use JavaScript;
use RvMedia;


class InitComposer
{

    /**
     * Except views
     */
    protected $exceptViews = ['admin.components.checkbox'];

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $user = Auth::user();
        if (!$user || isset($view['permissions'])) {
            return;
        }

        if (in_array($view->getName(), $this->exceptViews)) {
            return;
        }

        if (! $userPermissions = Session::get('permissions')) {
            $userPermissions = $user->getAllPermissions()->pluck('name')->toArray();
            Session::put('permissions', $userPermissions);
        }


        JavaScript::put(['permissions'  => $userPermissions]);
        //JavaScript::put(['is_full_permission'   => $user->hasRole(config('acl.group_with_full_permissions'))]);

        $mediaPermissions = array_intersect($userPermissions, config('media.permissions'));
        RvMedia::setPermissions($mediaPermissions);

        JavaScript::put([
            'acl_msgs' => [
                'warning_form_msg'  => __("You do not have permission to update some fields in this form")
            ]
        ]);

        $view->with('permissions', $userPermissions);
    }
}