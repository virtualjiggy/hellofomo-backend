<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Exception;

class ACLRoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (!$request->user()) {
            return redirect(route('admin.login'));
        }

        $roles = $request->user()->roles;

        // Check permission
        $aclMapAction = config('acl.map_actions');
        $aclMapMod = config('acl.map_mods');
        $routeName = $request->route()->getName();

        // Pass free permissions
        if (acl_is_free_permission($routeName)) {
            return $next($request);
        }

        try {
            list(, $mod, $action) = explode('.', $routeName);
        }
        catch(Exception $e) {
            list($mod, $action) = explode('.', $routeName);
        }

        // Check action from media package
        if (strpos($routeName, 'media') !== false && $_action = $request->get('action')) {
            $action = $_action;
        }

        try {

            // Map mod from other package
            if (isset($aclMapMod[$mod])) {
                $mod = $aclMapMod[$mod];
            }

            // there is no mod "folders" in $aclMapMod variable, so assign manually here
            $data = $request->all();
            $isFolder = array_get($data, 'selected.0.is_folder', false) === 'true' ? true : false;
            if ( $isFolder ) {
                $mod = 'folders';
            }

            // when in media delete actions (files or folders delete) , system will map "delete" => "destroy" -> permission denied
            // so, we are not map "delete" => "destroy"
            $isMediaDelete = ($mod == 'files' || $mod == 'folders') && $action == 'delete';

            // Map permission
            $permission = $mod.'.'.(isset($aclMapAction[$action]) && !$isMediaDelete ? $aclMapAction[$action] : $action);

            // Not have permission
            $authorized = $request->user()->can($permission);
        }
        catch(Exception $e) {
            $authorized = false;
        }


        if (!$authorized) {

            // Check roles
            if ($roles) {

                foreach($roles as $role) {

                    // Pass super admin
                    if ($role->name == config('acl.group_with_full_permissions')) {
                        return $next($request);
                    }

                    try {
                        if ($role->hasPermissionTo($permission)) {
                            $authorized = true;
                        }
                    }
                    catch(Exception $e) {}

                }
            }

        }

        if (!$authorized) {
            if ($request->ajax()) {
                return response()->error(__("You are not allowed to perform this action!"));
            }
            else {
                abort(403);
            }

        }

        return $next($request);
    }
}
