<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\App;

class AuthNotRedirect
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (! Auth::user()) {
            if ($request->ajax()) {
                return response()->error(__('Unauthenticated'), 1013, 401);
            }
            else {
                return redirect(sprintf('%s?r=%s', route('admin.login'), urlencode($request->url()) ));
            }
        }

        return $next($request);
    }
}
