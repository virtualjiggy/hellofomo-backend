<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use App;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    /**
     * Determine if the session and input CSRF tokens match.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return bool
     */
    protected function tokensMatch($request)
    {
        $token = $this->getTokenFromRequest($request);

        $isValid = is_string($request->session()->token()) &&
            is_string($token) &&
            hash_equals($request->session()->token(), $token);

        /**
         * @vulh
         * Avoid display mis token error
        */

        if (!$isValid && !App::runningInConsole()) {
            return redirect('/login');
        }


        return $isValid;
    }
}
