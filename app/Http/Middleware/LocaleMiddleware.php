<?php

namespace App\Http\Middleware;

use App;
use Auth;
use Closure;
use Session;
use Settings;

class LocaleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = Auth::user();
        if ( !$user ) {
            return $next($request);
        }

        /**
         * Setting locale
         * Note: top locale > user's locale > general setting locale
         *
         * @ticket #13162 - HFBACKEND-41 - Small adjustments in Software (2)
         */
        $topLocale = Session::get('locale');
        if ( empty($topLocale) ) {
            $locale = !empty($user->language) ? $user->language : Settings::get(config('fomo.settings.language'));
        } else {
            $locale = $topLocale;
        }

        Session::put('locale', $locale);
        App::setLocale($locale);

        return $next($request);
    }
}
