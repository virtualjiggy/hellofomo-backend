# Table of Contents

* [Overview](#overview)
    * [Steps 1](#Steps-1)
    * [Steps 2](#Steps-2)
    * [Steps 3](#Steps-3)
* [Examples](#examples)
    * [Example of full column configuration](#example-of-full-column-configuration)
    * [Example of full Action column configuration](#example-of-full-action-column-configuration)
* [References](#references)

# Overview

Display list of users in datatable combine with Metronic theme.

## Steps 1

Go to file ```config/tables.php``` and define which columns you need to show as follow

```
'user' => [
    'title' => 'Users Overview',
    'id' => 'users-datatable',
    'ajax' => [
        'src' => 'admin.ajax.users.more', // route name
    ],
    'columns' => [
        'id' => ['text' => '#'],
        'name' => [
            'text' => 'Username',
            'filter' => [
                'type' => 'input',
            ],
            'orderable' => true,
        ],
        'email' => [
            'text' => 'Email',
            'filter' => [
                'type' => 'input',
            ],
            'orderable' => true,
        ],
        'phone' => [
            'text' => 'Phone',
            'filter' => [
                'type' => 'input',
            ],
            'orderable' => true,
        ],
        'status' => [
            'text' => 'Status',
            'html' => '<span class="label label-sm label-success">#data#</span>',
            'filter' => [
                'type' => 'select',
                'data' => [
                    config('elidev.list_default_status.pending') => config('elidev.list_default_status.pending'),
                    config('elidev.list_default_status.publish') => config('elidev.list_default_status.publish'),
                ],
            ],
            'orderable' => true,
        ],
        'created_at' => [ 'text' => 'Created at' ],
        'updated_at' => [ 'text' => 'Updated at' ],
    ],
    'actions' => [
        'add'    => [
            'name'  => 'Add new',
            'route' => 'admin.users.create', // route name
        ],
        'edit'   => [
            'name'  => 'Edit',
            'route' => 'admin.users.edit', // route name
        ],
        'delete' => [
            'name'  => 'Trash',
            'route' => 'admin.users.destroy', // route name
        ],
    ],
],
```

## Step 2

Define controller to show the table

File ```app/Http/Controllers/Admin/UserController.php@index```

```
public function index()
{
    return view('admin.users.index', [
        'table' => config('tables.admin.users'), // load user table configuration
    ]);
}
```

## Step 3

Define ajax action to load users

```
public function more(Request $request, ListingUserService $service)
{
    $users = $service->execute($request);
    $total = $users->total();

    // NOTE : YOU MUST RETURN JSON BY THIS FORMAT BECAUSE DATATABLE PLUGIN USE THIS FORMAT TO RENDER TABLE'S DATA
    return json_encode([
        'data'            => $users->getCollection()->toArray(),
        'recordsTotal'    => $total,
        'recordsFiltered' => $total,
    ]);
}
```

Please refer ```app/Http/Controllers/Admin/Ajax/UserController.php@more``` for more detail.

# Examples

## Example of full column configuration

```
'columns' => [
    ...
    'first_name' => [
        'text' => 'First name',

        // Filter. There are 2 types: input or select
        // Input filter
        'filter' => [
            'type' => 'input',
        ],
        // Select filter
        'filter' => [
            'type' => 'select',
            'data' => [
                'pending' => 'pending',
                'publish' => 'publish',
            ],
        ],

        'orderable' => true, // True: can be sorted via Ajax calls, False : can not be sorted. Default : false.
        'width'  => '10%', // % or px are OK
        'html' => '<span class="label label-sm label-success">#data#</span>', // for example: display status column, image column
    ],
    ...
],
```

## Example of full Action column configuration

Currently, there are 4 types of action buttons: add, edit, delete and assign.
You can choose which button can be displayed.

```
'actions' => [
    'width' => '20%',
    'add'    => [
        // Must input these values
        'name'  => 'Add new',
        'route' => 'admin.roles.create', // route name

        // Optional values
        'icon' => 'fa fa-pencil-square-o', // custom icon
        'color' => 'red', // custom color
    ],
    'edit'   => [
        'name'  => 'Edit',
        'route' => 'admin.roles.edit', // route name
    ],
    'delete'   => [
        'name'  => 'Assign',
        'route' => 'admin.roles.delete', // route name
    ],
    'assign'   => [
        'name'  => 'Assign',
        'route' => 'admin.roles.edit-assign', // route name
    ],
],
```

# References

List of resource sites

* Font awesome icons : http://fontawesome.io/icons/
* List of color names : http://keenthemes.com/preview/metronic/theme/admin_1/ui_colors.html
